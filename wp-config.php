<?php

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'epic7_dev_msl');

/** MySQL database username */
define('DB_USER', 'epic7_dev');

/** MySQL database password */
define('DB_PASSWORD', '3p!cD3v');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');


define('FILEPATH', '/Applications/MAMP/htdocs/msl/wp-content/themes/msl/');
define('BASE_URL', 'http://localhost/msl/');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'vAN>;]oTO/.E*saf[RpicjekC@X<LydV9`H($12uZ_8MPr^UW!|5{~m}nS)&7IzJ ');
define('SECURE_AUTH_KEY', '|,yrw3l76~&X:C(F80Du@1d[;WkUNbhcxs5fKViB}tM?R]jP`ZG*4L%gYqa9Q_.E ');
define('LOGGED_IN_KEY', ':(7z6,1@58G$YCMfm0naOT~wgrF/2&WBLRt#.dxl>sVpZo*}]ejX_iA{%[^9?cu| ');
define('NONCE_KEY', '9:*,%bFA?dTfQOg0i`KCR$/!Mh[pxB|JI)<;X@{^D~a.kz_eVcSmyn4oG1HU3sE& ');
define('AUTH_SALT', '?r>i3AgdGm5JL_1w<M!HR86x}NZ$9cUWfalj7`Sn%@VpEz]FPhX#yY,~0;Iv[|e^ ');
define('SECURE_AUTH_SALT', '#qe>,O[*_v$~MUa(W<4mBTIhQt/0}wrs8o@SJj!:F|KE)c5;lx%NpD]b2V9zHg3n ');
define('LOGGED_IN_SALT', 'W%|Ys>$~t*ifV`89O_6M{CUDxQ[Tal1L<#AF3(0)}2@Kv,ejX^7]mdZP!5z.?Gu& ');
define('NONCE_SALT', '.d59L#XS8ReCJGazrE[M6,0BW)~:n2hb|AFQp>_7xi4Y@mqHoNy^&*`{IOTt?/K1 ');

/**#@-*/


define('WPCF7_AUTOP', false);
define('WP_MEMORY_LIMIT', '128M');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'msl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
