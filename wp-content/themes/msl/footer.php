</section>

<?php 

    /* 
    //Test output for any given "option" variable 
    //(useful for testing array ouput of repeater fields, img fields, etc)
    echo '<pre>';
        print_r( get_field('social_media_repeater', 'option') );// or put in "msl_footer_logo" to view img 
    echo '</pre>';
    */

    //Address and Phone Details from "Motor Sportsland General Contact Settings"
    $addressLine1 = get_field('address_line_1', 'option');
    $addressLine2 = get_field('address_line_2', 'option');
    $city = get_field('city', 'option');
    $state = get_field('state', 'option');
    $zipCode = get_field('zip_code', 'option');
    
    $phoneNumber = get_field('local_phone_number', 'option');
    $tollFreePhoneNumber = get_field('toll_free_phone_number', 'option');
    
    $socialMediaRepeater = get_field('social_media_repeater', 'option');

    //Motor Sportsland White Logos and Footer Text from "Footer Settings"
    $mslLogo = get_field('msl_footer_logo', 'option');
    $mslWidth = $mslLogo['sizes']['large-width'] / 2;
    $mslHeight = $mslLogo['sizes']['large-height'] / 2;

    $rvpLogo = get_field('secondary_footer_image', 'option');
    $rvpWidth = $rvpLogo['sizes']['large-width'] / 2;
    $rvpHeight = $rvpLogo['sizes']['large-height'] / 2;
    
    $addressHeader = get_field('address_header', 'option'); 
    $rvsHeader = get_field('rvs_header', 'option');
    $specialsHeader =  get_field('specials_header', 'option');
    $socialMediaHeader =  get_field('social_media_header', 'option');
    
    $rvsRepeater = get_field('rvs_repeater', 'option');
    $specialsRepeater = get_field('specials_repeater', 'option');
    $footerLinksRepeater = get_field('footer_links_repeater', 'option');
    
?>





<footer  id="footer" >
    <?php do_action( 'foundationpress_before_footer' ); ?>
    <?php dynamic_sidebar( 'footer-widgets' ); ?>
    <?php do_action( 'foundationpress_after_footer' ); ?>

    <div class="page-width">
    
        <!-- Logos Row -->
        <div class="row" >  
    
            <div id="footer-img-container">
                <div class="small-12 medium-7  columns">
                    
                    <img id="msl-footer-logo" src="<?php echo $mslLogo['sizes']['large']; ?>" alt="<?php echo $mslLogo['alt']; ?>" 
                        title="<?php echo $mslLogo['title']; ?>" style="max-width: <?php echo $mslWidth; ?>px; max-height:<?php echo $mslHeight; ?>px;" />
                    
                </div>  
                <div class="small-12 medium-5  columns">
                    
                    
                    <img id="priority-img" src="<?php echo $rvpLogo['sizes']['large']; ?>" alt="<?php echo $rvpLogo['alt']; ?>" 
                        title="<?php echo $rvpLogo['title']; ?>" style="max-width: <?php echo $rvpWidth; ?>px; max-height:<?php echo $rvpHeight; ?>px;" />
                    
                </div>
            </div>    
        
        </div><!-- /Logos Row -->
        
        <!-- Address and Links Row -->
        <div id="address-links-container" class="row" > 
            
            <div class="small-12 medium-6  columns">
                
                <h4 class="green"><?php echo $addressHeader; ?></h4>
                <div id="footer-address-container" >
                    <div class="white">
                        <?php echo $addressLine1."<br />";?>
                    </div>
                    <div class="white" >    
                        <?php if($addressLine2 != "") echo $addressLine2."<br />"; ?>
                        <?php echo $city;?>, <?php echo $state;?> <?php echo $zipCode;?>
                    </div>
                </div>
                
                <div>
                    <div class="display-inline green">Local: </div>
                    <div class="display-inline white phone-number">
                        <a class="" href="tel:<?php echo $phoneNumber; ?>"><?php echo $phoneNumber; ?></a>
                    </div>
                </div>
                <div><div class="display-inline green">Toll-Free: </div><div class="display-inline white"> <?php echo $tollFreePhoneNumber;?></div></div>

                <div id="social-media-container-left" class="hide-wider-than-medium" >
                    <?php include('parts/social-media.php'); ?>
                </div>


            </div>   
            
        
             
            <div class="small-12 medium-6  columns">  
                
                <div class="row right-collapse" >
                    
                    <div class="small-12 large-4 columns">
 
                         <div id="rvs-container" class="border-right hide-smaller-than-small" >
                            <h4 class="green"><?php echo $rvsHeader;?></h4>
                            <?php
                                foreach($rvsRepeater as $k => $v){
                            ?>        
                                    <div>
                                        <a class="white" href="<?php echo $v['rv_link'];?>" >
                                            <?php echo $v['rv_text'];?>
                                        </a>
                                    </div>
                            <?php        
                                }
                            ?>
                        </div>  
                        
                    </div>
                    <div class="small-12 large-4  columns">
 
                         <div id="specials-container" class="border-right hide-smaller-than-small" >
                            <h4 class="green"><?php echo $specialsHeader;?></h4>
                            <?php
                                foreach($specialsRepeater as $k => $v){
                            ?>
                                    <div>        
                                        <a class="white" href="<?php echo $v['specials_link'];?>" >
                                            <?php echo $v['specials_text'];?>
                                        </a>
                                    </div> 
                            <?php        
                                }
                            ?>
                        </div>  
                        
                    </div>
                   
                    <div class="social-media-container hide-smaller-than-medium">
                        <div class="small-12 large-4  columns">
                            <?php include('parts/social-media.php'); ?>
                        </div>    
                    </div>
                    
                </div>
                
            </div>
    
        </div><!-- Address and Links Row -->
        
        <!-- Copyright Bottom Row -->
        <div class="row" >
            
            <div id="footer-line" class="hide-smaller-than-small">&nbsp;</div>

            <div class="small-12 medium-6  columns">  
                
                <div id="footer-links-container" class="white">
                    <?php
                        $first = true;
                        foreach($footerLinksRepeater as $k => $v){
                            
                            //don't show green pipe on first iteration
                            if($first){ 
                                $first = false;
                            }else{
                                echo '<span class="green">|</span>';
                            }
                    ?>        
                            <a class="white" href="<?php echo $v['footer_link'];?>" target="_blank">
                                <?php echo $v['footer_text'];?>
                            </a>
                    <?php        
                        }
                    ?>
                </div>
            
            </div>

            <div class="small-12 medium-6  columns">  
                
                <div id="copyright-container" class="white" >
                    Copyright <?php echo date("Y"); ?>  &copy; Motor Sportsland, Inc.
                </div>
            
            </div>
                 
        </div><!-- Copyright Bottom Row -->

    <?php do_action( 'foundationpress_layout_end' ); ?>
    </div>

</footer>



<!-- Request Info Popup Form -->
<div id="privacy-policy-popup" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    
    <!-- close button -->
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    
    <div class="row">
        <div class="small-12 columns">    
          <div class="contact-us-title">Privacy Policy</div>
        </div> 
                

        <div class="small-12 columns"> 
            <div class="request-info-details-text">    
                <?php echo do_shortcode('[get_content id="276" title="" /] '); ?>
            </div>
         </div>  
                        
    </div> 

</div>

<div id="menu-dark-bg"></div>

<a class="exit-off-canvas"></a>




<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>



</body>
</html>
    