<?php
/**
 * Template Name: Custom Sitemap XML
 */
 
//call in Motor Sportsland API
$api = new msl_api();

$inventory = $api->fetch_inventory("","",1);
//echo "<div class='red'>Inventory:</div><pre>".print_r($inventory,1)."</pre>";

$makesLines = $api->fetch_makes_lines("%");
//echo "<div class='red'>Makes and Lines:</div><pre>".print_r($makesLines,1)."</pre>";


$xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8" ?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"/>');


//add WP pages
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
));
foreach($pages as $page){
    $link = get_permalink($page->ID);
    $url = $xml->addChild('url');
    $url->addChild('loc', $link);
    $url->addChild('changefreq', "monthly");    
}


//inventory pages
foreach($inventory['rows'] as $k => $v){
    $url = $xml->addChild('url');
    $url->addChild('loc', BASE_URL.'details/?stock='.$v['stock']);
    $url->addChild('changefreq', "daily");       
}


   
//specs floorplans
$currentLink = $oldLink = "";
foreach($makesLines['rows'] as $k => $v){
    
    $currentLink = BASE_URL.'specs-floorplans?line='.strtolower($v['line']).'&yearMade='.$v['year'].'&manuf='.strtolower($v['manufacturer']);
    if($currentLink != $oldLink){
        $link = preg_replace("|&([^;]+?)[\s<&]|","&amp;$1 ",$currentLink);//sanitize
        //$link = str_replace('&', '&amp;', $currentLink);//sanitize
        $url = $xml->addChild('url');
        $url->addChild('loc', $link);
        $url->addChild('changefreq', "monthly");      
    }
    $oldLink = $currentLink;             
}


//specs details
foreach($makesLines['rows'] as $k => $v){
    //print_r($v);
    $link = BASE_URL."specs-detail/?line=".strtolower($v['line'])."&yearMade=".$v['year']."&floorplan=".strtolower($v['floorplan'])."&manuf=".$v['manufacturer']."&type=".$v['type']; 
  
    $link = str_replace('&', '&amp;', $link);//sanitize
    $url = $xml->addChild('url');
    $url->addChild('loc', $link);
    $url->addChild('changefreq', "monthly");                            
}   

//print to screen or to file (default is file)
if( isset($_GET['view']) && $_GET['view'] == 1 ){
    Header('Content-type: text/xml');
    print($xml->asXML());    
}else{
    print($xml->asXML('sitemap.xml'));
}
 
?> 