<?php
/* 
Template Name: Specs and Floorplans
*/
get_header(); 

//call in Motor Sportsland API
$api = new msl_api();


$year = $manufacturer = $line = $type = "";
if( isset($_GET['yearMade']) &&  !empty($_GET['yearMade']) && is_numeric($_GET['yearMade']) ){
    $year = $_GET['yearMade'];
}
if( isset($_GET['manuf']) &&  !empty($_GET['manuf']) ){
    $manufacturer = $_GET['manuf'];
}
if( isset($_GET['line']) &&  !empty($_GET['line']) ){
    $line = $_GET['line'];
}
if( isset($_GET['type']) &&  !empty($_GET['type']) ){
    $type = $_GET['type'];
}

$type = str_replace("-"," ",$type);

//the type variable must match the "type" field from the database 
$rows = $api->fetch_model_floorplans($type, $year, $manufacturer, $line);
$models = $rows['rows'];

$specFloorplan = $api->build_inventory_title("", $year, $manufacturer, $line, "");
 
 
//To view these queries above, put &test=1 in the URL while being signed in as an admin
if(isset($_GET['test']) && $_GET['test'] == 1 && is_user_logged_in() ){
    echo "<div class='red'>Models/Floorplans Query</div><pre>".print_r($rows['query'],1)."</pre>";
    echo "<div class='red'>Models/Floorplans Query</div><pre>".print_r($rows['rows'],1)."</pre>";    
}
?>

<!-- featured image -->
<?php get_template_part('parts/featured-image') ?>


<div class="page-width" role="main">

     <div class="row">
        <div class="small-12 columns" >
            
            <!-- Add pics of motor homes overlapping the featured image -->
            <div id="line-img" >
            <?php 
                $TTorFW = "";
                if($type == "travel trailer"){
                    $TTorFW = " TT";
                }elseif($type == "travel trailer"){
                    $TTorFW = " FW";
                }
                $mainImage = 'wp-content/gallery/'.$models[0]['year'].'/newrvs/exterior/'.strtolower($models[0]['line']).$TTorFW.'.png';
                if (file_exists($mainImage) ) {
                    $img = '<img src="'.BASE_URL.$mainImage.'" alt="'.$models[0]['line'].' '.$models[0]['line'].'" class="img-responsive center"   />';
                    echo $img;
                } 
            ?>
            </div>
       
        </div> 
        
        <div class="small-12 columns">
            <!-- Add Title and main content (From WYSIWIG) -->
            <h1 class="tc" > <?php echo $specFloorplan; ?></h1>

        </div>
    </div>    
    
       
    <div class="row">
        
        <?php do_action( 'foundationpress_before_content' ); ?>



        <?php 
        foreach($rows['rows'] as $row){
        
            $columnNum = 4;        
            $link = BASE_URL."specs-detail/?line=".strtolower($line)."&yearMade=$year&floorplan=".$row['floorplan']."&manuf=$manufacturer&type=".$_GET['type']; 
        ?>    
        
                <div class="small-12 medium-<?php echo $columnNum; ?> end columns" >
                    
                    <div class="floorplan-img-container" >
                                         
                        <a href="<?php echo $link; ?> "> 
                          
                            <img src="<?php echo BASE_URL; ?>wp-content/gallery/<?php echo $year; ?>/newrvs/floorplan/<?php echo strtolower($row['line']).'/'.$row['floorplan']; ?>.png" 
                                alt = "<?php echo strtolower($line).' - '.$row['floorplan']; ?>" />
                       
                        </a>
                      
                    </div>
                    <div class="tc floorplan-link" >
                         <a href="<?php echo $link ?>">
                            <?php echo $row['floorplan']; ?>
                         </a>   
                    </div>
                                            
                </div>         
        
        <?php    
        } 
        ?>



        <?php do_action( 'foundationpress_after_content' ); ?>

        <?php //get_sidebar(); ?>
    </div>
    
</div>    
<?php get_footer(); ?>
