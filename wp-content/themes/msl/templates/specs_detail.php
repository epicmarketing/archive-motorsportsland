<?php
/* 
Template Name: Specs Detail
*/
get_header(); 

//call in Motor Sportsland API
$api = new msl_api();

$year = $manufacturer = $line = $floorplan = "";
if( isset($_GET['yearMade']) &&  !empty($_GET['yearMade']) && is_numeric($_GET['yearMade']) ){
    $year = $_GET['yearMade'];
}
if( isset($_GET['manuf']) &&  !empty($_GET['manuf']) ){
    $manufacturer = $_GET['manuf'];
}
if( isset($_GET['line']) &&  !empty($_GET['line']) ){
    $line = $_GET['line'];
}
if( isset($_GET['floorplan']) &&  !empty($_GET['floorplan']) ){
    $floorplan = $_GET['floorplan'];
}
//$api->fetch_specs($stockOverview['year'],$stockOverview['make'],$stockOverview['line'],$stockOverview['model']); 
$specs = $api->fetch_specs($year, $manufacturer, $line, $floorplan);
$specRows = $specs['rows'];

//To view these queries above, put &test=1 in the URL while being signed in as an admin
if(isset($_GET['test']) && $_GET['test'] == 1 && is_user_logged_in()){
    echo "<div class='red'>Specs Query</div><pre>".print_r($specs['query'],1)."</pre>";
    echo "<div class='red'>Specs Result</div><pre>".print_r($specs['rows'],1)."</pre>";
}


$specFloorplan = $api->build_inventory_title("", $year, $manufacturer, $line, $floorplan);
$phoneNumber = get_field('local_phone_number', 'option');//get phone number from options


    preg_match('/^\D*(?=\d)/', $floorplan, $fpnum);
    if( !empty($fpnum[0])){
        $fpnum=substr($floorplan,strlen($fpnum[0])); 
    }else{
        $fpnum = "";
    }
    //echo $fpnum;  

//sanitize this since it's not being used in a prepared statement by using esc_sql
$customWhere = "year = '".esc_sql($year)."' AND make = '".esc_sql($manufacturer)."' AND line = '".esc_sql($line)."' AND model like '%".esc_sql($fpnum)."%'";
$inventory = $api->fetch_inventory("", "", $customWhere);
$count = $inventory['count'];
//print_r($inventory['rows']);

?>

<!-- featured image -->
<?php get_template_part('parts/featured-image') ?>

<div class="page-width" role="main">

     <div class="row">
        
        <div class="small-12 columns">
            <!-- Add Title and main content (From WYSIWIG) -->

            <h1><?php echo $specFloorplan; ?></h1>
            <?php $link = BASE_URL."rvs/?brand=".strtolower($line)."&yearMade=$year&floorplan=$fpnum&manuf=$manufacturer&type=".$_GET['type'];  ?>
            
            <?php if($count > 0){ ?>
            <div id="view-inventory-btn-container">
                <div class="left">
                    <a class="graybutton" href="<?php echo $link;?>">View Inventory</a>
                </div> 
                <div class="left green stock-count" ><?php echo $count; ?> in stock</div>
                <div class="clearfix"></div>
            </div>
            <?php } ?> 
        </div>

        
    </div>    


    <?php 
    $img = "wp-content/gallery/$year/newrvs/floorplan/$line/$floorplan.png";
    if (file_exists($img) ) {
    ?>
     <div class="row">
        <div class="small-12 columns">
            <div id="large-floorplan-table">
                <div id="large-floorplan-img-container" >
                    <img src="<?php echo BASE_URL.$img; ?>" 
                        alt = "<?php echo strtolower($line).' - '.$floorplan; ?>" 
                        />
                </div>  
            </div>          
        </div>
    </div>
    <?php
    }
    ?>
    
       

    <div class="row">
        

        <?php do_action( 'foundationpress_before_content' ); ?>


      
      
  
  
        <!-- Tabs -->
        <div class="small-12 columns">
        
            <div id="tabs-container">
                <ul class="tabs " data-tab role="tablist">
                  <li class="tab-title active" role="presentation"><a href="#panel-1" role="tab" tabindex="0" aria-selected="true" aria-controls="panel-1">Features & Specs</a></li>

                  
                  <?php if ( !empty($specRows[0]['video']) ) { ?>
                  <li class="tab-title" role="presentation"><a href="#panel-3" role="tab" tabindex="0" aria-selected="false" aria-controls="panel-3">Videos</a></li>
                  <?php } ?>
                </ul>
                <div class="tabs-content" >
                  <section role="tabpanel" aria-hidden="false" class="content active" id="panel-1">
                    
                    <div class="small-12 large-6 columns">
                        <div class="tab-content-left" >
                            <div class="tabs-detail-title" ><?php echo $specFloorplan; ?></div>
                            
                            <div class="tab-detail-description" ><?php echo $specRows[0]['description']; ?></div>
                            <?php if( !empty($specRows[0]['brochure']) ){ ?>
                                <!-- '.$stockSpecsRows[0]['year'].'/Brochure/ use which directory? -->
                                <button id="download-manufacturer-brochure" >
                                    <a class="white" href="<?php echo BASE_URL.'wp-content/gallery/images/tmp/'.$specRows[0]['brochure']; ?>" target="_blank">
                                        Download Manufacturer Brochure
                                    </a>
                                </button>
                            <?php } ?>
                            
                            <div class="small-12 medium-6 columns nopadding ">
                                <div id="request-info-container" >
                                    <button id="btn-request-info"  data-reveal-id="request-info-popup">Request Info</button>
                                </div>
                            </div>
         
                            <div class="tab-detail-contact-us" >For more information, call us at 
                                <a class="phone-number bold" href="tel:<?php echo $phoneNumber;?>"><?php echo $phoneNumber;?></a>
                            </div>                
                        </div>
                    </div>
        
        
                    <div class="small-12 large-6 columns">
                        <div class="tab-content-right" >
                            <div class="tab-content-border" >
                                
                                <?php
                                    $oldTitle = "";
                                    $floorplanData = "";
                                    $first = true;
                                    foreach($specRows as $k => $v){
                                        
                                    
                                        $currentTitle = $v['spec_type'];  
                                ?>
                                
                                    <?php 
                                        $addPadding = "";
                                        
                                        if($currentTitle != $oldTitle){
                                            $addPadding = "add-padding";
                                            
                                            if($first != true){
                                                ?>
                                                    <!-- add bottom padding -->
                                                    <div class="detail-container-padding" >
                                                        <div class="detail-left " >&nbsp;</div>
                                                        <div class="detail-right " >&nbsp;</div>                          
                                                    </div>
                                                <?php                    
                                            }  
                                            
                                    ?>
                                            <div class="detail-title" ><?php echo $v['spec_type']; ?></div>
                                    <?php
                                        }
                                    ?>
                                    
                                    <div class="detail-container" >
                                        
                                        <div class="detail-left <?php echo $addPadding; ?>" >
                                            <?php echo $v['label']; ?>
                                        </div>
                                 
                                        <div class="detail-right <?php echo $addPadding; ?>" >
                                            <?php 
                                                $value = ($v['value'] == "1" && preg_match("/additional/i",$v['spec_type'])) ? "yes" : $v['value']; 
                                                echo $value.' '.$v['units'];
                                            ?>
                                        </div>                          

                                    </div>
                                
                                <?php
                                    $oldTitle = $currentTitle;
                                    $first = false;
                                    }
                                ?>
                                
                                <!-- add bottom padding to the very last thing -->
                                <div class="detail-container-padding" >
                                    <div class="detail-left " >&nbsp;</div>
                                    <div class="detail-right " >&nbsp;</div>                          
                                </div>
                                
                            </div>                                       
                        </div>
                    </div>            
                    
                  </section>

                  <section role="tabpanel" aria-hidden="true" class="content" id="panel-3">
        
                    <div class="small-12 columns">
                        <div class="tab-content-full" >
                            <div class="tabs-detail-title" ><?php echo $specFloorplan; ?></div>
                            <?php if( !empty($specRows[0]['video'])){ ?>
                            <div class="myIframe">
                                <iframe  src="https://www.youtube.com/embed/<?php echo $specRows[0]['video']; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <?php } ?>                    
                        </div>
                    </div>
        
                  </section>
                
                </div>  
            </div>
        
        </div>   


        <?php do_action( 'foundationpress_after_content' ); ?>

        
        <?php //get_sidebar(); ?>
    </div>
    
</div>  


<?php
    $popupForms = $api->build_popup_forms($specFloorplan);
    echo $popupForms;
?>

  
<?php get_footer(); ?>
