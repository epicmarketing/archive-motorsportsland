<?php
/*
Template Name: Comparison
*/
get_header(); 

$api = new msl_api();

?>

<!-- featured image -->
<?php get_template_part('parts/featured-image') ?>


<div class="page-width" role="main">

     <div class="row">
        <div class="small-12 columns" >
            
            <!-- Add pics of motor homes overlapping the featured image -->
            <?php get_template_part( 'parts/overlay-image'); ?>
       
        </div> 

        <div class="small-12  columns">
            <!-- Add Title and main content (From WYSIWIG) -->
            <?php get_template_part( 'parts/title-and-main-content'); ?>
        </div>
    </div>    
   
   <?php if( isset($_SESSION['compare'][0]) && is_numeric($_SESSION['compare'][0]) ){ ?> 

        <div id="compare-top-actions" >
            <div class="row">
                <div class="small-12 medium-4 large-3 columns">
                    <a class="graybutton full-width-button" href="javascript:void(0)" data-reveal-id="comparison-email-popup">Email Page</a>
                </div>
                <div class="small-12 medium-4 large-3 end columns" >
                    <a class="graybutton full-width print-this-page" href="" >Print Page</a>
                </div> 
            </div>            
        </div> 
           
    
        <div class="row">
     
    
            <?php do_action( 'foundationpress_before_content' ); ?>
    
            <?php
                //echo "<pre>".print_r($_SESSION['compare'], 1)."</pre>";//for testing  
                $count = count($_SESSION['compare']);
                
                $inventory = $api->fetch_inventory (false, $_SESSION['compare'], "");
                //echo "<pre>".print_r($inventory, 1)."</pre>";//for testing 
        
                $buildInventory = $api->build_inventory($inventory['rows'], 0, $count, "", true);
                
                //builds the top part of the page
                echo $buildInventory;
                
                $num = 0;
                foreach($inventory['rows'] as $key => $values){
                        
                    $stockSpecs = $api->fetch_specs($values['year'],$values['make'],$values['line'],$values['model']);  
                    $stockSpecsRows = $stockSpecs['rows'];
                    $stockSpecsArr[$num] = $stockSpecsRows;
    
                    //echo "<pre>".print_r($stockSpecsRows, 1)."</pre>";//for testing 
                    $num++;
                }
                
                //get all labels for the a accordions
                $allLabels = array();
                foreach($stockSpecsArr as $k => $v){
                    foreach($v as $innerkey => $innerArr){
                        $allLabels[$innerArr['spec_type']][] = $innerArr['label'];
                    }
                }
                $labels = $api->super_unique($allLabels);
                //echo "<pre>".print_r( $labels, 1)."</pre>";//for testing 
                
                //make the accordion size and it's contents fit the number of items added and screen size (tricky!)
                if($count == 1){
                    $sizing = "small-6 medium-4 large-3";
                    $interiorSizing = "small-12 medium-12 large-12";
                }elseif($count == 2){
                    $sizing = "small-12 medium-8 large-6";
                    $interiorSizing = "small-6 medium-6 large-6";
                }elseif($count == 3){
                    $sizing = "small-12 medium-12 large-9";
                    $interiorSizing = "small-6 medium-4 large-4";
                }else{
                    $sizing = "small-12 medium-12";
                    $interiorSizing = "small-6 medium-4 large-3";
                }

            ?>
            
        </div>
        <div class="row">
            <div class="comparison <?php echo $sizing; ?> columns">
                <ul class="accordion" data-accordion="">
                        
                    <?php
                    $num=0;
                    foreach($labels as $cat => $label){  
                    ?>
                    
                        <li class="accordion-navigation relative">
                            <a href="#panel<?php echo $num; ?>"><?php echo $cat; ?></a>
                            <div id="panel<?php echo $num; ?>" class="content" >
                                <div class="row" style="margin-left: 0; margin-right: 0;">
                                    
                                    <?php  
                                        $loopCount = 0;
                                        foreach($stockSpecsArr as $key => $spec){
                                            
                                            $show = "";
                                            if($loopCount == 2){
                                                $show = "show-for-medium-up";
                                            }elseif($loopCount == 3){
                                                $show = "show-for-large-up";
                                            }
                                            
                                            //for last border...
                                            $lastBorder = "";
                                            if($count - $loopCount == 1){
                                                $lastBorder = "border-last";
                                            }
                                            
                                            echo "<div class='$interiorSizing end columns $show'>
                                                    <div class='comp-accordion-border border-$loopCount $lastBorder' >";
                                            
                                            foreach($label as $number => $specType){
                                                
                                                echo $loopCount == 0 ? "<div class='comp-accordion-content' >".$specType."</div>" : "<div class='comp-accordion-content'>&nbsp;</div>";
                                                
                                                $hasMatch = false;
                                                foreach($spec as $batch => $detail){
      
                                                    if($detail['label'] == $specType ){
                                                        
                                                        if($cat == "Additional Features" && $detail['value'] == 1){
                                                            $detail['value'] = "yes";
                                                        }
                                                        
                                                        echo "<div class='green comp-accordion-content text-center'>".$detail['value']." ".$detail['units']."</div>";
                                                        $hasMatch = true;
                                                    }
                                                }
                                                if($hasMatch == false){
                                                    echo "<div class='comp-accordion-empty' >&nbsp;</div>";
                                                }
     
                                            }
    
                                            echo        "<div class='comp-accordion-bottom' >&nbsp;</div>
                                                    </div>
                                                </div>";
                                            $loopCount++;   
                                            
                                            //echo "<pre>".print_r($spec, 1)."</pre>";//for testing 
                                        }
                                    ?>
                                   
                                </div>    
                            </div>
                        </li>                            
                
                    <?php
                        $num++;
                    }
                    ?>                  
                        
                </ul>
            </div>














        <?php                
        }else{
        ?>    
            <div class="small-12 columns">
                Check "compare" check boxes to view comparisons.  Visit <a href="<?php echo BASE_URL; ?>rvs/">this page</a> to get started.
            </div>
        <?php
        }
        ?> 
        
    
        <?php do_action( 'foundationpress_after_content' ); ?>

        
        <?php //get_sidebar(); ?>
    </div>
    
</div>  



<?php
    $popupForms = $api->build_popup_forms("");
    echo $popupForms;
?>


<!-- Comp Form -->
<div id="comparison-email-popup" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    
    <!-- close button -->
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    
    <div class="row">
        <div class="small-12 columns">    
          <div class="contact-us-title">Email Comparison</div>
        </div> 
                

        <div class="small-12 columns"> 
            <div class="request-info-details-text">    
                Please fill out the form to send comparison details to the specified email.
            </div>
         </div>  
                        
    </div> 

    <div class="row">
        <?php echo do_shortcode('[contact-form-7 id="451" title="Email Comparison"]'); ?>
    </div> 
    
  
</div>


  
<?php get_footer(); ?>
