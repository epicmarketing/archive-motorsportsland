<?php
/*
Template Name: Parts Specials
*/
get_header();


$args = array(
    'post_type' => 'part_specials', 
    'posts_per_page' => 100,
    'post_status' => 'publish'
);
$loop = new WP_Query( $args );
?>
 
<div class="page-width" role="main">



     <div class="row">
        <div class="small-12 columns">
       
            <!-- Add Title and main content (From WYSIWIG) -->
            <?php get_template_part('parts/title-and-main-content') ?>
        </div> 
    </div>       

    <div class="row">
        

        <?php do_action( 'foundationpress_before_content' ); ?>
    
            <?php 
                while ( $loop->have_posts() ) : $loop->the_post();
                
                    $partTitle = get_the_title();
                    $partPrice = get_field( 'part_price' ); 
                    $partDesc = get_field( 'part_description' ); 
                    
                    $partImg = get_field( 'part_image' ); 
                    $partImgWidth = $partImg['sizes']['large-width'] / 2;
                    $partImgHeight = $partImg['sizes']['large-height'] / 2;
                
            ?>
                <div class="small-12  medium-6 large-4 end columns">
                    
                    <div class="part-special-container" >
                        <div class="box" >
                            <div class="img-container" > 
                                <img class="img" style="max-width: <?php echo $partImgWidth; ?>px; max-height:<?php echo $partImgHeight; ?>px;" 
                                    src="<?php echo $partImg['sizes']['large']; ?>" alt="<?php echo $partImg['alt']; ?>" 
                                    title="<?php echo $partImg['title']; ?>"  />
                            </div>
                        </div>
                        <div class="text-container" >
                            <div class="title" ><?php echo $partTitle; ?></div>
                            <div class="green price" ><?php echo $partPrice; ?></div>
                            <div class="description" ><?php echo $partDesc; ?></div>
                        </div>    
                    </div>
                        
                </div>
              
            <?php
                endwhile;
                wp_reset_query();
            ?> 
    
        <?php do_action( 'foundationpress_after_content' ); ?>

        

    </div>
    
</div>   
 
<?php get_footer(); ?>