<?php
/* 
Template Name: Manufacturers and Makes
*/
get_header(); 

//call in Motor Sportsland API
$api = new msl_api();

$pagename = $post->post_name;
$type = str_replace("-"," ", $pagename);

//the type variable must match the "type" field from the database 
$rows = $api->fetch_makes_lines($type);//this value is safe from injection since it comes from a page name and template (but I use a prepared statement anyway)


//To view these queries above, put &test=1 in the URL while being signed in as an admin
if(isset($_GET['test']) && $_GET['test'] == 1 && is_user_logged_in()){
    echo "<div class='red'>Manufacturers and Lines Query</div><pre>".print_r($rows['query'],1)."</pre>"; 
}
?>

<!-- featured image -->
<?php get_template_part('parts/featured-image') ?>


<div class="page-width" role="main">

     <div class="row">
        <div class="small-12 columns" >
            
            <!-- Add pics of motor homes overlapping the featured image -->
            <?php get_template_part( 'parts/overlay-image'); ?>
       
        </div> 
        
        <div class="small-12 columns">
            <!-- Add Title and main content (From WYSIWIG) -->

            <h1 class="tc" >Specs and Floorplans: <?php echo the_title(); ?></h1>

        </div>
    </div>    
    
       
    <div class="row">
        
        <?php do_action( 'foundationpress_before_content' ); ?>

        <?php
        $sorted = $manufacturer = array();
        foreach($rows['rows'] as $k => $v){
            $sorted[$v['manufacturer']][$v['year']][$v['line']]= $v['floorplan'];
        }
        

        //get counts (counts are needed to build the year)
        $manufCounts = array();
        foreach($sorted as $k => $v){//k = manuf, v = array of year and line
            $manufCounts[$k]['count'] = 0;

            foreach($v as $k2 => $v2){//k2 = year, v2 = array of line
                
                $totalForThisYear = count($v2);
                //echo $k." = manuf,  ".$k2." = year,  and ".key($v2)." = line <Br />";
                $manufCounts[$k]['count'] = $manufCounts[$k]['count'] + $totalForThisYear;
            }
        }
       
        
        if(isset($_GET['test']) && $_GET['test'] == 1 && is_user_logged_in()){
            echo "<div class='red'>Manufacturers Array</div><pre>".print_r($sorted,1)."</pre>"; 
            echo "<div class='red'>Manufacturers Counts Array</div><pre>".print_r($manufCounts,1)."</pre>"; 
        }
        ?>



        <?php 
        $oldManuf = "";
        $firstIteration = true;
        foreach($sorted as $k => $v){
            
            $currentManuf = $k;  

            if($currentManuf != $oldManuf){
                
                //only show horizontal bar after 1st iteration
                if($firstIteration == true){
                    $firstIteration = false;
                }else{
                    echo "<hr />";
                }
        ?>
                
                <div class="small-12 columns">

                    <img
                        class="manuf-img" alt="<?php echo $currentManuf; ?> Logo"
                        src="<?php echo BASE_URL; ?>wp-content/gallery/<?php echo key($v); ?>/newrvs/logo/<?php echo strtolower($currentManuf); ?>_large.png"  />
 
                </div>    
                
                <?php
                $firstLineIteration = true;//only have offset on first iteration of lines
                $count = 1;
                foreach($v as $year => $lineArr){ 
                    
                    //echo print_r($lineArr);
                    
                    foreach($lineArr as $line => $floorplan){
                        
                        
                        $columnNum = 2;     
                        /*
                        if($manufCounts[$currentManuf]['count'] == 1){
                            $columnNum = 12;
                        }elseif($manufCounts[$currentManuf]['count'] == 2){
                            $columnNum = 6;
                        }elseif($manufCounts[$currentManuf]['count'] == 3){
                            $columnNum = 4;
                        }elseif($manufCounts[$currentManuf]['count'] == 4  || $manufCounts[$currentManuf]['count'] == 5 || $manufCounts[$currentManuf]['count'] > 6){
                            $columnNum = 3;// needs offset of 1 when count = 5
                        }elseif($manufCounts[$currentManuf]['count'] == 6){
                            $columnNum = 2;
                        }
                        */
                        $addOffset = $extraOffset = "";
                        if($firstLineIteration){
                            if($manufCounts[$currentManuf]['count'] == 1){
                                $addOffset = "medium-offset-5";
                            }elseif($manufCounts[$currentManuf]['count'] == 2){
                                $addOffset = "medium-offset-4";
                            }elseif($manufCounts[$currentManuf]['count'] == 3){
                                $addOffset = "medium-offset-3";
                            }elseif($manufCounts[$currentManuf]['count'] == 4 ){
                                $addOffset = "medium-offset-2";
                            }elseif($manufCounts[$currentManuf]['count'] == 5 ){
                                $addOffset = "medium-offset-1";
                            }elseif($manufCounts[$currentManuf]['count'] >= 6){
                                $addOffset = "medium-offset-0";
                            }                               
                        }
                        
                        $extraOffset = "";//adjust second row if needed
                        if($manufCounts[$currentManuf]['count'] == 7 && $count == 7){
                            $extraOffset = "medium-offset-5";
                        }elseif($manufCounts[$currentManuf]['count'] == 8 && $count == 7){
                            $extraOffset = "medium-offset-4";
                        }elseif($manufCounts[$currentManuf]['count'] == 9 && $count == 7){
                            $extraOffset = "medium-offset-3";
                        }elseif($manufCounts[$currentManuf]['count'] == 10 && $count == 7){
                            $extraOffset = "medium-offset-2";
                        }elseif($manufCounts[$currentManuf]['count'] == 11 && $count == 7){
                            $extraOffset = "medium-offset-1";
                        }elseif($manufCounts[$currentManuf]['count'] == 12 && $count == 7){
                            $extraOffset = "medium-offset-0";
                        }
                   
                ?>

                        <div class="small-6 medium-<?php echo $columnNum; ?> <?php echo $addOffset; echo $extraOffset; ?> end columns" >
                            <div class="line-container" >
                                <div class="line-logo-container" > 
                                    <div class="line-logo-boundaries" > <!-- &floorplan=<?php echo $floorplan;?> -->    
                                        <a href="<?php echo BASE_URL; ?>specs-floorplans?line=<?php echo strtolower($line); ?>&yearMade=<?php echo $year;?>&manuf=<?php echo $currentManuf; ?>&type=<?php echo $pagename; ?>"> 
                                          
                                            <img src="<?php echo BASE_URL; ?>wp-content/gallery/<?php echo $year; ?>/newrvs/logo/<?php echo strtolower($line); ?>_large.png" 
                                                alt = "<?php echo strtolower($line); ?>  Logo" />
                                       
                                        </a>
                                    </div>
                                </div>
                                <div class="tc line-year" >
                                    <?php echo $year; ?>
                                </div>
                            </div>                           
                        </div> 

       <?php 
                        $firstLineIteration = false;
                        $count++;
                    } 
                }
            }
  
            $oldManuf = $currentManuf;
        }
        ?>


        <?php do_action( 'foundationpress_after_content' ); ?>

        <?php //get_sidebar(); ?>
    </div>
    
</div>    
<?php get_footer(); ?>
