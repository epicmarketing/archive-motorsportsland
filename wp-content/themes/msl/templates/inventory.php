<?php
/*
Template Name: Inventory
*/
get_header(); 

//call in Motor Sportsland API
$api = new msl_api();

//if specials page, then the page query is different - find out if specials first
//based off page name (but since that might change) and page id (so if a new page is created, this snippet must be updated)
$pagename = get_query_var('pagename'); 
$pageID =  get_the_ID();
$hasSpecial = false;
$managerSpecialStocks = array();
if($pagename == 'specials' || $pageID == "87" ){
    $hasSpecial = true;
    $_SESSION['where'] = array();
}

$managerSpecialStocks = $api->fetch_manager_specials();

//This number, rows_per_page, is changed by the "show" drop down
//It determines how many inventory results to display on a single page view
//Pagination allows the rest of the inventory to be viewed
if(!isset($_SESSION['rows_per_page'])){
    $_SESSION['rows_per_page'] = 10;
}
if(isset($_GET['rows_per_page']) && is_numeric($_GET['rows_per_page'])){
    $_SESSION['rows_per_page'] = $_GET['rows_per_page'];
}


//This number, sort_option, is changed by the "Sort Option" drop down
//It determines the "Order By" in the inventory query
if(!isset($_SESSION['sort_option'])){
    $_SESSION['sort_option'] = "";
}
if(isset($_GET['sort_option']) ){
    $_SESSION['sort_option'] = $_GET['sort_option'];
}


//reset on each page load if needed
if(isset($_GET['condition']) || isset($_GET['type']) || isset($_GET['manuf']) || isset($_GET['brand'])  || isset($_GET['weight']) || isset($_GET['price'])  ){
    $_SESSION['where'] = array();
}


//set on condition from URL
if(isset($_GET['condition']) ){
    $_SESSION['where']['thecondition'] = array($_GET['condition'] => true);
}

//set on type from URL
if(isset($_GET['type']) ){
    
    //allow for toy hauler exception
    if($_GET['type'] == "toy-hauler"){
        if($_GET['condition'] == "used"){
            //$customWhere = "thecondition = 'used' && usedtoy = '1'";
            $_SESSION['where']['features']['used_toy_hauler'] = true;
        }else{
            $_SESSION['where']['features']['waf_toy_hauler'] = true;//new inventory has access to features
        }
        
    }else{
        $type = str_replace("-"," ",$_GET['type']);
        $_SESSION['where']['types'] = array($type => true);       
    }

}

//set on type from URL
if(isset($_GET['brand']) ){
    
    $brand = str_replace("_"," ",$_GET['brand']);
    $brand = strtolower($brand);
    $_SESSION['where']['line'] = array($brand => true);      
}

//set on price from URL
$price_arr = array('15000','30000','60000','90000','90000plus');
if(isset($_GET['price'])  && in_array($_GET['price'],$price_arr) ){
    
    $price = $_GET['price'];
    $price = str_replace("plus","+",$price);
    $_SESSION['where']['price'] = array($price => true);      
}

//set on weight from URL
$weight_arr = array('3500','5500','8500','8500plus');
if(isset($_GET['weight'])  && in_array($_GET['weight'],$weight_arr) ){
    
    $weight = $_GET['weight'];
    $weight = str_replace("plus","+",$weight);
    $_SESSION['where']['weight'] = array($weight => true);      
}




//set on type from URL
if(isset($_GET['manuf']) ){
    $make = strtolower($_GET['manuf']);
    $_SESSION['where']['make'] = array($make => true);
}

//get inventory on page load
$stock = array();
if(isset($_GET['stock']) && is_numeric($_GET['stock']) && strlen($_GET['stock']) == 5){
    $stock[0] = $_GET['stock'];
}

$result = $api->fetch_inventory($hasSpecial, $stock);
$rows = $result['rows'];
$counts = $api->fetch_counts($rows);
if(isset($_SESSION['where'])){
    $counts = $api->fetch_updated_counts($counts, $hasSpecial);
}



/*
$result = $api->fetch_inventory();
$rows = $result['rows'];
//echo "<pre>".print_r($rows,1)."</pre>";
// echo  basename( get_page_template() ); 
?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
    <channel>
        <title>Motor Sportsland Product Catalog</title>
        <link>http://dev.epicslc.com/msl</link>
        <description>All current inventory for Motor Sportsland</description>

<?php 
    foreach($rows as $k => $row){
        
        
        
            if( strtolower($row['thecondition']) == "used"){
                $rootImage = "wp-content/gallery/used/".$row['stock']."/main.jpg";
            }else{
                $rootImage = "wp-content/gallery/".$row['year']."/current/stock/".$row['stock']."/main.jpg";
            }
            
            //make sure image exists
            if(!file_exists($rootImage)){
                $rootImage = 'wp-content/themes/msl/assets/img/no-images.jpg';
            }            
                        
            if (file_exists($rootImage)) {
                $img = BASE_URL.$rootImage;
            } else {
                $img = BASE_URL.'wp-content/themes/msl/assets/img/no-images.jpg';
            }
?>    

        <item>
            <g:id><?php echo $row['stock'];?></g:id>
            <g:title><?php echo $api->build_inventory_title($row['thecondition'], $row['year'], $row['make'],$row['line'],$row['model']);?></g:title>
            <g:description><?php echo $row['description'];?></g:description>
            <g:link><?php echo BASE_URL; ?>details?stock=<?php echo $row['stock']; ?></g:link>
            <g:image_link><?php echo $img;?></g:image_link>
            <g:brand><?php echo $row['model'];?></g:brand>
            <g:condition><?php echo $row['thecondition']; ?></g:condition>
            <g:availability>in stock</g:availability>
            <g:price><?php echo $row['price']; ?></g:price>

            <g:google_product_category>920</g:google_product_category>
            <g:custom_label_0>MSRP: <?php echo $row['msrp']; ?></g:custom_label_0>
        </item>
<?php 
    }
?>          
    </channel>
</rss>
<?php
*/


/*
//exclusion fields (if you want to exclude a field, then put it here, ie array("u_shaped_dinette", "front_bedroom"); )
//NOTE: you can't exclude toy_hauler, or that will break that entire category!
$exclude = array();

$query = "
    SELECT u.id as model_id, l.id as line_id, u.floorplan, u.type, u.year as year, m.manufacturer, l.line, 
        sl.label, sl.units, st.spec_type, us.value, st.sort_order, sl.sort_order 
    FROM `units` u 
    INNER JOIN manufacturers m ON m.id = u.manuf_id 
    INNER JOIN ".DB_NAME.".lines l ON l.id = u.line_id 
    INNER JOIN unit_specs us ON us.unit_id = u.id 
    INNER JOIN spec_labels sl ON sl.id = us.label_id 
    INNER JOIN spec_types st ON st.id = sl.spec_type_id 
    WHERE m.disp =1 AND u.disp =1 AND ( (spec_type = 'weights' AND label LIKE '%Unloaded Vehicle Weight%') OR spec_type LIKE '%Additional%')
    ORDER BY `sl`.`label`  DESC
";
//echo $query;
$rowsExtra = $wpdb->get_results($query, ARRAY_A);//$this->db->get_results($query, ARRAY_A);
//echo "<div class='red'>ROWS:</div><pre>".print_r($rowsExtra,1)."</pre>";

$modelId = array();
$tableFields = $insertQueryPrep = array();
foreach($rowsExtra as $k => $specArr){
    $preppedField = 'waf_'.strtolower(str_replace( array(" ","&","-","1/2"),array("_","and","_","half"),$specArr['label']));
    if(!in_array($preppedField,$exclude)){
        $tableFields[$preppedField] = ($preppedField == "waf_unloaded_vehicle_weight" ? "`$preppedField` int(11) DEFAULT 0" : "`$preppedField` tinyint(1) DEFAULT 0");
    }    
    
    $m = $specArr['manufacturer'];
    $y = $specArr['year'];
    $l = $specArr['line'];
    $f = $specArr['floorplan'];
    $insertQueryPrep[$y][$m][$l][$f][$preppedField] = $specArr['value'];
    $modelId[$f] = $specArr['model_id'];//maybe this needs both year and floorplan, and not just floorplan
}
//echo "<div class='red'>Table Fields:</div><pre>".print_r($tableFields,1)."</pre>";
//echo "<div class='red'>Prep Query Array:</div><pre>".print_r($insertQueryPrep,1)."</pre>";

$extraFieldsQuery = implode(",",$tableFields);
$extraFieldsColumns = "`".implode("`, `", array_keys($tableFields))."`";

//scripts can't process more than one query at a time, so split this into three and run each of them
$createTableQuery1 = 
    "DROP TABLE IF EXISTS `weights_and_features`;";
$createTableQuery2 = 
    "CREATE TABLE `weights_and_features` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `waf_year` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `waf_make` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `waf_line` varchar(255) COLLATE utf8_unicode_ci NOT NULL,  
        `waf_model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `waf_model_id` int(11) NOT NULL,
        $extraFieldsQuery,
    PRIMARY KEY (`id`));";
$createTableQuery3 = 
    "ALTER TABLE `weights_and_features` ADD UNIQUE `unique_index`(`waf_year`, `waf_make`, `waf_line`, `waf_model`);";

//echo $createTableQuery1.$createTableQuery2.$createTableQuery3."<br /><br />";
$createdTable1 = $wpdb->get_results($createTableQuery1, ARRAY_A);
$createdTable2 = $wpdb->get_results($createTableQuery2, ARRAY_A);
$createdTable3 = $wpdb->get_results($createTableQuery3, ARRAY_A);

$insertQuery = "INSERT INTO `weights_and_features` (`waf_year`, `waf_make`, `waf_line`, `waf_model`, `waf_model_id`, ".$extraFieldsColumns.") VALUES";
//echo "<br /><br /><br />".$insertQuery;
$insertRowsQuery = array();
foreach($insertQueryPrep as $year => $makeArr){
    foreach($makeArr as $make => $lineArr){
        foreach($lineArr as $line => $modelArray){
            foreach($modelArray as $model => $specArray){
                $queryArr = array();
                foreach($tableFields as $column => $notNeeded){
                    if($column == "waf_unloaded_vehicle_weight"){
                        $queryArr[] = (isset($specArray['waf_unloaded_vehicle_weight']) ? $specArray['waf_unloaded_vehicle_weight'] : 0);
                    }elseif(array_key_exists($column,$specArray)){
                        $queryArr[] = 1;
                    }else{
                        $queryArr[] = 0;
                    }  
                    
                }   
                $insertRowsQuery[] = "($year, '$make', '$line', '$model', $modelId[$model], ".implode(",",$queryArr).")";
            }
        }
    }
}


//echo "<div class='red'>INSERT Query:</div><pre>".print_r($insertRowsQuery,1)."</pre>";
$insertQuery .= implode(",",$insertRowsQuery).";";

//echo $insertQuery;
$specsResults = $wpdb->get_results($insertQuery, ARRAY_A);
*/














//TESTING
if(isset($_GET['test']) && $_GET['test'] == 1 && is_user_logged_in()){
    echo "<div class='red'>Query:</div>".$result['query'];
    $specialRows = $api->fetch_types_specials(); echo "<div class='red'>specials:</div><pre>".print_r($specialRows['specialRows'],1)."</pre>";
    echo "<div class='red'>SESSION:</div><pre>".print_r($_SESSION,1)."</pre>";
    echo "<div class='red'>Counts:</div><pre>".print_r($counts,1)."</pre>";
    // echo "<div class='red'>Rows Results:</div><pre>".print_r($rows,1)."</pre>";
    echo "<div id='ajax-test'>Replace this with AJAX</div>";
    $countProblems = 0;
    foreach($rows as $k => $v){
        if(empty($v['waf_unloaded_vehicle_weight'])){
            $countProblems++;
        }
    }
    echo "The number of records where inventory did not have unloaded vehicle wait (and maybe didn't match up with specs): ".$countProblems."<br /><br />";
}

//build pagination details
$rowCount = sizeof($rows);
$paginationArr = $api->build_pagination($rowCount);


//build the inventory results
$display = $api->build_inventory($rows, $paginationArr['start'], $paginationArr['end'], $managerSpecialStocks);
if( empty($display) ){
    $display = "Sorry, your search did not return any results.  Please refine your search and try again.";
} 
?>

<div class="page-width" role="main">


    <?php do_action( 'foundationpress_before_content' ); ?>

    <!-- Title and content -->
    <div class="row">
        <div class="small-12 columns">
       
            <!-- Add Title and main content (From WYSIWIG) -->
            <?php get_template_part('parts/title-and-main-content'); ?>
        </div> 
    </div>       


    <div class="row" id="inv-sidebar">
         
         <!-- Search Accordions (left sidebar on web)-->
        <div class="small-12 large-4 columns">
            

            
            <div id="search-container" class="left relative" >
                <div id="search-dropdown-container" class="show-for-medium-down ">
                    <a id="search-dropdown" class="graybutton"  href="">
                        <div class="left search-btn-text" >Search</div>
                        <div class="right search-btn-arrow" >
                            <i class="fa fa-chevron-down"></i>
                            <!--<img src="<?php echo BASE_URL;?>wp-content/themes/msl/assets/img/arrow-down.png" />-->
                        </div>
                        <div class="clear"></div>
                    </a>
                </div>
                
                <div id="search-accordion-container">
                <ul class="accordion" data-accordion="">
                    
                    <!-- Condition (new and used) -->
                    <li class="accordion-navigation relative">
                        <a id="condition-accordion" href="#condition">Condition</a>
                        <div id="condition" class="content">
                            <div class="row">
                                <?php 
                                    $checkboxes = $api->build_search_checkboxes("thecondition", $counts);
                                   
                                    echo $checkboxes;  
                                ?>
                            </div>    
                        </div>
                    </li>                            
    
    
                    <li class="accordion-navigation relative">
                        <a href="#type">Type</a>
                        <div id="type" class="content">
                            <div class="row">
                                <?php 
                                    $checkboxes = $api->build_search_checkboxes("types", $counts);
                                   
                                    echo $checkboxes;  
                                ?>
                            </div>    
                        </div>
                    </li>
    
    
                    <li class="accordion-navigation relative">
                        <a href="#manufacturer">Manufacturer</a>
                        <div id="manufacturer" class="content">
                            <div class="row">
                                <?php 
                                    $checkboxes = $api->build_search_checkboxes("make", $counts);
                                   
                                    echo $checkboxes;  
                                ?> 
                            </div>    
                        </div>
                    </li>  

                    
                    <li class="accordion-navigation relative">
                        <a href="#line">Brand</a>
                        <div id="line" class="content">
                            <div class="row">
                                <?php 
                                    $checkboxes = $api->build_search_checkboxes("line", $counts);
                                   
                                    echo $checkboxes;  
                                ?> 
                            </div>    
                        </div>
                    </li>                                    
    
    
                    <li class="accordion-navigation relative">
                        <a href="#features">Features</a>
                        <div id="features" class="content">
                            <div class="row">
                                <?php 
                                    $checkboxes = $api->build_search_checkboxes("features", $counts);
                                   
                                    echo $checkboxes;  
                                ?> 
                            </div>    
                        </div>
                    </li>
    
    
                    <li class="accordion-navigation relative">
                        <a href="#price">Price</a>
                        <div id="price" class="content">
                            <div class="row">
                                <?php 
                                    $checkboxes = $api->build_search_checkboxes("price", $counts);
                                   
                                    echo $checkboxes;  
                                ?> 
                            </div>    
                        </div>
                    </li>
                    
    
                    <li class="accordion-navigation relative">
                        <a href="#weight">Weight</a>
                        <div id="weight" class="content">
                            <div class="row">
                                <?php 
                                    $checkboxes = $api->build_search_checkboxes("weight", $counts);
                                   
                                    echo $checkboxes;  
                                ?> 
                            </div>    
                        </div>
                    </li>                
    
                </ul> 
                </div>           
            </div>
            
            <!-- Search (left sidebar on web)-->
            <?php
            $show = "show";
            $comparisonRows = "";
            if( isset($_SESSION['compare'][0]) && is_numeric($_SESSION['compare'][0]) ){
                $show = "show";
                
                $comparisonRows = $api->build_comparison();
            }
            ?>
            
                
                <div id="compare-dropdown-container" class="right relative" >
                     <div class="show-for-medium-down ">
                        <a id="compare-dropdown" class="graybutton"  href="">
                            <div class="left compare-btn-text" >Compare</div>
                            <div class="right compare-btn-arrow" ><i class="fa fa-chevron-down"></i></div>
                            <div class="clear"></div>
                        </a>
                    </div>
                
                    <div id="compare-container" class="<?php echo $show;?> small-12 medium-6 columns">
                        
                        <div class="compare-inner-container" >
                            <div id="compare-content">
                                <?php echo ( !empty($comparisonRows) ? $comparisonRows : 'Check "compare" check boxes to view comparisons.<br /><br />' );?>
                            </div>
                            <a class="button btn-super btn-square btn-ghost-alt" href="<?php echo BASE_URL.'comparison';?>">Compare</a>
                            <div id="compare-error"></div> 
                        </div> 
                        
                    </div>  
                </div>
                                
        </div><!-- /Search Accordions (left sidebar on web)-->   
        

         
         
        <!-- Sort and Show options (start of right content)--> 
        <div class="small-12 large-8 columns">
            
            <div id="option-bar" class="" >
                <div id="sort-option-container" class="left" >
                    <select id="sort-options" >
                        <option value="">Sort Options</option>
                        <?php   
                        
                            //build "show" dropdown and remember user preference
                            $showSortOptions = array(
                                "Year - Oldest" => "year-oldest",
                                "Year - Newest" => "year-newest",
                                "Price - Lowest" => "price-lowest",
                                "Price - Highest" => "price-highest",
                                "Manufacturer - A" => "manufacturer-a",
                                "Manufacturer - Z" => "manufacturer-z"
                            );
                            
                            foreach($showSortOptions as $k => $v){
                            
                                $selected = "";
                                if(isset($_SESSION['sort_option']) && $_SESSION['sort_option'] == $v){
                                    $selected = "selected";
                                }
                                
                                echo "<option value='$v' $selected>$k</option>";
                            }
                        ?>                        
                    </select>
                </div>
                <div >
                    <div id="show-option-container" class="right show-for-medium-up" >
                        <select id="show-options" >
                        <?php   
                        
                            //build "show" dropdown and remember user preference
                            $showOptions = array(
                                "Show 10" => 10,
                                "Show 25" => 25,
                                "Show 50" => 50,
                                "Show 100" => 100
                            );
                            
                            foreach($showOptions as $k => $v){
                            
                                $selected = "";
                                if(isset($_SESSION['rows_per_page']) && $_SESSION['rows_per_page'] == $v){
                                    $selected = "selected";
                                }
                                
                                echo "<option value='$v' $selected>$k</option>";
                            }
                        ?>
                        </select>
                    </div>                     
                    <div id="results-top" class="results-line right show-for-medium-up" ><?php echo $paginationArr['results-line']; ?></div>
   
                </div>                
                <div class="clearfix"></div>
            </div>

        <!-- this is where the main block of inventory is displayed -->
        <div id="inventory-display">
            <?php echo $display; ?>
        </div>


        <?php
        //add pagination
        $pagination = paginate_links_custom($paginationArr['args']);
        ?> 
        
        <div id='pagination-inventory'><?php echo $pagination;?></div>
        <div class='clearfix'></div>
        <div id='results-bottom'>Showing <?php echo $paginationArr['results-line'];?></div>
        <div id="fine-print-2" class="fine-print">
            <?php echo get_field('disclaimer_text_payment_calculation', 'option'); ?>
        </div>
        
        <?php do_action( 'foundationpress_after_content' ); ?>

        </div><!-- /Sort and Show options (start of right content)--> 
    </div><!-- /Row--> 
</div> <!-- /Page Width--> 



<!-- Edit Details Popup Form -->
<div id="edit-details-popup" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    
    <!-- close button -->
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    
    <div class="row">
        <div class="small-12 columns">    
          <div class="contact-us-title" id="edit-title" data-stockedit="" data-yearedit="" data-oldmodeledit="">Edit Details</div>
        </div>
                
    </div> 

    <div class="pad-top-20">&nbsp;</div>

    <div class="row">
        <div class="small-12 medium-6 columns">
            <div class="form-group">
                <label class="" for="formMSRP">MSRP</label>
                <span class="">
                    <input type="text" id="formMSRP" name="formMSRP" value="" size="40" class="form-control" />
                </span>
            </div>
        </div>
        <div class="small-12 medium-4 columns hide" >
            <div class="form-group">
                <label class="" for="formMAP">MAP</label>
                <span class="">
                    <input type="text" id="formMAP" name="formMAP" value="" size="40" class="form-control"  />
                </span>
            </div>
        </div>
        <div class="small-12 medium-6 columns">
            <div class="form-group">
                <label class="" for="formPrice">Price</label>
                <span class="">
                    <input type="text" id="formPrice" name="formPrice" value="" size="40" class="form-control"  />
                </span>
            </div>
        </div>
        
        
        <div class="small-12 medium-12 columns">
            <div class="form-group">
                <label class="" for="formDescription">Unique Description</label>
                <span class="">
                    <textarea id="formDescription" name="formDescription" value="" size="40" class="form-control"  ></textarea>
                </span>
            </div>
        </div>
        <div class="clearfix"></div>
        

        <div id="edit-floor-plan-container" >
            <div class="small-12 columns">
                <div class="" >Floor Plan</div>
            </div>
    
            <div class="small-12 medium-4 columns">
                <div class="form-group">
                    <label class="" for="formManuf">Manuf</label>
                    <span class="">
                        <input type="text" id="formManuf" name="formManuf" value="" size="40" class="form-control" readonly/>
                    </span>
                </div>
            </div>
            <div class="small-12 medium-4 columns">
                <div class="form-group">
                    <label class="" for="formLine">Line</label>
                    <span class="">
                        <input type="text" id="formLine" name="formLine" value="" size="40" class="form-control"  readonly/>
                    </span>
                </div>
            </div>
            <div class="small-12 medium-4 columns">
                <div class="form-group">
                    <label class="" for="formModel">Model</label>
                    <span class="">
                        <select id="formModelS" name="formModelS" >
                        </select>
                    </span>
                </div>
            </div>              
        </div>
        
        <div class="pad-top-20">&nbsp;</div>
        
        <div class="small-12 columns">
            <div class="form-group">
                <a id="update-inv-details" class="button btn-super btn-square btn-ghost-alt" href="javascript:void(0)">Update</a>
            </div>
        </div>
        
        <div class="small-12 columns">
            <div id="updated-msg" class="green pad-top-20">
                
            </div>
        </div>

    </div>
 </div>  <!-- Edit Details Popup Form -->
<?php get_footer(); ?>
