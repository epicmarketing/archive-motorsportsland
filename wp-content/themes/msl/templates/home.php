<?php
/*
Template Name: Home
*/

if ( have_posts() ) : the_post(); 

    $title = get_the_title();
    
    $content = get_the_content();
    $content = apply_filters('the_content', $content);
    
   
endif; // end of the loop. 

get_header(); ?>


<!-- Slider -->
<?php get_template_part( 'parts/slider'); ?>

<!-- RV Sub Menu -->
<?php get_template_part( 'parts/rv-sub-menu'); ?>


<div class="page-width" role="main">

    <div class="row">
        <div class="small-12 columns">
            <h1 id="home-title" ><?php echo $title; ?></h1>
        </div> 
    </div>
    

    <div class="row">
        
       <div class="relative">
           
            <div class="small-12 medium-6 columns">
                
                <div class="row">
                    
                    <div id="home-search-box" >
                        <?php   
                            $api = new msl_api();
                            $result = $api->fetch_inventory("","",1);
                            $rows = $result['rows'];
                            $initialResults = count($rows);
                            $counts = $api->fetch_counts($rows);
                        ?>
                                                    
                        <div class="home-search-toggle" >
                            Search Inventory
                            <div class="right search-btn-arrow" ><i class="fa fa-chevron-down"></i></div>
                        </div>
                            
                        <div class="small-12 columns"><div id="results-home" class="green">All Inventory (<?php echo $initialResults; ?>)</div></div>    
                        <div class="small-12 medium-6 columns">
                            <div class="left" id="new-check-home" >
                                <input class="left home-search-inv" type="checkbox" id='search-new' />
                                <label class="left"  for='search-new'>New</label>
                            </div>
                            <div class="left" id="used-check-home" >
                                <input class="left home-search-inv" type="checkbox" id='search-used' />
                                <label class="left"  for='search-used'>Used</label>
                            </div>
                        </div> 
                        <div class="small-12 medium-6 columns">

                            <select id="search-type" class="home-search-inv" data-cat="types" data-column="">
                                <option value="">Type</option>
                                <?php
                                    $ddOptions = $api->build_dd_options("types", $counts);
                                    echo $ddOptions;
                                ?>                        
                            </select>    
                        </div> 
                        <div class="small-12 medium-6 columns">
                            <select id="search-manufacturer" class="home-search-inv" data-cat="make" >
                                <option value="">Manufacturer</option>
                                <?php
                                    $ddOptions = $api->build_dd_options("make", $counts);
                                    echo $ddOptions;
                                ?>
                            </select>     
                        </div>
                        <div class="small-12 medium-6 columns">
                            <select id="search-features" class="home-search-inv" data-cat="features" ><!-- multiple -->
                                <option value="" >Features</option>
                                <?php
                                    $ddOptions = $api->build_dd_options("features", $counts);
                                    echo $ddOptions;
                                ?>                                
                            </select>     
                        </div> 
                        <div class="small-12 medium-6 columns">
                            <select id="search-price" class="home-search-inv" data-cat="price" >
                                <option value="">Price</option>
                                <?php
                                    $ddOptions = $api->build_dd_options("price", $counts);
                                    echo $ddOptions;
                                ?>                                
                            </select>     
                        </div>                                                 
                        <div class="small-12 medium-6 columns">
                            <select id="search-weight" class="home-search-inv" data-cat="weight" >
                                <option value="">Weight</option>
                                <?php
                                    $ddOptions = $api->build_dd_options("weight", $counts);
                                    echo $ddOptions;
                                ?>                                
                            </select>     
                        </div> 

                        <div class="small-12 columns">
                            <input type="text" placeholder="Stock # (press enter after input)" id="search-stock-num" class="home-search-inv" />
                        </div>
                           
                        <div class="small-12 columns">
                            <a id="search-inventory-button" class="button" href="<?php echo BASE_URL."rvs/";?>">View Inventory</a>
                        </div>
                    </div>
                </div>
                 
                 
                <div id="search-dropdown-container">
                    <a id="search-dropdown" class="graybutton home-search-toggle home-content-btn"  href="" >
                        <div class="left search-btn-text" >Search</div>
                        <div class="right search-btn-arrow" ><i class="fa fa-chevron-up"></i></div>
                        <div class="clear"></div>
                    </a>
                </div>   
                
            </div>      
        </div> 

        <div class="small-12 medium-6 show-for-medium-up columns" >
            <a class="graybutton full-width-button home-content-btn" href="javascript:void(0)" data-reveal-id="contact-us-popup">Contact Us</a>
        </div> 
        
    </div> 
     


    <?php
        //echo "<pre>".print_r($_SESSION,1)."</pre>";
    
        $buildTypeArr['Class A Diesel'] = array(
            "image" => "wp-content/uploads/Class-A-Diesel.png", 
            "used" => "rvs/?condition=used&type=class-a-diesel",
            "new" => "rvs/?condition=new&type=class-a-diesel"
        );
        $buildTypeArr['Class A Gas'] = array(
            "image" => "wp-content/uploads/Class-A-Gas.png", 
            "used" => "rvs/?condition=used&type=class-a-gas",
            "new" => "rvs/?condition=new&type=class-a-gas"
        );
        $buildTypeArr['Class C'] = array(
            "image" => "wp-content/uploads/Class-C.png", 
            "used" => "rvs/?condition=used&type=class-c",
            "new" => "rvs/?condition=new&type=class-c"
        );
        $buildTypeArr['Toy Haulers'] = array(
            "image" => "wp-content/uploads/Toy-Hauler.png", 
            "used" => "rvs/?condition=used&type=toy-hauler",
            "new" => "rvs/?condition=new&type=toy-hauler"
        );
        $buildTypeArr['Travel Trailers'] = array(
            "image" => "wp-content/uploads/Travel-Trailers.png", 
            "used" => "rvs/?condition=used&type=travel-trailer",
            "new" => "rvs/?condition=new&type=travel-trailer"
        );
        $buildTypeArr['Fifth Wheels'] = array(
            "image" => "wp-content/uploads/Fifth-Wheel.png", 
            "used" => "rvs/?condition=used&type=fifth-wheel",
            "new" => "rvs/?condition=new&type=fifth-wheel"
        );
        $buildTypeArr['Folding Campers'] = array(
            "image" => "wp-content/uploads/Folding-Trailer.png", 
            "used" => "rvs/?condition=used&type=folding-trailer",
            "new" => "rvs/?condition=new&type=folding-trailer"
        );
    ?>

    <!-- Displays for full size screens and up - then disappears for Foundation Grid when screen is smaller -->
    <div id="home-types-container-1" class="show-for-large-up" >
        <div id="types-faux-bg" >&nbsp;</div>
        <div class="row">
            <div class="small-12 columns">
                <?php 
                foreach($buildTypeArr as $type => $details){
                ?>
                    <div class="type-box-1 left"  >
                        <div class="title-img-container-1">
                            <div class="type-title-1" ><?php echo $type; ?></div>
                            <div class="type-img-1" style="background: url('<?php echo BASE_URL.$details['image'];?>') no-repeat center center; background-size: contain;" ></div>
                        </div>
                        <div class="link-container-1" >
                            <div class="small-6 columns nopadding" >
                                <div class="new-container-1">
                                    <a href="<?php echo BASE_URL.$details['new']; ?>" class="sliding-middle-out">
                                        <div class="text-center  full-width link-text-1">New</div>
                                    </a>    
                                </div>
                            </div>
                            <div class="small-6 columns nopadding">
                                <div class="used-container-1">
                                    <a href="<?php echo BASE_URL.$details['used']; ?>" class="sliding-middle-out">
                                        <div class="text-center  full-width link-text-1">Used</div>
                                    </a>    
                                </div>
                            </div>
                        </div>
                    </div>
                
                <?php  
  
                }
                ?>                
                
            </div> 
        </div>
      
   </div>
        
   <!-- Types on Grid - only shows for medium and down -->     
   <div id="home-types-container-2" class="show-for-medium-down">     
        <div class="row">
            
            <div id="homes-inner-2">
                
                <?php
                $count = 0;
                foreach($buildTypeArr as $type => $details){
                    $leftBorder = "";
                    $leftBorderSmall = "";
                    if($count%4 == 0){
                        $leftBorder = "left-border";
                    }
                    if($count%4 == 2){
                        $leftBorderSmall = "left-border-small";
                    }
                ?>


                     <div class="small-6 medium-3 columns nopadding">   
                        <div class="type-box <?php echo $leftBorder; echo $leftBorderSmall; ?>" >
                            <div class="title-img-container">
                                <div class="type-title"><?php echo $type; ?></div>
                                <div class="type-img" style="background: url('<?php echo BASE_URL.$details['image'];?>') no-repeat center center; background-size: contain;" ></div>
                            </div>
                            <div class="link-container">
                                <div class="small-6 columns nopadding" >
                                    <div class="new-container">
                                        <a href="<?php echo BASE_URL.$details['new']; ?>" class="sliding-middle-out">
                                            <div class="text-center  full-width link-text">New</div>
                                        </a>    
                                    </div>
                                </div>
                                <div class="small-6 columns nopadding">
                                    <div class="used-container">
                                        <a href="<?php echo BASE_URL.$details['used']; ?>" class="sliding-middle-out">
                                            <div class="text-center  full-width link-text">Used</div>
                                        </a>    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                
                
                <?php  
                    $count++;  
                }
                ?>

                    <!-- empty box number 8 at the end -->
                     <div class="small-6 medium-3 columns nopadding">   
                        <div class="type-box " >
                            <div class="title-img-container">
                                <div class="type-title"></div>
                                
                            </div>
                            <div class="link-container last-container">
                                
                            </div>
                        </div>
                    </div>  
            </div>         
    
        </div>  
    </div>                   

    <div class="row">
        <div class="small-12 columns  text-center">

        <?php do_action( 'foundationpress_before_content' ); ?>

            <div class="home-page-content"><?php echo $content; ?></div>
 

        <?php do_action( 'foundationpress_after_content' ); ?>

        </div>

        <?php get_template_part( 'parts/specials'); ?> 

        <?php //get_sidebar(); ?>
    </div>
    
    
    
</div>    
<?php get_footer(); ?>
