<?php
/*
Template Name: FAQ
*/
get_header(); ?>
<div class="page-width" role="main">



     <div class="row">
        <div class="small-12 columns">
       
            <!-- Add Title and main content (From WYSIWIG) -->
            <?php get_template_part('parts/title-and-main-content'); ?>
        </div> 
    </div>       

    <div class="row">
        <div class="small-12 columns">

        <?php do_action( 'foundationpress_before_content' ); ?>

           
        <!-- Accordions -->
        <?php get_template_part( 'parts/accordions'); ?>
 

        <?php do_action( 'foundationpress_after_content' ); ?>

        </div>

        <?php //get_sidebar(); ?>
    </div>
    
</div>    
<?php get_footer(); ?>
