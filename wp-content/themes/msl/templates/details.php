<?php
/*
Template Name: Details
*/
get_header();

//call in Motor Sportsland API
$api = new msl_api();

$stockSpecsRows = $stockOverview = array();
$stock = $stockTitle = $sliderImages = $floorplanData = $floorplanImage = $virtualImages = "";
$validStock = false;
if(isset($_GET['stock']) && is_numeric($_GET['stock'])){
    $stock = $_GET['stock'];
    
    $stockOverviewRows = $api->fetch_inventory(false,array(0=>$stock));
    $stockOverview = array_shift($stockOverviewRows['rows']);//strip off the "0" since there is only one row and not multiple "rows"
    $stockSpecs = $api->fetch_specs($stockOverview['year'],$stockOverview['make'],$stockOverview['line'],$stockOverview['model']);  
    $stockSpecsRows = $stockSpecs['rows'];
      
    //To view these queries above, put &test=1 in the URL while being signed in as an admin
    if(isset($_GET['test']) && $_GET['test'] == 1 && is_user_logged_in()){
        echo "<div class='red'>Inventory Query</div><pre>".print_r($stockOverviewRows['query'],1)."</pre>"; 
        //echo "<div class='red'>Specs Query</div><pre>".print_r($stockSpecs['query'],1)."</pre>";
        //echo "<div class='red'>Specs Result</div><pre>".print_r($stockSpecs['rows'],1)."</pre>";
        //echo "<div class='red'>Videos Query</div><pre>".print_r($stockSpecs['query-video'],1)."</pre>";
        //echo "<div class='red'>Videos</div><pre>".print_r($stockSpecs['rows-video'],1)."</pre>";
        echo "<div class='red'>All Stock Specs</div><pre>".print_r($stockSpecs,1)."</pre>";
    }
    
    
    
    if(!empty($stockOverviewRows) && !empty($stockSpecsRows)){

        //Floorplan image
        $floorplanData = $stockSpecsRows[0]['floorplan'];
        $floorplanImage = "wp-content/gallery/".$stockOverview['year']."/newrvs/floorplan/".strtolower($stockOverview['line'])."/".$floorplanData.".png";
        //echo $floorplanImage;   

        $validStock = true;        
    }
    
    //virtual images path
    $virtualImages = "wp-content/gallery/".$stockOverview['year']."/current/stock/".$stock."/";

    //get title
    $stockTitle = $api->build_inventory_title($stockOverview['thecondition'], $stockOverview['year'], $stockOverview['make'], $stockOverview['line'], $stockOverview['model']);
    
    //if used, get used images.  Otherwise, get new images
    if( strtolower($stockOverview['thecondition']) == "used"){
        $sliderImages = "wp-content/gallery/used/".$stock."/";
    }else{
        $sliderImages = "wp-content/gallery/".$stockOverview['year']."/current/stock/".$stock."/";
    }


    $menu_logo = get_field('menu_logo', 'option'); 
    $logoWidth = $menu_logo['sizes']['large-width'] / 2;
    $logoHeight = $menu_logo['sizes']['large-height'] / 2;
    
    $price = number_format ( $stockOverview['price'], 2 );
    $msrp = number_format ( $stockOverview['msrp'], 2 );
    $msrpCalc = str_replace(",", "", $stockOverview['msrp']);
    $priceCalc =  str_replace(",", "", $stockOverview['price']);
    $difference = floatval($msrpCalc) - floatval($priceCalc); 
    $difference = "$".number_format ( ($difference), 2 ).""; 
    $description = $stockOverview['description'];
    
    
    $fetchEMP = $api->fetch_est_monthly_payment($stockOverview['price']);
    $estMonthlyPayment = $fetchEMP['emp'];    

}

$phoneNumber = get_field('local_phone_number', 'option');



//if(!empty($stock) && $validStock){
if(!empty($stock) ){
?>

<style>
    .virtual-tour canvas{
        height: 500px !important;
        width: 100% !important;
    }
    #watermark{
    	display: none;
    }
</style>
 
<div class="page-width" role="main">



     <div class="row">
        <div class="small-12 columns">
       
            <!-- Add Title -->
            <h1 id="detail-title"><?php echo $stockTitle; ?></h1>
            
        </div> 
    </div>       

    <div class="row">
        

        <?php do_action( 'foundationpress_before_content' ); ?>
    
        
        <!-- Pricing Details -->    
        <div class="small-12 columns">
        
            <div class="green price" >$<?php echo $price; ?></div>
            
            <div class="detail-pricing-container" >
                <?php if( strtolower($stockOverview['thecondition']) != "used"){ ?>
                <div class="detail-pricing" >MSRP | <span class="bold" >$<?php echo $msrp; ?></span></div>
                
                <div class="detail-pricing" >YouSave | 
                    <span class="red bold" >                                        
                        <?php echo $difference;  ?>
                     </span>
                  </div>
                  <?php } ?>
 
                
                <div class="detail-pricing last" >Est. Monthly Payment | 
                    <span id="esimated-pmt-<?php echo $stock; ?>" class="green bold" >$<?php echo $estMonthlyPayment; ?></span>
                </div> 
                <div class="clear"></div>
            </div>      
            
        </div>
        
          
        <!-- Four buttons on web, 2 on tablet, 0 on phone -->
        <div class="medium-6 large-3 columns show-for-large-up">
            <a id="calculate-<?php echo $stock; ?>" class="graybutton full-width-button calculate-payments-btn" href="javascript:void(0)"  
                data-interest="<?php echo $fetchEMP['interest_rate']; ?>" data-down="<?php echo $fetchEMP['down']; ?>" data-years="<?php echo $fetchEMP['years']; ?>" 
                data-price="<?php echo $stockOverview['price']; ?>" data-emp="<?php echo $estMonthlyPayment; ?>" data-stock="<?php echo $stock; ?>">Calculate Payment
            </a><!-- data-reveal-id="calculate-payment-popup" -->
        </div>
 
            <div class="medium-6 large-3 columns show-for-large-up">
            <a class="graybutton full-width-button" href="<?php echo BASE_URL;?>financing/" >Apply For Financing</a>
        </div>

        <div class="medium-6 large-3 columns show-for-medium-up">
            <a class="graybutton full-width-button" href="javascript:void(0)" data-reveal-id="email-popup">Email Page</a>
        </div>

        <div class="medium-6 large-3 columns show-for-medium-up">
            <a class="graybutton full-width-button print-this-page" href="#">Print Page</a>
        </div>            


        <!-- Slider -->
        <?php  if(file_exists($sliderImages."1.jpg")){ ?>      

        <div class="small-12 columns">
        
            <div  class="detail-slider-container">
                
                <div id="sync1" class="owl-carousel">
                    
                    <?php 
                        $j=1;
                        while(file_exists($sliderImages.$j.".jpg")) {                         
                    ?>
                            <div class="item" style="background-image: url('<?php echo BASE_URL.$sliderImages.$j.".jpg"; ?>'); background-size: cover;"></div>   
                            <!--
                            <img src='<?php echo BASE_URL.$sliderImages.$j.".jpg"; ?>' />   
                            -->         
                     <?php
                            $j++;
                        }
                    ?>   
                
                </div>
                <div id="sync2" class="owl-carousel show-for-medium-up" >
                
                    <?php 
                        $j=1;
                        while(file_exists($sliderImages.$j.".jpg")) {
                    ?>
                            <div class="item relative" style="background-image: url('<?php echo BASE_URL.$sliderImages.$j.".jpg"; ?>'); background-size: cover;">
                                <div class="selected-item-bg" ></div> 
                            </div>         
                     <?php
                            $j++;
                        }
                    ?>
                
                </div>
        
            </div>    
        
        </div>              
        <?php  }   ?>




        

        <!-- Tabs -->
        <div class="small-12 columns">
        
            <div id="tabs-container">
                <ul class="tabs " data-tab role="tablist">
                  <li class="tab-title active" role="presentation"><a href="#panel-1" role="tab" tabindex="0" aria-selected="true" aria-controls="panel-1">Features & Specs</a></li>
                  <?php if (file_exists($floorplanImage) ) { ?>
                  <li class="tab-title" role="presentation"><a href="#panel-2" role="tab" tabindex="1" aria-selected="false" aria-controls="panel-2">Floorplan</a></li>
                  <?php } ?>
                  
                  <?php if ( !empty($stockSpecs['rows-video'][0]['youtube_id']) ) { ?>
                  <li class="tab-title" role="presentation"><a href="#panel-3" role="tab" tabindex="2" aria-selected="false" aria-controls="panel-3">Videos</a></li>
                  <?php } ?>

                  <?php if (file_exists($virtualImages.'/360_1.jpg') ) { ?>
                  <li class="tab-title" role="presentation"><a href="#panel-4" role="tab" tabindex="3" aria-selected="false" aria-controls="panel-4">Virtual Tour</a></li>
                  <?php } ?>
                </ul>
                <div class="tabs-content" >
                  <section role="tabpanel" aria-hidden="false" class="content active" id="panel-1">
                    
                    <div class="small-12 large-6 columns">
                        <div class="tab-content-left" >
                            <div class="tabs-detail-title" ><?php echo $stockTitle; ?></div>
                            <div class="green tab-detail-price" >$<?php echo number_format ( $stockOverview['price'], 2 ); ?></div>
                            <div class="tab-detail-description" ><?php echo stripslashes($description) ?></div>
                            <?php if( !empty($stockSpecsRows[0]['brochure']) ){ ?>
                                <!-- '.$stockSpecsRows[0]['year'].'/Brochure/ use which directory? -->
                                <button id="download-manufacturer-brochure" >
                                    <a class="white" href="<?php echo BASE_URL.'wp-content/gallery/images/tmp/'.$stockSpecsRows[0]['brochure']; ?>" target="_blank">
                                        Download Manufacturer Brochure
                                    </a>
                                </button>
                            <?php } ?>
                            
                            <div class="small-12 medium-6 columns nopadding ">
                                <div id="request-info-container" >
                                    <button id="btn-request-info"  data-reveal-id="request-info-popup">Request Info</button>
                                </div>
                            </div>
                            <div class="small-12 medium-6 columns nopadding">
                                <div id="calculate-payment-container" >
                                   
                                    <button id="calculate-<?php echo $stock; ?>" class="calculate-payments-btn btn-calculate-payments" href="javascript:void(0)"  
                                        data-interest="<?php echo $fetchEMP['interest_rate']; ?>" data-down="<?php echo $fetchEMP['down']; ?>" 
                                        data-years="<?php echo $fetchEMP['years']; ?>" data-price="<?php echo $stockOverview['price']; ?>" 
                                        data-emp="<?php echo $estMonthlyPayment; ?>" data-stock="<?php echo $stock; ?>">Calculate Payments
                                    </button>
                                </div>
                            </div>
                            <div class="tab-detail-contact-us" >For more information, call us at 
                                <a class="phone-number bold" href="tel:<?php echo $phoneNumber;?>"><?php echo $phoneNumber;?></a>
                            </div>   
                            
                            
                            <!-- print view pics -->
                            <div class="comp-print ">
                                <div class="small-12 columns nopadding pad-top-20">
                                    <img class="item" src='<?php echo BASE_URL.$sliderImages."1.jpg"; ?>' />
                                </div>
                                <div class="small-12 columns nopadding pad-top-20 pad-bottom-20">
                                    <?php            
                                        if (file_exists($floorplanImage) ) {
                                            $img = '<img src="'.BASE_URL.$floorplanImage.'" 
                                                alt="'.$stockOverview['line'].' '.$stockOverview['model'].'" class="img-responsive center"  />';
                                            echo $img;
                                        } 
                                    ?>
                                </div>          
                            </div>
                                         
                        </div>
                    </div>
        
        
                    <div class="small-12 large-6 columns">
                        <div class="tab-content-right" >
                            <div class="tab-content-border" >
                                
                                <?php
                                    $oldTitle = "";
                                    $floorplanData = "";
                                    $first = true;
                                    foreach($stockSpecsRows as $k => $v){
                                    
                                        $currentTitle = $v['spec_type'];  
                                ?>
                                
                                    <?php 
                                        $addPadding = "";
                                        

                                        
                                        if($currentTitle != $oldTitle){
                                            $addPadding = "add-padding";
                                            
                                            if($first != true){
                                                ?>
                                                    <!-- add bottom padding -->
                                                    <div class="detail-container-padding" >
                                                        <div class="detail-left " >&nbsp;</div>
                                                        <div class="detail-right " >&nbsp;</div>                          
                                                    </div>
                                                <?php                    
                                            }                                          
                                    ?>
                                            <div class="detail-title" ><?php echo $v['spec_type']; ?></div>
                                    <?php
                                        }
                                    ?>
                                    
                                    <div class="detail-container" >
                                        
                                        <div class="detail-left <?php echo $addPadding; ?>" >
                                            <?php echo $v['label']; ?>
                                        </div>
                                 
                                        <div class="detail-right <?php echo $addPadding; ?>" >
                                            <?php 
                                                $value = $v['value'] == "1" ? "yes" : $v['value']; 
                                                echo $value.' '.$v['units'];
                                            ?>
                                        </div>                          

                                    </div>
                                
                                <?php
                                    $oldTitle = $currentTitle;
                                    $first = false;
                                    }
                                ?>
                                
                                <!-- add bottom padding to the very last thing -->
                                <div class="detail-container-padding" >
                                    <div class="detail-left " >&nbsp;</div>
                                    <div class="detail-right " >&nbsp;</div>                          
                                </div>
                                
                            </div>                                       
                        </div>
                    </div>            
                    
                  </section>
                  <section role="tabpanel" aria-hidden="true" class="content" id="panel-2">
        
                    <div class="small-12 columns">
                        <div class="tab-content-full" >
                            <div class="tabs-detail-title" ><?php echo $stockTitle; ?></div>
                            <div class="floorplan-img-container" >
                                <?php            
                                    if (file_exists($floorplanImage) ) {
                                        $img = '<img src="'.BASE_URL.$floorplanImage.'" 
                                            alt="'.$stockOverview['line'].' '.$stockOverview['model'].'" class="img-responsive center"  />';
                                        echo $img;
                                    } 
                                ?>                      
                            </div>
                        </div>
                    </div>
        
                  </section>
                  <section role="tabpanel" aria-hidden="true" class="content" id="panel-3">
        
                    <div class="small-12 columns">
                        <div class="tab-content-full" >
                            <div class="tabs-detail-title" ><?php echo $stockTitle; ?></div>
                            <?php
                            foreach($stockSpecs['rows-video'] as $k => $v){
                            ?>
                                <div class="myIframe">
                                    <iframe  src="https://www.youtube.com/embed/<?php echo $v['youtube_id']; ?>?rel=0" frameborder="0" allowfullscreen></iframe>
                                </div>
                            <?php    
                            }
                            ?>
                   
                        </div>
                    </div>
        
                  </section>

                  <section role="tabpanel" aria-hidden="true" class="content" id="panel-4">
        
                    <div class="small-12 columns">
                        <div class="tab-content-full" >
                            <div class="tabs-detail-title" ><?php echo $stockTitle; ?></div>
                            
                            <?php
                                //https://developers.google.com/vr/concepts/vrview-web
                                if( file_exists($virtualImages.'/360_1.jpg')){
                                    $n=1;
                                    while(file_exists($virtualImages.'/360_'.$n.".jpg")) {
                                        echo do_shortcode('[vrview img="'.BASE_URL.$virtualImages.'360_'.$n.'.jpg" 
                                            pimg="'.BASE_URL.$virtualImages.'360_'.$n.'.jpg" width="100%" height="500" ]');

                                        $n++;
                                    }
                                }
                            ?>  
                   
                        </div>
                    </div>
        
                  </section>
                
                </div>  
            </div>
        
        </div>   



        <!-- Get 2 similar listings to display at bottom -->
        <?php get_template_part( 'parts/similar'); ?> 
    
        <?php do_action( 'foundationpress_after_content' ); ?>
        
    </div>
    
</div>  



<?php
    $popupForms = $api->build_popup_forms($stockTitle);
    echo $popupForms;
?>




    <!-- Email Form -->
    <div id="email-popup" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        
        <!-- close button -->
        <a class="close-reveal-modal" aria-label="Close">&#215;</a>
        
        <div class="row">
            <div class="small-12 columns">    
              <div class="contact-us-title"><?php echo $stockTitle; ?></div>
            </div> 
                    
    
            <div class="small-12 columns"> 
                <div class="request-info-details-text">    
                    Please fill out the form to send page details to the specified email.
                </div>
             </div>  
                            
        </div> 
    
        <div class="row">
            <?php echo do_shortcode('[contact-form-7 id="450" title="Email Detail"]'); ?>
        </div> 
        
      
    </div>


 
<?php }else{ echo "<div class='text-center' >You must have a valid stock number to see inventory details!  <a href='".BASE_URL."/rvs/'>Return to inventory page.</a></div>"; } ?>
    


<?php get_footer(); ?>