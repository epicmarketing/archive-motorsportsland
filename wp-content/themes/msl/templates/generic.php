<?php
/*
Template Name: Generic
*/
get_header(); 


$hasSidebar = get_field('include_image_sidebar');
$sidebarImage = "";
$mainColumn = "12";
if(is_array($hasSidebar) && $hasSidebar[0]  == "yes" ){
    $sidebarImage = get_field('sidebar_image'); 
    $mainColumn = "8";
    //print_r( $sidebarImage);
}

?>

<!-- featured image -->
<?php get_template_part('parts/featured-image') ?>

<div class="page-width" role="main">

     <div class="row">
        <div class="small-12 columns" >
            
            <!-- Add pics of motor homes overlapping the featured image -->
            <?php get_template_part( 'parts/overlay-image'); ?>
       
        </div> 
        
        <div class="small-12 medium-<?php echo $mainColumn; ?> columns">
            <!-- Add Title and main content (From WYSIWIG) -->
            <?php get_template_part( 'parts/title-and-main-content'); ?>
            

        </div>
        
        <?php if(is_array($hasSidebar) && $hasSidebar[0] == "yes" ){ ?>
            <div class="small-12 medium-4 show-for-medium-up columns">
                <div class="sidebar-img">
                    <img src="<?php echo $sidebarImage['url']; ?>" alt="<?php echo $sidebarImage['alt']; ?>" class="img-responsive" />
                </div>    
            </div>
        <?php } ?>
        
    </div>    
    
       

    <div class="row">
        

        <?php do_action( 'foundationpress_before_content' ); ?>


        <!-- Add flexible/secondary content -->
        <?php get_template_part( 'parts/secondary-content'); ?> 

        <!-- Get 2 manager specials to display at bottom -->
        <?php get_template_part( 'parts/specials'); ?> 
        
    
        <?php do_action( 'foundationpress_after_content' ); ?>

        

        <?php //get_sidebar(); ?>
    </div>
    
</div>    
<?php get_footer(); ?>
