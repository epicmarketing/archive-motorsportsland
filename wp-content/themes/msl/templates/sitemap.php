<?php
/*
Template Name: Sitemap
*/
get_header(); 

//call in Motor Sportsland API
$api = new msl_api();

$inventory = $api->fetch_inventory("","",1);
//echo "<div class='red'>Inventory:</div><pre>".print_r($inventory,1)."</pre>";

$makesLines = $api->fetch_makes_lines("%");
//echo "<div class='red'>Makes and Lines:</div><pre>".print_r($makesLines,1)."</pre>";

?>
<div class="page-width" role="main">



     <div class="row">
        <div class="small-12 columns">
       
            <!-- Add Title and main content (From WYSIWIG) -->
            <?php get_template_part('parts/title-and-main-content'); ?>
        </div> 
    </div>       

    <div class="row">
        <div class="small-12 columns">

        <?php do_action( 'foundationpress_before_content' ); ?>

            <h2 class="">Inventory</h2>
            <ul class="wsp-pages-list">
                <?php
                    foreach($inventory['rows'] as $k => $v){
                        $inventoryTitle = $api->build_inventory_title($v['thecondition'], $v['year'], $v['make'],$v['line'],$v['model']);
                        
                        echo '<li class="page_item"><a href="'.BASE_URL.'details/?stock='.$v['stock'].'">'.$inventoryTitle.'</a></li>';
                    }
                ?>
            </ul>       
 



            <h2 class="">Specs Floorplans</h2>
            <ul class="wsp-pages-list">
                <?php
                    $currentLink = $oldLink = "";
                    foreach($makesLines['rows'] as $k => $v){
                        
                        $currentLink = BASE_URL.'specs-floorplans?line='.strtolower($v['line']).'&yearMade='.$v['year'].'&manuf='.strtolower($v['manufacturer']);
                        if($currentLink != $oldLink){
                ?>        
                
                            <li class="page_item">
                                <a href="<?php echo $currentLink; ?>">
                                    <?php echo $v['year'].' '.strtolower($v['manufacturer']).' '.strtolower($v['line']); ?>
                                </a> 
                            </li>
                <?php
                        }
                        $oldLink = $currentLink;
                             
                    }
                ?>
            </ul>


 
            <h2 class="">Specs Details</h2>
            <ul class="wsp-pages-list">
                <?php
                    foreach($makesLines['rows'] as $k => $v){
                        $link = BASE_URL."specs-detail/?line=".strtolower($v['line'])."&yearMade=".$v['year']."&floorplan=".strtolower($v['floorplan'])."&manuf=".$v['manufacturer']; 
                ?>        

                        <li class="page_item">
                            <a href="<?php echo $link; ?>">
                                <?php echo $v['year'].' '.strtolower($v['manufacturer']).' '.strtolower($v['line']).' - '.strtolower($v['floorplan']); ?>
                            </a> 
                        </li>                       
                        
                <?php        
                    }
                ?>
            </ul>    
            
 

        <?php do_action( 'foundationpress_after_content' ); ?>

        </div>

        <?php //get_sidebar(); ?>
    </div>
    
</div>    
<?php get_footer(); ?>
