
jQuery(document).ready(function($) {

    /*
 

    var nice = $(".mega-sub-menu").niceScroll({
        cursorcolor: "#424242", // change cursor color in hex
        cursoropacitymin: 1, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
        cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
        cursorwidth: "5px",
        horizrailenabled: false
    });

    */

    /**************************************************
     * Home Page Slider
     **************************************************/
    var slider = $('.bxslider').bxSlider({
        auto: true,         //Slides will automatically transition
        autoDelay: 4000,    //Time (in ms) auto show should wait before starting
        pause: 6000,        //The amount of time (in ms) between each auto transition
        useCSS: false,      //If true, CSS transitions will be used instead of juery animate()
        autoHover: true     //Auto show will pause when mouse hovers over slider
        ,controls: false    //If true, "Next" / "Prev" controls will be added
    });  

    /*  
    //don't slide if only one slide
    if($('.bxslider').length != 0){

        var slideQty = slider.getSlideCount();
        if(slideQty == 1){
            slider.stopAuto();
            setTimeout(function() { slider.stopAuto(); }, 2000);
        }       
    }
    */


    /**************************************************
     * Home Page Search Button and form
     **************************************************/
    $( ".home-search-toggle" ).click(function(e) {
        e.preventDefault();
        $( "#home-search-box" ).slideToggle( 400, function() {
            
        });
    });
    
    
    $(".home-search-inv").change(function(){
        
        //get all values and pass them all over
        var type = $("#search-type").val();
        var make = $("#search-manufacturer").val();
        var features = $("#search-features").val();
        var price = $("#search-price").val();
        var weight = $("#search-weight").val();
        var stockNum = $("#search-stock-num").val();
        var newCheck = $("#search-new").is(":checked");
        var usedCheck = $("#search-used").is(":checked");
        
        //console.log('Type: ' + type + ' Make: ' + make + ' Features: ' + features + ' Price ' + price + ' Weight: ' + weight + ' Stock num: ' + stockNum + ' New:' +  newCheck + ' Used: ' + usedCheck );
        xajax_update_home_search(type, make, features, price, weight, stockNum, newCheck, usedCheck);
        
    });
  



    /**************************************************
     * Detail Page - Update Monthly Payment
     **************************************************/    
    $("#calculateMonthlyPmt").click(function(e){
        //get all values and pass them all over
        var years = $("#formLoanTerm").val();
        var intRate = $("#formAnnualPercentageRates").val();
        var downPayment = $("#formDownPayment").val();
        var price = $("#formPrice").val();
        var stock = $("#stockNum").val();
        
        xajax_update_estimated_monthly_pmt(stock, years, intRate, downPayment, price);
    });

    $(".calculate-payments-btn").click(function(e){
        //get all values and pass them all over
        $("#formLoanTerm").val($(this).data("years"));
        $("#formAnnualPercentageRates").val($(this).data("interest"));
        $("#formDownPayment").val($(this).data("down"));
        $("#formPrice").val($(this).data("price"));
        $("#est-pmt").text($(this).data("emp"));
        $("#stockNum").val($(this).data("stock"));
        
        $('#calculate-payment-popup').foundation('reveal','open');
    });
 
    
    /**************************************************
     * Print button (on compare page and inventory detail page)
     **************************************************/    
    $(".print-this-page").click(function(e){
        e.preventDefault();
        window.print();
    });


    
    /**************************************************
     * Inventory Page - Search Checkbox is checked/unchecked
     **************************************************/
    $(".search-checkbox").change(function(){

        //console.log(' happened ' + $(this).data("cat") + ' and ' + $(this).data("column") );
        
        var checked = false;
        if($(this).is(":checked")){
            checked = true;
        }else{

        }

        //$objResponse->addAssign("inventory-display", "innerHTML", "<img src='".BASE_URL."wp-content/themes/msl/assets/img/green_spinner.gif' />");
        $("#inventory-display").html("<img id='loading-gif' src='https://www.motorsportsland.com/wp-content/themes/msl/assets/img/green_spinner.gif' />");
        xajax_update_inventory($(this).data("cat"),$(this).data("column"),checked);

    });



    /**************************************************
     * Inventory Page - Compare Checkbox is checked/unchecked  (This method makes sure checkboxes are always in the DOM)
     **************************************************/
    $(document).on('change', ".compare-checkbox", function(){
        //console.log(' happened ' + $(this).attr("id") + ' ' + $(this).data("name") + ' ' 
            //+  $(this).data("price") + ' ' +  $(this).data("condition") + ' ' +  $(this).data("year"));
        
        var checked = 0;
        if($(this).is(":checked")){
            //console.log( $(this).attr("id") + ' is checked' );
            checked = 1;
        }else{
            //console.log( $(this).attr("id") + ' is not checked' );
        }
        
        xajax_update_comparison( checked, $(this).attr("id"), $(this).data("name"), $(this).data("price"), $(this).data("condition"), $(this).data("year") );
    });

    
    //this wasn't working (element is not in dom)  //TODO update this to use the method above (it will work even with AJAX)
    /*
    $('.compare-container').on('click', '.remove_comparison', function () {
         console.log("yeahhhh!!! but this doesn't work for me :(");
        
        xajax_update_comparison( 0, $(this).data("stock"), "", "", "", "" );
        
        //remove checkbox
        document.getElementById("compare_" + $(this).data("stock") ).checked = false;
    });
    */


    /**************************************************
     * Inventory Page - Update Details button for admins
     **************************************************/    
    //update the data
    $("#update-inv-details").click(function(e){
        e.preventDefault();
        
        var msrp = $("#formMSRP").val();
        var map = $("#formMAP").val();
        var price = $("#formPrice").val();
        var descr = $("#formDescription").val();
        var manuf = $("#formManuf").val();
        var line = $("#formLine").val();
        var model = $("#formModelS").val();
        var oldModel = $("#edit-title").data("oldmodeledit");
        var stock = $("#edit-title").data("stockedit");
        var year = $("#edit-title").data("yearedit");
        
        xajax_edit_details_db( stock, msrp, map, price, descr, manuf, line, model, year, oldModel );
    });   
    //popup the modal
    $(document).on('click', ".edit-link", function(e){
    // $(".edit-link").click(function(e){
        e.preventDefault();
        
        $("#formModelS").empty();//get rid of this at start (because append will append forever and ever)
        xajax_edit_details_modal( $(this).data("stock"), $(this).data("red")  );
    });          
 
    /**************************************************
     * Inventory Page - Compare Checkbox / Search options are expanded when in tablet/mobile view
     **************************************************/ 
    $("#compare-dropdown").click(function(e){
        e.preventDefault();
        $("#compare-container").toggle();
        if( $("#compare-container").css('display') == "none"){
            //$("#compare-container").addClass( "compare-display" ); //.removeClass( "myClass noClass" )
        }else{
            
        }
    }); 
    $("#search-dropdown").click(function(e){
        e.preventDefault();
        $("#search-accordion-container").toggle();

    });       
    
    $(window).on('resize', function(){
        var win = $(this); //this = window
        //if (win.height() >= 820) { /* ... */ }
        
        if (win.width() >= 986) { 
            $("#compare-container").show();
            $("#search-accordion-container").show();
            //console.log(win.width());
            //xajax_check_session();
        }else{
            //$("#compare-container").hide();
        }
       
        
        //xajax_check_session(win.width());
    });    
    
    //sticky sidebar on inventory page
    $('#inv-sidebar .large-4').theiaStickySidebar({
        //additionalMarginTop : -135
        additionalMarginTop : 190
    });
    

    /**************************************************
     * Compare Page - remove from comparison
     **************************************************/     
    $(".remove-this-stock").click(function(e){
        
        xajax_update_comparison( 0, 'compare_' + $(this).data("stock"), "", "", "", "", true );
        window.location='?act=remove-comparison';
    });  
    

    
    /**************************************************
     * Detail Page Slider
     **************************************************/
    //http://owlgraphic.com/owlcarousel/index.html
    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
 
    sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: true,
            navigationText: [
      //"<i class='fa fa-chevron-left white'></i>",
      //"<i class='fa fa-chevron-right white'></i>"
      "<img src='../wp-content/themes/msl/assets/img/arrow-left.png'>",
      "<img src='../wp-content/themes/msl/assets/img/arrow-right.png'>"
      ],
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
    });
 
      sync2.owlCarousel({
        items : 4,
        itemsDesktop      : [1199,4],
        itemsDesktopSmall : [979,4],
        itemsTablet       : [768,4],
        itemsMobile       : [479,4],
        navigation : true,
        navigationText : ["",""],    
        pagination:false,
        responsiveRefreshRate : 100,
        afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
        }
      });
     
      function syncPosition(el){
        var current = this.currentItem;
        $("#sync2")
          .find(".owl-item")
          .removeClass("synced")
          .eq(current)
          .addClass("synced");
        if($("#sync2").data("owlCarousel") !== undefined){
          center(current);
        }
      }
     
      $("#sync2").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
      });
     
      function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
          if(num === sync2visible[i]){
            var found = true;
          }
        }
     
        if(found===false){
          if(num>sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", num - sync2visible.length+2);
          }else{
            if(num - 1 === -1){
              num = 0;
            }
            sync2.trigger("owl.goTo", num);
          }
        } else if(num === sync2visible[sync2visible.length-1]){
          sync2.trigger("owl.goTo", sync2visible[1]);
        } else if(num === sync2visible[0]){
          sync2.trigger("owl.goTo", num-1);
        }
        
      }
      
  

    
    /**************************************************
     * Detail Page sort Options
     **************************************************/
    //sort and show options on inventory page
    $("#sort-options").change(function(){
       
        var baseUrl = get_base_url();
        window.location=baseUrl +'?sort_option=' + this.value;
    });

    $("#show-options").change(function(){
       
        var baseUrl = get_base_url();
        window.location=baseUrl +'?rows_per_page=' + this.value;
    }); 
   
   
    
    /**************************************************
     * Details page - fix tabs to vertical or horizontal depending on screen width
     **************************************************/
    if ($(".tabs")[0]){
        $(window).bind("resize",function(){
            //console.log($(this).width())
            if($(this).width() <625){
                $('.tabs').addClass('vertical');
            }
            else{
                $('.tabs').removeClass('vertical');
            }
        });
    }
    
    /**************************************************
     * Details page -  set stock id of "email" form (so it can be retrieved in )
     **************************************************/    
    if($('#hidden-stock').length > 0){
        var stockId = getUrlParameter('stock');
        $('#hidden-stock').val(stockId);
    }

    if($('#request-info-popup').length > 0){
        var stockId = getUrlParameter('stock');
        var fullName = $('.contact-us-title').html();
        //console.log(fullName + ' is fullname');
        $('.request-form-stock').val(stockId);
        $('.request-form-fullname').val(fullName);
    }    
    
    
    /**************************************************
     * Generic "contact us" form, but with extra data if on deails page
     **************************************************/   
     if($('#contact-us-popup').length > 0){
         var stockId = getUrlParameter('stock');
         
         if(stockId != "" && stockId != 'undefined'){
             $('.contact-form-stock').val(stockId);
         }
     }

 
    /**************************************************
     * Comparison page -  set stock id of "email" form (so it can be retrieved in email)
     **************************************************/    
    if($('#hidden-stock-comp-1').length > 0){
        //must use ajax because session variables aren't retrievable for JavaScript
        xajax_set_hidden_stock_comparison();
    } 
    

  
            
    /**************************************************
     * Financing form - Remove the additional applicant fields initially 
     **************************************************/ 
    
    $('#formRentOrOwn').change(function () {
        //console.log( $("input[name='formRentOrOwn']:checked").val() );
        if( $("input[name='formRentOrOwn']:checked").val() == "Rent" ){
            $("#formRentOrOwnAmount").attr("placeholder", "Monthly Rent Amount*");
        }else{
            $("#formRentOrOwnAmount").attr("placeholder", "Monthly Mortgage Amount*");
        }
    });
    
    
    $("#additional-application-container").hide();
    var $inputs = $('#additional-application-container :input');
    $inputs.each(function() {
        if($(this).attr('type') == 'text'){
            $(this).val('NA');                  
        }
    }); 
            

    //Show the text field only when the third option is chosen
    $('#formAddCoApplicant').change(function () {

        var atLeastOneIsChecked = $('input[name="coApplicant[]"]:checked').length > 0;
        //console.log('this is atleastoneischecked: ' + atLeastOneIsChecked);        
        
       
        if (atLeastOneIsChecked ) {
            
            //$("#additional-application-container").append(fields);
            
            //console.log('checked!');
            $("#additional-application-container").show();

            var values = {};
            $inputs.each(function() {
                //console.log($(this).attr('type'));
                if($(this).attr('type') == 'text'){
                    $(this).val('');                  
                }
            });

        } else {
            
            //$("#additional-application-container").html(''); 
            
            //console.log('unchecked!');
            $("#additional-application-container").hide();

            var values = {};
            $inputs.each(function() {
                if($(this).attr('type') == 'text'){
                    $(this).val('NA');                  
                }
            }); 
        }
    });

    
    
    
    /**************************************************
     * Menu hack to add infinite background and to fix green menu button in firefox
     **************************************************/
    var FIREFOX = /Firefox/i.test(navigator.userAgent);
    
    if (FIREFOX) {
      //document.getElementById("menu-green-button").style.display="none";
      //document.getElementById("green-background").style.cssText = 'bottom:-41px !important;';
    }
    
    //mega menu - faux forever menu
    $('.mega-sub-menu').prepend($('<div id="full-width-mega"></div>'));
    //if($('.mega-sub-menu').find('.menu-green-button').length != 0){
    //    $('.menu-green-button').after($('<div class="green-background"></div>'));
    //}    
     
   
    /**************************************************
     * Mega menu affects
     **************************************************/    
    //used on mega menu to highlight background box of image
    $('.menu-text').hover(function(){
        //console.log('hover menu-text' + this);
        $(this).parent().prev('.menu-img-container').css('background-color', '#e1e1e1');
    },function(){
        $('.menu-img-container').css('background-color', '');
    });


    //darken the background when any menu opens, lighten it again when all of them are closed
    jQuery('#mega-menu-item-99').on('close_panel', function() {
        //console.log('Sub menu for item 99 closed');
        $('#menu-dark-bg').css('display', 'none');
    });
    jQuery('#mega-menu-item-98').on('close_panel', function() {
        //console.log('Sub menu for item 98 closed');
        $('#menu-dark-bg').css('display', 'none');
    });
    jQuery('#mega-menu-item-97').on('close_panel', function() {
        //console.log('Sub menu for item 97 closed');
        $('#menu-dark-bg').css('display', 'none');
    });
    
    jQuery('li.mega-menu-item').on('open_panel', function() {
        //console.log('Sub menu opened');
        $('#menu-dark-bg').css('display', 'block');
    });
        



    /**************************************************
     * Phone and Form optimization script - works with custom post type "custom phone numbers"
     **************************************************/   
    var cusName = "moto";
    var phoneCookie = getCookie(cusName + "-phoneNumber");
    var sourceCookie = getCookie(cusName + "-source");

    if(phoneCookie !== "" && phoneCookie.length > 0){
        var newPhone = decodeURIComponent(phoneCookie);
        newPhone = newPhone.replace('+',' ');
        //$('.phone-number').html(newPhone);
        var text = '<a  href="tel:' + newPhone + '">' + newPhone + '</a>';
        $(".phone-number").html(text);        
        $('.utm-source').val(sourceCookie);
    }
    
    //console.log( phoneCookie + ' and ' + sourceCookie + ' and ' + window.location.hostname );   
    
   
}); 




function getCookie(cname) {
    var theCookie = "";
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)===' '){
            c = c.substring(1);
        } 
        if (c.indexOf(name) === 0) {
               theCookie = c.substring(name.length,c.length);
        }
    }
    return theCookie;
}


function get_base_url(){
    var re = new RegExp(/^.*\//);
    var baseUrl = re.exec(window.location.href);
    baseUrl = baseUrl[0];
    if (/page/.test(baseUrl)){
        baseUrl = baseUrl.split('/page')[0];
    }
    //console.log(baseUrl);   
    
    return baseUrl;    
}



function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}


/**************************************************
 * FOOTER  - Used to keep footer bars same length
 **************************************************/  
var rvsContainer = document.getElementById('rvs-container');
var specialsContainer = document.getElementById('specials-container');

specialsContainer.style.height = (window.getComputedStyle) ?
    window.getComputedStyle(rvsContainer, null).height :
    rvsContainer.currentStyle.height;

