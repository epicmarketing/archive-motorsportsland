<?php

//AJAX class: May call addConfirmCommands, addAssign, addAppend, addPrepend, addReplace, addClear, addAlert, addRedirect, addScript, addScriptCall, addRemove
//addCreate, addInsert, addInsertAfter, addCreateInput, addInsertInput, addInsertInputAfter, addEvent, addHandler, addRemoveHandler, addIncludeScript, getXML, loadXML

session_start();

 /**
 * test => I've been using this function for years to test that AJAX is working
 * @var $in_divid = return response to the chosen div
 * @var $in_text = the text to display
*/
function test($in_divid, $in_text){

    $objResponse = new xajaxResponse();
        
    //$objResponse->addAlert("$in_divid should be updated with $in_text");//for testing
    
    $objResponse->addAssign($in_divid, "innerHTML", "$in_text");
    
    return $objResponse;
}



/**
 * update_inventory
 * @var $in_column : the type, as in condition, type, manufacturer, feature, price, weight
 * @var $in_value : new or used for condition, folding trailer etc. for type, etc.
 * @var $in_checked : true or false value designating whether or not checkbox is checked or not
 * @return updates inventory details, pagination, counts and results text
 */
function update_inventory($in_column, $in_value, $in_checked){
        
    $objResponse = new xajaxResponse();
    
    $api = new msl_api();
    $actualLink = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url = parse_url($actualLink);
    
    //on specials page, reset SESSION, and pass it along each time
    $hasSpecials = false;
    $specialClass = $api->fetch_manager_specials();
    if(preg_match("/specials/",$actualLink)){
        $hasSpecials = true;
    }
        
    //update session so that fetch_inventory will be accurate
    $_SESSION['where'][$in_column][$in_value] = $in_checked;
        
    //fetch updated rows and counts
    $rows = $api->fetch_inventory($hasSpecials, "", "");
    $counts = $api->fetch_counts($rows['rows']);
    
    //Motor Sportsland update to the way counts work
    $counts = $api->fetch_updated_counts($counts, $hasSpecials);
       
    //update counts or hide empty values
    foreach($counts as $cat => $countsArr){
        foreach($countsArr as $k => $v){
            if($cat != "types" && $cat != "thecondition" && $v == 0){
                $objResponse->addScript("$(\"[id='$k-container']\").addClass('hide-check');");
                //$objResponse->addScript("console.log('#$k-container')");//for testing  
            }else{
                $objResponse->addScript("$(\"[id='$k-container']\").removeClass('hide-check');");
                //$objResponse->addScript("console.log('#$k-container should not have class')");//for testing  
            }
            $objResponse->addAssign("$cat-$k-count", "innerHTML", "(".$v.")");
            
        }
    }

    //update the URL Reset pagination if needed
    if( (preg_match("/page/",$actualLink)) || isset($url['query']) ){
        $objResponse->addScript(" window.history.pushState('page1', 'RV Inventory | Motor Sportsland', '".BASE_URL."rvs/');");
    }
    
    //build pagination details
    $rowCount = sizeof($rows['rows']);
    $paginationArr = $api->build_pagination($rowCount);
    
    //update result count
    $resultText = ($paginationArr['start'] + 1).'-'.$paginationArr['end'].' of '.$rowCount;
    $objResponse->addAssign("results-top", "innerHTML", $resultText);
    $objResponse->addAssign("results-bottom", "innerHTML", $resultText);
    
    //build and display inventory
    $display = $api->build_inventory($rows['rows'], $paginationArr['start'], $paginationArr['end'], $specialClass);
    if( empty($display) ){
        $display = "Sorry, your search did not return any results.  Please refine your search and try again.";
    }    
    $objResponse->addAssign("inventory-display", "innerHTML", "$display");
    
    //update pagination (starting over with 0)
    $pagination = paginate_links_custom($paginationArr['args']);
    $objResponse->addAssign("pagination-inventory", "innerHTML", "$pagination");     

    //for testing
    //$countPrintout = "<div class='red'>Counts:</div><pre>".print_r($counts,1)."</pre>";     
    $test = $rows['query'];
    $objResponse->addAssign("ajax-test", "innerHTML", "$test ");//<br /><br />$countPrintout for testing 
    //$objResponse->addScript("console.log('$in_column $in_value $in_checked')");//for testing    
    
    return $objResponse;    
}


/**
 * update_home_search => Home Page Search Functionality
 * @var $in_type : types, such as travel trailer, fifth wheel, etc.
 * @var $in_make : makes, such as starcraft, crossroads, etc.
 * @var $in_features : features, such as bunks, front bedroom, etc.
 * @var $in_price : the price range of the unit
 * @var $in_weight : the weight range of the unit
 * @var $in_stockNum : a specific stock number
 * @var $in_newCheck : true false boolean for new
 * @var $in_usedCheck : true false boolean for used
 * Updates the result count on the home page, and prepare the WHERE session variable which is used to populate check boxes on the inventory page
 */
function update_home_search($in_type, $in_make, $in_features, $in_price, $in_weight, $in_stockNum, $in_newCheck, $in_usedCheck){
    
    $objResponse = new xajaxResponse();
    
    $in_type = esc_html($in_type);
    $in_make = esc_html($in_make);
    $in_price = esc_html($in_price);
    $in_weight = esc_html($in_weight);
    $in_stockNum = esc_html($in_stockNum);


    //Reset the "WHERE" session variable, and then rebuild it
    $_SESSION['where'] = array();
    
    //types
    if( !empty($in_type) ){
        $_SESSION['where']['types'] = array($in_type => true);
    }

    //make
    if( !empty($in_make) ){
        $_SESSION['where']['make'] = array($in_make => true);
    }

    //price
    if( !empty($in_price) ){
        $_SESSION['where']['price'] = array($in_price => true);
    }

    //weight
    if( !empty($in_weight) ){
        $_SESSION['where']['weight'] = array($in_weight => true);
    }
    
    //new checkbox
    if($in_newCheck == "true"){
        $_SESSION['where']['thecondition']['new'] = true;
    }
    
    //used checkbox
    if($in_usedCheck == "true"){
        $_SESSION['where']['thecondition']['used'] = true;
    }
    
    //features
    /* put this back if multiselect box is added back again
    $throwAway = (empty($in_features[0]) ? array_shift($in_features) : "");//stupid empty value of "features" empty option requires this
    if( !empty($in_features) && !empty($in_features[0]) ){//empty features still sets up an array with first value as empty
        foreach($in_features as $k => $v){
           
            $v = esc_html($v);
            $_SESSION['where']['features'][$v] = true;                
        
        }
    }
     */
    if( !empty($in_features) ){
        $_SESSION['where']['features'][$in_features] = true;     
    }
      

    $stockArr = array();
    $hasValidStock = false;
    if( is_numeric($in_stockNum) && strlen($in_stockNum) == 5 ){
        $stockArr[0] = $in_stockNum;
        $hasValidStock = true;
    }
    
    //$test = "<pre>".print_r($in_features,1)."</pre>";

    //Get current results and update the display
    $api = new msl_api();
    $rows = $api->fetch_inventory("", $stockArr, "");
    $rowCount = sizeof($rows['rows']);
    
    //update button for stock number, if needed
    if($hasValidStock == true && $rowCount == 1 ){
        //$objResponse->addScript(" $('#search-inventory-button').attr('href', '".BASE_URL."rvs/?stock=$in_stockNum');" );
        $objResponse->addRedirect( BASE_URL."rvs/?stock=$in_stockNum");
    }else{
        $objResponse->addScript(" $('#search-inventory-button').attr('href', '".BASE_URL."rvs/');" );
    }
    
    $objResponse->addAssign("results-home", "innerHTML", "$rowCount Results");

    return $objResponse;       
}


/**
 * update_comparison
 * @var $in_check : Boolean => Did the user check or uncheck the check box? 1 == checked
 * @var $in_compareStock : Stock Id
 * @var $in_name : The title (name) of the unit being compared, which includes make, year, line, etc.
 * @var $in_price: the price of the unit being compared
 * @var $in_conditon: the condition, new or used, of the unit being compared
 * @var $in_year : the year of the unit being compared
 * @var $in_comparisonPage : Boolean => designates 1 if on comparison page, otherewise the user is on the inventory page
 * @return  Adds or removes the checked/unchecked unit from the comparison Session variable
 */
function update_comparison ($in_check, $in_compareStock, $in_name, $in_price, $in_condition, $in_year, $in_comparisonPage = false){
    
    $objResponse = new xajaxResponse();
    
    $api = new msl_api();
    
    //$_SESSION['compare'] = "";//resets teh session, useful if testing
    
    //initialize compare session in needed
    if( !isset($_SESSION['compare']) ){
        $_SESSION['compare'] = "";
    }

    $idParts = explode("_",$in_compareStock);
    $stockNum = $idParts[1];
    $changed = false;

    if($in_check == 1){//checked

        //if checked, then add to the next available slot
        $added = false;
        for($x = 0;$x < 4;$x++){
            if( !isset($_SESSION['compare'][$x]) || empty ($_SESSION['compare'][$x]) ){
                $_SESSION['compare'][$x] = $stockNum;
                $_SESSION['compare_data'][$x]['name'] = $in_name;
                $_SESSION['compare_data'][$x]['price'] = $in_price;
                $_SESSION['compare_data'][$x]['condition'] = $in_condition;
                $_SESSION['compare_data'][$x]['year'] = $in_year; 
                $_SESSION['compare_data'][$x]['stock'] = $stockNum;                
                $added = true;
                $changed = true;
                break;//break out of loop
            }
        }

            
        $errorMsg = "";
        //if there are already 4 slots, then return error and uncheck the box.
        if($added == false){
            $errorMsg = "You can only compare 4 at a time";
            //uncheck the box
            $objResponse->addScript("alert('$errorMsg');");
            $objResponse->addScript("document.getElementById(\"$in_compareStock\").checked = false;");
        }

        //for testing
        //$session = "<pre>".print_r($_SESSION['compare'], 1)."</pre><pre>".print_r($_SESSION['compare_data'], 1)."</pre>";//for testing
        //$objResponse->addAssign("compare-error", "innerHTML", "$in_check, $in_compareStock, $in_name, $in_price <br /> $session<br />$errorMsg");
            
    }elseif($in_check == 0){//unchecked

        //if unchecked, then remove and slide up all other sessions 
        for($x = 0;$x < 4;$x++){
            if( isset($_SESSION['compare'][$x]) &&  ($_SESSION['compare'][$x] == $stockNum) ){
                unset($_SESSION['compare'][$x]);
                unset($_SESSION['compare_data'][$x]);
                $changed = true;
                break;//break out of loop
            }
        }     
        
        //bump up order 
        $_SESSION['compare'] = array_values($_SESSION['compare']);
        $_SESSION['compare_data'] = array_values($_SESSION['compare_data']);
        
        //just in case (uncheck that box if not on comparison page)
        if($in_comparisonPage == false){
            $objResponse->addScript("document.getElementById(\"$in_compareStock\").checked = false;");
        }
        
        //for testing
        //$session = "<pre>".print_r($_SESSION['compare'], 1)."</pre>";//for testing  
        //$objResponse->addAssign("compare-error", "innerHTML", "$in_check == 0 and $in_compareStock <br /> $session");  
    }
    
    //check to see if there is one checkbox checked
    $atleastOneChecked = false;
    if(isset($_SESSION['compare']) && !empty($_SESSION['compare']) ){
        foreach($_SESSION['compare'] as $num => $stock){
            if( is_numeric($stock) ){
                $atleastOneChecked = true;
            }
        }        
    }

    //update the contents if a change was made (and there is still something to show)
    if($changed && $in_comparisonPage == false){ //&& $atleastOneChecked


        $comparison = $api->build_comparison();
    
        if( empty($comparison) ){
            $comparison = 'Check "compare" check boxes to view comparisons.<br /><br />';
        }
        
        $objResponse->addAssign("compare-content", "innerHTML", "$comparison");
    }
   

    //if there is at least one checked, then display compare container
    if($atleastOneChecked){
        //$objResponse->addScript(" $('#compare-container').show();");
    }else{
        //$_SESSION['compare_data'] = array();
        //$comparison = $api->build_comparison();
        //$objResponse->addScript(" $('#compare-container').hide();");
    }
    
    
    return $objResponse;  
    
}



/**
 * check_session : I don't think this is actually being used...
 */
function check_session($in_windowWidth){
              
    $objResponse = new xajaxResponse();    
      
        
    $atleastOneChecked = false;
    if(isset($_SESSION['compare']) && !empty($_SESSION['compare']) ){
        foreach($_SESSION['compare'] as $num => $stock){
            if( is_numeric($stock) ){
                $atleastOneChecked = true;
            }
        }        
    }

    if($atleastOneChecked && $in_windowWidth > 985){
        //$objResponse->addScript(" $('#compare-container').show();");
        //$objResponse->addScript(" console.log(' compare container is shown')");
    }else{
        
    }

    return $objResponse; 
    
}



/**
 * set_hidden_stock_comparison : if a user submits an email, this fills in the hidden form fields with stock numbers
 */
function set_hidden_stock_comparison(){
    
    $objResponse = new xajaxResponse();  
    
    $count = 1;
    if(isset($_SESSION['compare'])){
        foreach($_SESSION['compare'] as $k => $v){
            $objResponse->addScript(" $('#hidden-stock-comp-$count').val($v);");
            $count++;
        }
    }
    
    return $objResponse; 
}



/**
 * update_estimated_monthly_pmt
 * @var $in_stock : The stock number of the unit being estimated
 * @var $in_years : the year of the unit being estimated
 * @var $in_intRate : the interest rate of the unit being estimated
 * @var $in_downPayment : the down payment of the unit being estimated
 * @var $in_price : the price of the unit being estimated
 * @return Updates the estimated monthly payment number based on the numbers the user inputs
 */
function update_estimated_monthly_pmt($in_stock, $in_years, $in_intRate, $in_downPayment, $in_price){
    
    $objResponse = new xajaxResponse();
    
    //only calculate if the user has all numbers (otherwise, show error)
    if(is_numeric($in_years) &&  is_numeric($in_intRate) && is_numeric($in_downPayment) && is_numeric($in_price)){
        $api = new msl_api();
        $estMonthlyPayment = $api->calcPay($in_price, $in_years, $in_intRate, $in_downPayment);
        $estMonthlyPayment =  number_format ( round($estMonthlyPayment,2),2); 
        
        //$objResponse->addScript(" console.log('$in_stock, $in_years, $in_intRate, $in_downPayment, $in_price')");//For testing
        $objResponse->addAssign("est-pmt-error", "innerHTML", ""); 
        $objResponse->addAssign("est-pmt", "innerHTML", "$".$estMonthlyPayment);
        
        //update the data attributes so that if the modal is closed and then opened again, the values are still there (on refresh they are lost)
        $objResponse->addScript("$('#calculate-$in_stock').data('years', '$in_years');");
        $objResponse->addScript("$('#calculate-$in_stock').data('interest', '$in_intRate');");
        $objResponse->addScript("$('#calculate-$in_stock').data('down', '$in_downPayment');");
        $objResponse->addScript("$('#calculate-$in_stock').data('price', '$in_price');");
        $objResponse->addScript("$('#calculate-$in_stock').data('emp', '$estMonthlyPayment');");
        
        
    }else{
        $objResponse->addAssign("est-pmt-error", "innerHTML", "Please check your values and try again.");    
    }

    
    return $objResponse; 
}



/**
 * edit_details  (available to admins only - updates price, MRSP, descr, and fixes yellow problems if needed)
 * @var $in_stock : the stock number of the unit being edited
 * @var $in_red : red is actually now yellow, but this designates if the unit needs additional edit fields
 */
function edit_details_modal($in_stock, $in_red){
    
    $objResponse = new xajaxResponse();  
    
    $api = new msl_api();
    
    $stockOverviewRows = $api->fetch_inventory(false,array(0=>$in_stock));
    $stockOverview = array_shift($stockOverviewRows['rows']);//strip off the "0" since there is only one row and not multiple "rows"
    $stockSpecs = $api->fetch_specs($stockOverview['year'],$stockOverview['make'],$stockOverview['line'],$stockOverview['model']);  
    $stockSpecsRows = $stockSpecs['rows'];
    
    $stockTitle = $api->build_inventory_title($stockOverview['thecondition'], $stockOverview['year'], $stockOverview['make'], $stockOverview['line'], $stockOverview['model']);
  
    
    //title
    $year = $stockOverview['year'];
    $oldModel = $stockOverview['model'];
    $objResponse->addAssign("edit-title", "innerHTML", "Edit Details for $in_stock<br />$stockTitle");
    $objResponse->addScript("$('#edit-title').data('stockedit', '$in_stock');");
    $objResponse->addScript("$('#edit-title').data('yearedit', '$year');");
    $objResponse->addScript("$('#edit-title').data('oldmodeledit', '$oldModel');");
    $objResponse->addAssign("updated-msg", "innerHTML", "");
    
    //Pricing, Description and MAP (whatever that is)
    $objResponse->addAssign("formMSRP", "value", $stockOverview['msrp']);
    $objResponse->addAssign("formMAP", "value", "needed?");
    $objResponse->addAssign("formPrice", "value", $stockOverview['price'] );
    $objResponse->addAssign("formDescription", "value", $stockOverview['description']);
    
    //floorplan
    if($in_red == 1){//show floorplan data if red (no specs matching up)
        $objResponse->addScript(" $('#edit-floor-plan-container').show();");
    
        $objResponse->addAssign("formManuf", "value", $stockOverview['make']);
        $objResponse->addAssign("formLine", "value", $stockOverview['line']);
        
        $floorplansResults = $api->fetch_model_floorplans("", $stockOverview['year'], $stockOverview['make'], $stockOverview['line']);
        
        
        $options = "<option value=''></option>";
        foreach($floorplansResults['rows'] as $k => $v){
            $options .= "<option value='$v[floorplan]'>$v[floorplan]</option>";
        }
        
        //*add WAF
        //$floorplansResultsWaf = $api->fetch_lines_waf($stockOverview['year'], $stockOverview['make'], $stockOverview['line']);
        //$options .= "<option value=''>WAF COMES NEXT!</option>";
        //foreach($floorplansResultsWaf['rows'] as $k => $v){
        //    $options .= "<option value='$v[waf_model]'>$v[waf_model]</option>";
        //}
        
        //$objResponse->addAssign("formModel", "value", "$options");    
        $objResponse->addAppend("formModelS", "innerHTML", "$options"); 
    
    }else{
        //hide
        $objResponse->addScript(" $('#edit-floor-plan-container').hide();");
    }
    
    
    $objResponse->addScript(" $('#edit-details-popup').foundation('reveal', 'open');");
    
    
    //$objResponse->addScript(" $('#formMSRP').val($in_stock);");
    return $objResponse; 
}


/**
 * Edit Details DB => Insert or update the DB with edited details (updating inventory details)
 * @var $in_stock : The stock number of the inventory item being updated
 * @var $in_msrp : The MSRP being updated
 * @var $in_map : The Map being updated (not used)
 * @var $in_price : The Price being updated
 * @var $in_descr : THe description being udated
 * @var $in_manuf : The Manuf
 * @var $in_line : The line
 * @var $in_model : The model being updated
 * @return Updates the database for newinv_info and/or for model_aliases
 */
function edit_details_db($in_stock, $in_msrp, $in_map, $in_price, $in_descr, $in_manuf, $in_line, $in_model, $in_year, $in_oldModel ){
        
    $objResponse = new xajaxResponse();  
    
    $api = new msl_api();
    
    $in_map = 0;
    
    //$objResponse->addScript(" console.log('$in_stock, $in_msrp, $in_map, $in_price, $in_descr, $in_manuf, $in_line, $in_model, $in_year')");//for testing
    
    //standard update
    $api->update_inventory_details($in_stock, $in_msrp, $in_map, $in_price, $in_descr);
    
    //alias update
    if( !empty($in_model) ){
        $api->update_alias_details($in_manuf, $in_line, $in_model, $in_year, $in_oldModel);
    }
    
    $objResponse->addAssign("updated-msg", "innerHTML", "Updated.");
    
    return $objResponse; 
}



/**
 * IMPORTANT - if a function is added above, it must be registered here!
 */
function myplugin_xajax(){
    global $xajax;
    $xajax->registerFunction("test");
    $xajax->registerFunction("update_inventory");
    $xajax->registerFunction("update_comparison");
    $xajax->registerFunction("check_session");
    $xajax->registerFunction("edit_details_modal");
    $xajax->registerFunction("edit_details_db");
    $xajax->registerFunction("set_hidden_stock_comparison");
    $xajax->registerFunction("update_home_search");
    $xajax->registerFunction("update_estimated_monthly_pmt");
}

add_action('init','myplugin_xajax');
?>