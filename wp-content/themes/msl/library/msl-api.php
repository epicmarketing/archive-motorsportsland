<?php 


//increase max size
//ini_set("memory_limit","3072M");
//ini_set('max_execution_time','1800');

/**
 * 
 */
class msl_api {
    
    
    private $db = "";
   
    /**
     *  Class constructor / 
    */
    public function __construct() {
    
        global $wpdb;
        $this->db = $wpdb;  

    }



    /**
     * Reset all POST and SESSION variables
     */
    public function reset_everything(){
        $_SESSION = array();
        unset($_POST);
    }



    /**
     * fetch_parts_specials => This function uses the default WordPress WP_Query with $args to get the custom post type
     * @return all "parts specials" custom post types that are "published"
     */
    public function fetch_part_specials(){
        
                               
        $args = array(
            'post_type' => 'part_specials', 
            'posts_per_page' => 100,
            'post_status' => 'publish'
        );
        $loop = new WP_Query( $args );
        //$partSpecials = array();
        //while ( $loop->have_posts() ) : $loop->the_post();
            
            //$partSpecials['title'] = get_the_title();
            //$otherCustom = get_field( 'future_field' ); 

        //endwhile;
        wp_reset_query();
        
        return $loop;
    }


    /**
     * fetch_manager_specials => This function uses the default WordPress WP_Query with $args to get the custom post type
     * @return all "manager special" custom post types 
     */
    public function fetch_manager_specials(){
        
                               
        $args = array(
            'post_type' => 'manager_special', 
            'posts_per_page' => 100,
            'post_status' => 'publish'
        );
        $loop = new WP_Query( $args );
        $stocks = array();
        while ( $loop->have_posts() ) : $loop->the_post();
            
            $stocks[] = get_the_title();
            //$otherCustom = get_field( 'future_field' ); 

        endwhile;
        wp_reset_query();
        
        return $stocks;
    }



    /**
     * Fetch_types_specials => Gets oldest two of each type
     * @var $in_stock : the stock number we are trying to find similar inventory items
     * @return array of stock numbers that are similar (same type but not same make, using comparable features if possible)
     */
    public function fetch_similar_listings($in_stock){

        $query = "
            SELECT types, make 
            FROM newinv 
            WHERE stock = %d
        ";    
        $typeQ = $this->db->prepare($query, $in_stock);

        $currentStockResult = $this->db->get_results($typeQ, ARRAY_A );//return as array
        $currentStock = array_shift($currentStockResult);//remove 0
        
        $rowCount = 0;
        if(isset($_SESSION['where']['features'])){
                        
            $whereParts = array();    
            foreach($_SESSION['where']['features'] as $k => $v){
                if($v == "true"){
                    $k = esc_sql($k);//escape this, unknown number of query items coming from a session
                    $whereParts[] = "$k = '1'";
                }
            }            
            $where = implode(" OR ", $whereParts);
            $where = (!empty($where) ? $where : "1 = 1");
            
            //could also be based off model, year, or line, but currently based off "make"
            $andWhere = " AND (ni_v.types = '$currentStock[types]' AND ni_v.make != '$currentStock[make]') ";//doesn't need escaping
            
            $similarFeaturesQ = "                
                SELECT ni_v.stock
                FROM newinv_view ni_v 
                LEFT JOIN weights_and_features waf ON ni_v.year = waf.waf_year AND ni_v.make = waf.waf_make AND ni_v.line = waf.waf_line 
                    AND (ni_v.model = waf.waf_model OR ni_v.web_model = waf.waf_model  )
                WHERE $where $andWhere";
            $simlarResults = $this->db->get_results($similarFeaturesQ, ARRAY_A );//return as array
            $rowCount = sizeof($simlarResults);
        }
        

        //try again if above query did not work, but keep it simple (no features matching)
        if($rowCount < 3){

            $query = "
               SELECT stock 
               FROM newinv_view ni_v 
               WHERE ni_v.types = %s AND ni_v.make != %s
            ";    
            $similarQ = $this->db->prepare($query, $currentStock['types'], $currentStock['make']);
            $simlarResults = $this->db->get_results($similarQ, ARRAY_A );//return as array 
        }
        
        return $simlarResults;
    }
    
    

    /**
     * Fetch Color => Gets color from newinv_view
     * @var $in_stock : the stock number we are trying to find color for
     * @return color
     */
    public function fetch_color($in_stock){
           
        
        $query = "
           SELECT color 
           FROM newinv_view ni_v 
           WHERE ni_v.stock = %d
        ";    
        $colorQ = $this->db->prepare($query, $in_stock);
        $colorResult = $this->db->get_results($colorQ, ARRAY_A );//return as array 
       
        
        return $colorResult;
    }    
    
    


    /**
     * Fetch_types_specials => Gets oldest two of each type
     * @return array of oldest two inventory of each type
     */
    public function fetch_types_specials(){
        
        $sRows = array();
            
        //get all distinct types
        $typesQ = "SELECT DISTINCT types FROM newinv_view;";
        $typesResults = $this->db->get_results($typesQ, ARRAY_A );//return as array
        
        //cycle through each type and get last two of each type dynamically
        $specialParts = array();
        $a = 1;
        foreach($typesResults as $k => $v){
            $specialParts[] = 
                "(SELECT ni_v.stock, ni_v.types, ni.datesl 
                FROM newinv ni 
                INNER JOIN newinv_view ni_v ON ni_v.stock = ni.stock
                WHERE ni_v.types = '$v[types]' 
                ORDER BY ni.datesl LIMIT 2) AS a$a";
            $a++;
        }
        
        //build the query
        $specialsInnerQueries = implode(" UNION ALL  SELECT stock, types, datesl FROM ",$specialParts);
        
        $specialQuery = "SELECT stock, types, datesl FROM $specialsInnerQueries  ORDER BY types, datesl;";       
        
        $specialResults = $this->db->get_results($specialQuery, ARRAY_A);//this query is special ;)
        
        $sRows['specialQuery'] = $specialQuery;
        $sRows['specialRows'] = $specialResults;
        
        
        return $sRows;
    }


    /**
     * fetch_inventory 
     * search values specified by the user (types, make, model, condition, etc)
     * @var $in_hasSpecial : Boolean => get specials if on the special page
     * @var $in_stockList : pull inventory only if in this array
     * @var $in_customWHere : overwrite the where with this custom query (set to 1 to grab everything, which sets WHERE 1)
     * @var $in_skip : skip this part of WHERE statement (used for fetch_counts)
     * @return $result : an array of all inventory rows, both new and used by default, but takes where conditions specified by search
     */
    public function fetch_inventory ($in_hasSpecial = false, $in_stockList = array(), $in_customWhere = "", $in_skip = ""){
        
         //return this result arraay
         $result = array();

         //build the "WHERE" part of the query
         $where = "1";
         
         //to hold each of the separate parts of the query
         $wherePart = array();
         

         if(isset($_SESSION['where'])){
             $type = array();
             
             foreach($_SESSION['where'] as $category => $catArr){
                 if($category == $in_skip){
                    continue;//skip this one since the category = the category to be skipped
                 }
                 $atLeastOne = false;
                 $type = array();
                 foreach($catArr as $k => $v){
                     if($v == "true"){
                         $atLeastOne = true;
                         if($category == "price"){
                             if($k == "15000"){
                                 $type[] = "($category <= '15000')";
                             }elseif($k == "30000"){
                                 $type[] = "($category <= '30000' AND $category > '15000')";
                             }elseif($k === "90000+"){
                                 $type[] = "($category > '90000')";
                             }else{
                                 $lowerBound = $k - 30000;
                                 $type[] = "($category <= '$k' AND $category > '$lowerBound')";
                             }
                         }elseif($category == "weight"){
                             if($k == "3500"){
                                 $type[] = "(waf_unloaded_vehicle_weight <= '3500')";
                             }elseif($k == "5500"){
                                 $type[] = "(waf_unloaded_vehicle_weight <= '5500' AND waf_unloaded_vehicle_weight > '3500')";
                             }elseif($k == "8500"){
                                 $type[] = "(waf_unloaded_vehicle_weight <= '8500' AND waf_unloaded_vehicle_weight > '5500')";    
                             }else{
                                 $type[] = "(waf_unloaded_vehicle_weight > '8500')";
                             }
                         }elseif($category == "features"){//"features" is the only category that could be "AND" instead of "OR" (if so, then put in different array)
                             $type[] = esc_sql($k)." = '1'";
                         }else{
                             $type[] = esc_sql($category)." = '".esc_sql($k)."'";
                         }
                         
                     }
                     
                 }
                 if($atLeastOne == true){
                     $wherePart[] = "(".implode(" OR ", $type).")";
                 }
             }
         }
        
         
         //combine all parts with "AND"
         if(!empty($wherePart)){
            $where = implode(" AND ", $wherePart); 
         }
         
         
         
         //build the Order By (no need for sanitation since I'm checking specific values)
         $orderBy = "thecondition, make, line, year, model";//default sort
         if(isset($_SESSION['sort_option']) && !empty($_SESSION['sort_option'])){
                 
             if($_SESSION['sort_option'] == "price-lowest"){
                 $orderBy = " price, ".$orderBy;
             }elseif($_SESSION['sort_option'] == "price-highest"){
                 $orderBy = " price DESC, ".$orderBy;
             }elseif($_SESSION['sort_option'] == "year-oldest"){
                 $orderBy = "year, ".$orderBy;
             }elseif($_SESSION['sort_option'] == "year-newest"){
                 $orderBy = "year DESC, ".$orderBy;
             }elseif($_SESSION['sort_option'] == "manufacturer-a"){
                 $orderBy = "make, ".$orderBy;
             }elseif($_SESSION['sort_option'] == "manufacturer-z"){
                 $orderBy = "make DESC, ".$orderBy;
             }
 
         }



        //get all stock numbers from specials (the oldest two records of each "types" plus manager specials)
        $managerSpecial = array();
        $specialStocksQuery = ""; 
        if($in_hasSpecial){
            
            $specialRows = $this->fetch_types_specials();
            

            $specialStocks = array(); 
            
            foreach($specialRows['specialRows'] as $k => $v){
                $specialStocks[] = $v['stock'];
            }
            
            //add manager special
            $managerSpecial = $this->fetch_manager_specials();
            
            $specialStocks = array_merge($managerSpecial,$specialStocks);//important to put manager specials first
            $specialStocksList = implode(",", $specialStocks);
            $specialStocksQuery = " AND stock IN ($specialStocksList) ";    
            
            //process the results to just get the stock numbers, to add to the "WHERE" clause of the main query below
            $where .= "$specialStocksQuery";
            //$orderBy = " FIELD(stock,$specialStocksList) ";//overwrite //TODO uncomment this to put specials on top
        }
        
        
        //custom WHERE based on an array of stock IDs (overwrites standard where)
        if( !empty($in_stockList) ){
            $stockList = implode(",", $in_stockList);
            $where = " stock IN ($stockList)";   
        }
     
        if( !empty($in_customWhere) ){
            $where = $in_customWhere;
        }   
     
        //http://stackoverflow.com/questions/7407864/mysql-select-union-for-different-columns 
        //Including the model alias table only adds a couple of records, but makes the query take 2x longer to run
        //Another problem, using regex only works in "WHERE" statement, so it doesn't work on JOIN (making it useless here) - Also, very slow performance...
        //The only thing that will work is to create a user defined function, which will slow performance
        //Or, the easiest thing (and fastest performance wise) - put the updated model in a column by itself
        /*
        $query = "
            SELECT *
            FROM (
                SELECT 'Used' as thecondition, u.types, u.year, u.make, u.model, u.line, u.stock, ni_i.msrp, u.price, ni_i.description, waf.*
                FROM Used u 
                LEFT JOIN newinv_info ni_i ON ni_i.stock = u.stock  
                LEFT JOIN weights_and_features waf ON u.year = waf.waf_year AND u.make = waf.waf_make AND u.line = waf.waf_line AND u.model = waf.waf_model
                
                UNION ALL
        
                SELECT 'New' as thecondition, ni_v.types, ni_v.year, ni_v.make, ni_v.model, ni_v.line, ni_v.stock, ni_i.msrp, ni_i.price, ni_i.description, waf.*
                FROM newinv_view ni_v 
                LEFT JOIN newinv_info ni_i ON ni_i.stock = ni_v.stock 
                LEFT JOIN weights_and_features waf ON ni_v.year = waf.waf_year AND ni_v.make = waf.waf_make AND ni_v.line = waf.waf_line 
                    AND (ni_v.model = waf.waf_model  )
            ) a
            
            WHERE $where
            ORDER BY $orderBy        
        ";
         */ 
        
         
         
        $query = "
        SELECT *
            FROM (
                SELECT 'Used' as thecondition, '' as color, u.types, u.year, u.make, u.model, u.line, u.stock, ni_i.msrp, u.price, ni_i.description, waf.*, ma.model_alias, u.toy as used_toy_hauler
                FROM Used u 
                LEFT JOIN newinv_info ni_i ON ni_i.stock = u.stock  
                LEFT JOIN model_aliases ma ON ma.year = u.year AND ma.manuf LIKE u.make AND ma.line LIKE u.line AND ma.model LIKE u.model
                LEFT JOIN weights_and_features waf ON u.year = waf.waf_year AND u.make = waf.waf_make AND u.line = waf.waf_line AND u.model = waf.waf_model
                
                
                UNION ALL
        
                SELECT 'New' as thecondition, ni_v.color as color, ni_v.types, ni_v.year, ni_v.make, ni_v.model, ni_v.line, ni_v.stock, ni_i.msrp, ni_i.price, ni_i.description, waf.*, ma.model_alias, '0' as used_toy_hauler 
                FROM newinv_view ni_v 
                LEFT JOIN newinv_info ni_i ON ni_i.stock = ni_v.stock 
                LEFT JOIN model_aliases ma ON ma.year = ni_v.year AND ma.manuf LIKE ni_v.make AND ma.line LIKE ni_v.line AND ma.model LIKE ni_v.model
                LEFT JOIN weights_and_features waf ON ni_v.year = waf.waf_year AND ni_v.make = waf.waf_make AND ni_v.line = waf.waf_line 
                    AND (ni_v.model = waf.waf_model  OR ma.model_alias = waf.waf_model  )
                
            ) a
            
            WHERE $where
            ORDER BY $orderBy   
        ";//OR ni_i.web_model = waf.waf_model  */

       

        $rows = $this->db->get_results($query, ARRAY_A);
            
        $result['count'] = $this->db->num_rows;//get count of inventory (used for specs_details.php)
        $result['query'] = $query;//return for testing
        $result['rows'] = $rows;//use this 
            
            
        return $result;        
    }


    /**
     * fetch_specs: fetch floorplan, type, year, manufacture data.  This is not stock specific, but rather general data.
     * @var in_year: the year of the vehicle
     * @var in_manufacturer: the make (Crossroads, etc.)
     * @var in_line: the line (Rezerve, etc) - line has brochure, video, and description data
     * @var in_floorplan: the model or floorplan (RTZ26KS, etc.)
     * @return result: return all specs of the year, manufacture, line and floorplan
     */
    public function fetch_specs($in_year, $in_manufacturer, $in_line, $in_floorplan){
                    
      
        $fpnum = array();    
        preg_match('/^\D*(?=\d)/', $in_floorplan, $fpnum);
        $fpnum= ( !empty($fpnum[0]) ) ? substr($in_floorplan,strlen($fpnum[0])) : "$in_floorplan";     
            
        $specsQuery = "
            SELECT u.id as model_id, l.id as line_id, u.floorplan, u.type, u.year as year, m.manufacturer, l.line, 
                l.brochure, l.description, 
                sl.label, sl.units, st.spec_type, us.value, st.sort_order, sl.sort_order 
            FROM `units` u 
            INNER JOIN manufacturers m ON m.id = u.manuf_id 
            INNER JOIN ".DB_NAME.".lines l ON l.id = u.line_id 
            INNER JOIN unit_specs us ON us.unit_id = u.id 
            INNER JOIN spec_labels sl ON sl.id = us.label_id 
            INNER JOIN spec_types st ON st.id = sl.spec_type_id 
            WHERE m.disp =1 AND u.disp =1 AND year=%d AND manufacturer=%s AND line=%s AND floorplan LIKE %s 
            ORDER BY st.`sort_order`, sl.`sort_order`;
        ";    
        $query = $this->db->prepare($specsQuery, $in_year, $in_manufacturer, $in_line, "%$fpnum%");
        
         
        $rows = $this->db->get_results($query, ARRAY_A);
        
        $result['query'] = $query;//return for testing
        
        //if previous query fails, then try alias table
        $result['query-redo']  = '';
        if(empty($rows)){
            
            $aliasQuery = $this->db->prepare("
                select * 
                from `model_aliases` 
                where `year` = %d AND `manuf`= %s AND `line`= %s AND `model`= %s;
            ", $in_year, strtolower($in_manufacturer), strtolower($in_line), $in_floorplan);
            
            $aliasRow = $this->db->get_results($aliasQuery, ARRAY_A);
            
            if(!empty($aliasRow)){
                $aliasModel = $aliasRow[0]['model_alias'];
                $query = $this->db->prepare($specsQuery, $aliasRow[0]['year'], $aliasRow[0]['manuf_alias'], $aliasRow[0]['line_alias'], "%$aliasModel%");
                
                $rows = $this->db->get_results($query, ARRAY_A);    
                
                $result['query-redo'] = $query;         
            }
                 
            $result['query-alias'] = $aliasQuery;//return for testing
            $result['row-alias'] = $aliasRow;
            
            //$rows = $this->db->get_results($query, ARRAY_A);
            
        } 
        
        
        
        $result['rows'] = $rows;//use this 
        
        //Also, fetch videos, if any (using the Motor Sportsland DB logic)
        $modelId = isset($rows[0]['model_id']) ? 'unit_'.$rows[0]['model_id'] : "model_id_not_found";
        $lineId = isset($rows[0]['line_id']) ? 'line_'.$rows[0]['line_id'] : "line_id_not_found";
        $query = $this->db->prepare("
            SELECT youtube_id
            FROM `videos`
            WHERE (`rel_id` = %s AND `year`= %d) OR (`rel_id` = %s AND `year`= %d);
        ", $modelId, $in_year, $lineId, $in_year);

        $rowsV = $this->db->get_results($query, ARRAY_A);
        
        $result['query-video'] = $query;//return for testing
        $result['rows-video'] = $rowsV;//use this          
            
            
        return $result;  
    }


    /**
     * fetch_makes_lines
     * @var $in_type : Class A Gas, Fifth Wheel, etc. (Types are chosen from top menu navigation)
     * @return $result : an array of all manufacturers and lines
     */
    public function fetch_makes_lines($in_type){
                        
        /*
            SELECT un.id, un.year, manuf.manufacturer, `un`.line_id, `lines`.line, un.`type` 
            FROM units as un, manufacturers as manuf , mslweb_new.`lines` as `lines` 
            WHERE un.manuf_id = manuf.id AND un.line_id=`lines`.id AND un.`disp`=1 AND un.`type` LIKE '%$in_type%'
            ORDER BY manuf.manufacturer, un.`year` DESC, `lines`.line;                    
        */                

        if($in_type == "toy hauler"){//this exception pulled directly from loadLines_new.php from old msl site
            
            $query = 
                "SELECT un.id, un.year, manuf.manufacturer, `un`.line_id, `lines`.line, un.`type`, un.floorplan
                FROM ".DB_NAME.".`unit_specs` as `specs`, ".DB_NAME.".units as un, ".DB_NAME.".manufacturers as manuf , ".DB_NAME.".`lines` as `lines` 
                WHERE un.manuf_id = manuf.id AND un.line_id=`lines`.id AND un.`disp`=1 AND un.id=`specs`.`unit_id` AND `specs`.`label_id`=30 
                ORDER BY manuf.manufacturer, un.`year` DESC, `lines`.line";
        }else{
        
            $query = $this->db->prepare("
                SELECT un.id, un.year, manuf.manufacturer, `un`.line_id, `lines`.line, un.`type`, un.floorplan
                FROM units un
                INNER JOIN manufacturers manuf ON un.manuf_id = manuf.id
                INNER JOIN `lines` ON un.line_id=`lines`.id
                WHERE un.`disp`=1 AND un.`type` LIKE %s
                ORDER BY manuf.manufacturer, un.`year` DESC, `lines`.line        
            ", "%$in_type%");
        }    
        
        $rows = $this->db->get_results($query, ARRAY_A);
        
        $result['query'] = $query;//return for testing
        $result['rows'] = $rows;//use this 
            
            
        return $result; 
        
    }
    
    
    
    /**
     * Fetch Model Floorplans: retrieve model/floorplan based on type, year, manufacturer/make, line
     * @var in_type - the type (Class A Gas, Travel Trailer, Folding Trailer, etc.)
     * @var in_year - the year of hte floorplan
     * @var in_manufacturer ()
     * @var in_line 
     */
    public function fetch_model_floorplans($in_type, $in_year, $in_manufacturer, $in_line){
           
        $result = array();      
                    
        $query = $this->db->prepare("
            SELECT un.id, un.year, manuf.manufacturer, `un`.line_id, `lines`.line, un.`type`, un.floorplan
            FROM units un
            INNER JOIN manufacturers manuf ON un.manuf_id = manuf.id
            INNER JOIN `lines` ON un.line_id=`lines`.id
            WHERE un.`disp`=1 AND un.`type` LIKE %s AND line = %s AND year = %d AND manufacturer = %s
            ORDER BY un.floorplan  ASC     
        ", "%$in_type%", $in_line, $in_year, $in_manufacturer);


        //toy hauler exception copied from loadfp_new.php (and then updated slightly)
        if($in_type == "toy hauler"){
            $query = $this->db->prepare("
                SELECT un.id, un.year, manuf.manufacturer, `un`.`line_id`, `lines`.`line`, `un`.`type`, `un`.`floorplan`
                FROM `unit_specs` as `specs`, units as un, manufacturers as manuf , ".DB_NAME.".`lines` as `lines` 
                WHERE un.manuf_id = manuf.id AND line = %s AND un.year=%d AND un.line_id=`lines`.id 
                    AND un.`disp`=1 AND un.id=`specs`.`unit_id` AND `specs`.`label_id`=30 order by  un.`type`, un.floorplan
            ", $in_line, $in_year);
        }          
        
        $rows = $this->db->get_results($query, ARRAY_A);
        
        $result['query'] = $query;//return for testing
        $result['rows'] = $rows;//use this 
            
            
        return $result;         
        
    }    
    

   
    
    /**
     * fetch_counts
     * @var $in_rows : the result set from fetch_inventory()
     * @return $count_arr : an array of all search sidebar counts
     */    
    public function fetch_counts ($in_rows){

        $allRowsArray = $this->fetch_inventory("","",1);
        $allRows = $allRowsArray['rows'];
            
        //the first half of this function gets all makes/conditions/types/features dynamically  - weights and prices are set statically
        $count_arr = array();
        foreach($allRows as $k => $v){
            
            //make
            $make = strtolower($v['make']);
            if(!isset($count_arr['make'][$make])){
                $count_arr['make'][$make] = 0;
            }

            //line (brand)
            $line = strtolower($v['line']);
            if(!isset($count_arr['line'][$line])){
                $count_arr['line'][$line] = 0;
            }            
            
            //condition
            $condition = strtolower($v['thecondition']);
            if(!isset($count_arr['thecondition'][$condition])){
                $count_arr['thecondition'][$condition] = 0;
            }    
            
            //type
            $type = strtolower($v['types']);
            if(!isset($count_arr['types'][$type])){
                $count_arr['types'][$type] = 0;
            }
         
        }
        
        //just cycle through one to get features
        foreach($allRows[0] as $k => $v){
            if( preg_match("/waf_/", $k) && $k != "waf_unloaded_vehicle_weight" && $k != "waf_year" && $k != "waf_make" && $k != "waf_line" && $k != "waf_model" && $k != "waf_model_id"){
                //$column = str_replace(array("waf_","_"),array(""," "),$k);
                $column = $k;
                $count_arr['features'][$column] = 0;
            }
        }
        $count_arr['features']['used_toy_hauler'] = 0;//used as a hack to get used toy haulers to work (used rvs do not have features, but this is a feature)

        $count_arr['price']['15000'] = 0;
        $count_arr['price']['30000'] = 0;
        $count_arr['price']['60000'] = 0;
        $count_arr['price']['90000'] = 0;
        $count_arr['price']['90000+'] = 0;
        
        $count_arr['weight']['3500'] = 0;
        $count_arr['weight']['5500'] = 0;
        $count_arr['weight']['8500'] = 0;
        $count_arr['weight']['8500+'] = 0;

        //now get current results default
        foreach($in_rows as $k => $v){
            
            //make
            $make = strtolower($v['make']);
            
            //brand, or line
            $line = strtolower($v['line']);

            //condition
            $condition = strtolower($v['thecondition']);
   
            //type
            $type = strtolower($v['types']);
      
            //price (convert to range value)
            $price = strtolower($v['price']);
            if($price <= 15000){
                $price = 15000;
            }elseif($price <= 30000){
                $price = 30000;
            }elseif($price <= 60000){
                $price = 60000;
            }elseif($price <= 90000){
                $price = 90000;
            }else{
                $price = "90000+";
            } 

            
            $weight = $v['waf_unloaded_vehicle_weight'];
            
            //some units don't have weight (unlinked to specs), so remove these (they end up in first bucket, so remove it from there)
            if($weight == 0){
                $count_arr['weight']['3500']--;
            }
            
            if($weight <= 3500 ){
                $weight = 3500;
            }elseif($weight <= 5500){
                $weight = 5500;
            }elseif($weight <= 8500){
                $weight = 8500;
            }else{
                $weight = "8500+";
            }
            
            foreach($count_arr['features'] as $feature => $boo){
                if($v[$feature] == 1){
                    $count_arr['features'][$feature]++;
                }
            }

            
            $count_arr['thecondition'][$condition]++;
            $count_arr['types'][$type]++;
            $count_arr['make'][$make]++;
            $count_arr['line'][$line]++;
            $count_arr['price'][$price]++;
            $count_arr['weight'][$weight]++;
            
        } 
        return $count_arr;  
    }    


    /**
     * fetch_updated_counts => Redo counts so that "or" stuff doesn't remove from total (show what's possible, not what's there)
     * @var $in_counts : the old count method, showing "what's there"
     * @var $hasSpecials : special stuff
     */
    public function fetch_updated_counts($in_counts, $hasSpecials){
        
        //loop through each checked category to redo counts on categories that have at least one checked
        $redoCatArr = array();
        foreach($_SESSION['where'] as $cat => $catOptions){
            foreach($catOptions as $k => $v){
                if($v == 1 || $v == true){
                    $redoCatArr[] = $cat;
                    //echo "$cat $k => $v";//for testing  
                }
            }    
        }

        foreach($redoCatArr as $k => $v){
            //echo "$k => $v";//for testing  
    
            $newRows = $this->fetch_inventory($hasSpecials, "", "",$v);
            $newCounts = $this->fetch_counts($newRows['rows']);
            $in_counts[$v] = $newCounts[$v];//replace the updated counts with the category that needs it
            //$test = "<div class='red'>Counts:</div><pre>".print_r($counts,1)."</pre>"; 
            //$objResponse->addAssign("compare-container", "innerHTML", "$test");  
        }
        
        return $in_counts;
        
    }

    /**
     * fetch_est_monthly_payment
     * @var $in_price : The price, used to find the range to return correct payment
     * @return $estimatedMonthlyPayment (rounded and ready to go!)
     */
    public function fetch_est_monthly_payment($in_price){
            
        $estMonthlyPayment = array();    
            
        //Pull from Custom Post Type (there should only be one published at a time)
        $args = array(
            'post_type' => 'monthly_payment', 
            'posts_per_page' => 1,
            'post_status' => 'publish'
        );
        $loopPayment = new WP_Query( $args );
        if ( $loopPayment->have_posts() ) : $loopPayment->the_post();
            $monthlyPaymentsArr = get_field('monthly_payments_repeater');
            $taxRate = get_field('tax_rate');
            $taxAddtionalAmount = get_field('tax_additional_amount');
            $docRepeaterArr = get_field('doc_repeater');
        endif;
        wp_reset_query();
        
        if(!is_numeric($in_price)){
            $in_price = 0.00;
        }
        
        foreach($monthlyPaymentsArr as $k => $v){
            if($in_price >= $v['min_principal'] && $in_price < $v['max_principal'] ){
                $interestRate = $v['interest_rate'];
                $years = $v['years'];
                $down = ($v['down_payment_percentage']/100) * $in_price; 
            }
        }
                        
        $calculate = $this->calcPay($in_price, $years, $interestRate, $down); 
        $emp = number_format($calculate,2);
        
        $estMonthlyPayment['years'] = $years;
        $estMonthlyPayment['down'] = number_format($down,2, '.', '');
        $estMonthlyPayment['interest_rate'] = $interestRate;
        $estMonthlyPayment['emp'] = $emp;
        
        return $estMonthlyPayment;
    }


    /**
     * fetch_description (added June 21, 2016 to allow a faster method of checkingn for empty descriptions for signed in users)
     * @var $in_stock : The price, used to find the range to return correct payment
     * @return $description
     */
    public function fetch_description($in_stock){
    
        $query = $this->db->prepare("
            SELECT `description`
            FROM `newinv_info` as ni
            WHERE ni.stock=%d
        ", $in_stock);
            
        
        $description = $this->db->get_results($query, ARRAY_A);        
        
        return $description;
    }
    
    

    /**
     * A systematic way of building out an inventory title (which is pervavise throughout website)
     * @var $in_condition = new or used
     * @var $in_year = the year of the inventory or plan
     * @var $in_make = the manufacturer such as Starcraft, Winnebago, Crossroads, Forest River, etc.
     * @var $in_line = the product line
     * @var $in_model = the specific floorplan of the line
     * @return the formatted inventory title name
     */
    public function build_inventory_title($in_condition, $in_year, $in_make, $in_line, $in_model) {
        
        $title = "$in_condition $in_year ".ucwords($in_make)." ".ucwords($in_line)." $in_model";
        
        return $title;
    }
    
    
    
    /**
     * A systematic way of building the price, msrp, savings, and estimated monthly payment
     * @var $in_price = the price
     * @var $in_msrp = the MSRP
     */
    public function build_pricing ($in_price, $in_msrp) {
        
        $pricing = array();
        
        $fetchEMP = $this->fetch_est_monthly_payment($in_price);
        $pricing = $fetchEMP;
        $pricing['estMonthlyPayment'] = "$".$fetchEMP['emp'];   
        
        if( ($in_price == '' || $in_price == '0.00') || $in_price == $in_msrp ) {
            $pricing['price'] = 'Call for Best Price';
            $pricing['msrp'] = "-";
            $pricing['savings'] = "-";
            $pricing['estMonthlyPayment'] = "-";
        } else {

            $pricing['price'] = "$".number_format ( $in_price, 2 );
            $pricing['msrp'] = "$".number_format ( $in_msrp, 2 );
            $msrpCalc = str_replace(",", "", $in_msrp);
            $priceCalc =  str_replace(",", "", $in_price);
            $difference = floatval($msrpCalc) - floatval($priceCalc); 
            $pricing['savings'] = "$".number_format ( ($difference), 2 )."";                  
        } 
        
        return $pricing;
    }    


 
    /**
     * build_inventory => This builds each inventory box on specials and rvs landing pages
     * @var $in_rows : array of rows, comes from fetch_inventory
     * @var $start : the result to print as the first item (pagination variable - when AJAXed, the start value is always 0)
     * @var $end : the result to print as the last item on the page (pagination variable - default 10 but can be set from dropdown)
     * @var $in_managerSpecialStocks : array of stock numbers - make this green because they are special
     * @var $in_buildComparison (optional) : Boolean, default "false" => Display alternate comparison view for comparison page
     * @return the inventory list to be printed to the screen
     */    
    public function build_inventory($in_rows, $start, $end, $in_managerSpecialStocks, $in_buildComparison = false){
        

    
        ob_start();
           
            for ($i=$start;$i < $end ;++$i ) {

            $row = $in_rows[$i];

            $specialClass = "";
            if(!empty($in_managerSpecialStocks) && in_array($row['stock'], $in_managerSpecialStocks)){
                $specialClass = "specials";
            }
                
            if( strtolower($row['thecondition']) == "used"){
                $rootImage = "wp-content/gallery/used/".$row['stock']."/main.jpg";
            }else{
                $rootImage = "wp-content/gallery/".$row['year']."/current/stock/".$row['stock']."/main.jpg";
            }
            
            //make sure image exists
            if(!file_exists($rootImage)){
                $rootImage = 'wp-content/themes/msl/assets/img/no-images.jpg';
            }            
                 
            $imgBG = "";            
            if (file_exists($rootImage)) {
                $img = '<img src="'.BASE_URL.$rootImage.'" alt="'.$row['stock'].'" class="prod-image" />';
                $imgBG = BASE_URL.$rootImage;
            } else {
                $img = '<img src="'.BASE_URL.'wp-content/themes/msl/assets/img/no-images.jpg" alt="'.$row['stock'].'" class="prod-image" />';
            }
            
            $unformattedPrice = $row['price'];
            $pricing = $this->build_pricing($row['price'],$row['msrp']);
            // if($row['price'] == '' || $row['price'] == '0.00') {
                // $row['price'] = 'Call for Price';
            // } else {
                // $row['price'] = number_format ( $row['price'], 2 );
            // }            
            // $fetchEMP = $this->fetch_est_monthly_payment($unformattedPrice);
            // $estMonthlyPayment = $fetchEMP['emp'];     
      
            $inventoryTitle = $this->build_inventory_title($row['thecondition'], $row['year'], $row['make'],$row['line'],$row['model']); 
        
            if($in_buildComparison == true){//comparison view
            
                $show = "";
                if($i == 2){
                    $show = "show-for-medium-up";//when the screen size is mobile, we only show 2
                }elseif($i == 3){
                    $show = "show-for-large-up";//when tablet screen size, we only show 3
                }
    
        ?>
        <div class="small-6 medium-4 large-3 end <?php echo $show; ?> columns">
            <div class="comp-container"  >
                <div class="comp-content-container relative" >  


                    <div class='remove-comparison remove-this-stock'  
                        data-stock='<?php echo $row['stock']; ?>'><div class='remove-comparison-x'>X</div></div>
                    
                    <div class="comp-title"  >
                        <?php echo $inventoryTitle;?>
                    </div>
               
                    <?php echo "<div class='comp-img' style='background: url(".BASE_URL.$rootImage.") no-repeat center center; background-size: cover;'>&nbsp;</div>"; ?>
                    <?php echo "<img class='comp-print' src=".BASE_URL.$rootImage." />"; ?>

                    <div class="green price" >
                        
                        <?php  echo $pricing['price']; ?>
                    </div>
                    <div class="comp-stock-num">
                        Stock #: <?php echo $row['stock']; ?>
                    </div>
                    <div class="comp-text-container show-for-medium-up" >
                        
                        <?php if (strtolower($row['thecondition']) != "used"){ ?>
                        <div class="clearfix comp-row" >
                            <div class="small-6 columns border-right nopadding-left" >
                                <div class="comp-detail text-right" >MSRP</div>
                            </div>
                            <div class="small-6 columns">
                                <div class="comp-value" >
                                    <?php 
                                         echo $pricing['msrp'];
                                    ?>
                                </div>
                            </div>
                        </div>  
                        <div class="clearfix comp-row" >
                            <div class="small-6 columns border-right nopadding-left" >
                                <div class="comp-detail text-right" >You Save</div>
                            </div>
                            <div class="small-6 columns">
                                <div class="red comp-value" >
                                    <?php 
                                       echo $pricing['savings'];
                                     ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="clearfix comp-row" >
                            <div class="small-6 columns border-right nopadding-left" >
                                <div class="comp-detail text-right" >Est. Monthly<br />Payment</div>
                            </div>
                            <div class="small-6 columns">
                                <div class="green comp-value" >
                                    <?php 
                                        echo $pricing['estMonthlyPayment'];                                         
                                    ?>                                    
                                </div>
                            </div>
                        </div> 
                    </div>  
                                                                         
                </div>    
   
            </div>
            
            <div class="comp-payments-btn show-for-medium-up">
                <a id="calculate-<?php echo $row['stock']; ?>" class="graybutton full-width calculate-payments-btn" href="javascript:void(0)"   
                    data-interest="<?php echo $pricing['interest_rate']; ?>" data-down="<?php echo $pricing['down']; ?>" data-years="<?php echo $pricing['years']; ?>" 
                    data-price="<?php echo $unformattedPrice; ?>" data-emp="<?php echo $pricing['estMonthlyPayment']; ?>" data-stock="<?php echo $row['stock']; ?>">Calculate Payment</a>
            </div>
            <div class="comp-apply-btn">
                <a class="graybutton full-width" href="<?php echo BASE_URL;?>financing/" >Apply For Financing</a>
            </div>
        </div>

                
        <?php        
            }else{//standard
            
                //if admin, then check for incomplete data and color it read
                $bgColor = $editLink = $isRed = "";
                if (current_user_can( 'read' )) {
                    
                    $specs = $this->fetch_specs($row['year'],$row['make'],$row['line'],$row['model']);
                    if(empty($specs['rows'])){
                        $bgColor = "style='background-color: yellow;'";//#BB465D
                        //echo "<pre>".print_r($specs,1)."</pre>";
                        $isRed = 1; 
                    }
                    
                    $description = $this->fetch_description($row['stock']);
                    //echo "<pre>".print_r($description,1)."</pre>";
                    if(empty($description[0]['description'])){
                        $color = "#C65D57";//red
                        if($isRed == 1){
                          $color = "#ed872d";//orange
                        }
                        $bgColor = "style='background-color: $color;'";
                        //$isRed = 1; 
                    }
                    
                    $editLink = "<a href='#' class='edit-link' data-stock='".$row['stock']."' data-red='$isRed' >Edit Details</a>";
                }
        ?>
            <?php echo $editLink; ?>
            <div class="relative">
            <a href="<?php echo BASE_URL; ?>details?stock=<?php echo $row['stock']; ?>">
                <div class="inventory-container <?php echo $specialClass; ?>"  <?php echo $bgColor; ?>>
                    <div class="inventory-content-container">
                        <div class="small-12 medium-10 columns">
                            <div class="inventory-title" >
                                <?php echo $inventoryTitle; ?>
                            </div>
                        </div>
                        <?php
                        if($specialClass == "specials"){
                        ?>
                            <div class="small-12 medium-2 columns">
                                <div class="manager-special-icon">
                                    <img src="<?php echo BASE_URL;?>wp-content/themes/msl/assets/img/manager-special-icon.png" alt="Manager's Special" />
                                </div>
                            </div>
                        <?php                       
                        }
                        ?>
                        <div class="small-12 medium-6 columns nopadding-right">
                            
                            <div class="inventory-img-container" style="background-image: url('<?php echo $imgBG; ?>');">
                                <?php //echo $img; ?>
                                <?php if(!empty($row['color'])){ ?>
                                    <div class="dark-mask"></div>
                                    <div class="white color-text" >Interior Color: <?php echo ucfirst(strtolower($row['color'])); ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="small-12 medium-6 columns">
                            

                            
                            <?php     
                                if(preg_match("/Call/",$pricing['price'])){
                                    
                            ?>        
                                <div class="inventory-text-container no-price-top" >     
                                    <div class="clearfix ">
                                        <div class="small-12 columns">
                                            <div class="inventory-value">   
                                                <div class="green price price-adjustment">
                                                    <?php 
                                                       echo $pricing['price'];
                                                    ?>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>                                                    
                                </div>                                    
                            <?php     
                                }else{
                            ?> 
                                <div class="inventory-text-container top" >     
                                    <div class="clearfix ">
                                        <div class="small-6 columns border-right" >
                                            <div class="inventory-detail text-right" >Sales Price</div>
                                        </div>
                                        <div class="small-6 columns">
                                            <div class="inventory-value">   
                                                <div class="green price price-adjustment">
                                                    <?php 
                                                       echo $pricing['price'];
                                                    ?>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>                                                    
                                </div>                            
                            <?php     
                                }
                            ?>                                                  
                            <div class="inventory-text-container" >
                                
                                <?php if (strtolower($row['thecondition']) != "used"){ ?>
                                <div class="clearfix inventory-row">
                                    <div class="small-6 columns border-right" >
                                        <div class="inventory-detail text-right" >MSRP</div>
                                    </div>
                                    <div class="small-6 columns">
                                        <div class="inventory-value">                                    
                                        <?php 
                                           echo $pricing['msrp']; 
                                        ?>
                                        </div>
                                    </div>
                                </div>  
                                <div class="clearfix inventory-row">
                                    <div class="small-6 columns border-right" >
                                        <div class="inventory-detail text-right" >You Save</div>
                                    </div>
                                    <div class="small-6 columns">
                                        <div class="red inventory-value">
                                            <?php 
                                                echo $pricing['savings'];
                                             ?>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="clearfix inventory-row">
                                    <div class="small-6 columns border-right" >
                                        <div class="inventory-detail text-right" >Est. Monthly<br />Payment</div>
                                    </div>
                                    <div class="small-6 columns">
                                        <div class="green inventory-value">
                                            <?php 
                                                echo $pricing['estMonthlyPayment'];                                         
                                            ?>                                             
                                        </div>
                                    </div>
                                </div> 
                            </div>  
                                                                             
                        </div>     
    
                    </div>    
       
                </div>
            </a>
            
                
            
                <!-- pull this out of normal flow so that it doesn't click to detail page -->    
                <div class="compare-checkbox-container" >
                    <div class="small-12 columns">
                        
                        <?php
                            $checked = "";
                            if( isset($_SESSION['compare']) &&  in_array($row['stock'],$_SESSION['compare'])){
                                $checked = "checked";
                            }
                        ?>
                    
                        <input type="checkbox" class="compare-checkbox"  <?php echo $checked; ?>  data-year="<?php echo $row['year']; ?>"
                            data-name="<?php echo $inventoryTitle; ?>" data-price="<?php echo $pricing['price']; ?>" data-condition="<?php echo $row['thecondition']; ?>"
                            name="compare_<?php echo $row['stock']; ?>" id="compare_<?php echo $row['stock']; ?>" 
                            value="<?php echo $row['stock']; ?>  ">
                            <label for="compare_<?php echo $row['stock']; ?>">Compare</label>
                    </div>
                </div> 
                <div class="inventory-stock-id">#<?php echo $row['stock']; ?></div>
            </div>
        <?php
            }
       
        }
    
        $output = ob_get_clean();
        //flush();
        return $output;    
   
    }


    /**
     *  build_popup_forms - used on inventor details (details.php) and spec details (specs_detail.php) pages
     * @var in_title = the current year, make, model, line, etc. passed in as the full title string
     * @return the two popup forms "request info" and "calculate payment"
     */
    function build_popup_forms($in_title){

    ob_start(); 
    ?>
    
        <!-- Request Info Popup Form -->
        <div id="request-info-popup" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
            
            <!-- close button -->
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            
            <div class="row">
                <div class="small-12 columns">    
                  <div class="contact-us-title"><?php echo $in_title; ?></div>
                </div> 
                        
        
                <div class="small-12 columns"> 
                    <div class="request-info-details-text">    
                        Please fill out the form and one of our representatives will contact you as soon as possible.
                    </div>
                 </div>  
                                
            </div> 
        
          <?php echo do_shortcode('[contact-form-7 id="243" title="Request Info"]'); ?>
          
        </div>

        <!-- Calculate Payment Popup Form -->
        <div id="calculate-payment-popup" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
            
            <!-- close button -->
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            
            <div class="row">
                <div class="small-12 columns">    
                  <div class="contact-us-title">Calculate Payment</div>
                </div> 
                        
            </div> 
        
            <div class="pad-top-40">&nbsp;</div>
        
            <div class="row">
                <div class="small-12 medium-6 columns">
                    <div class="form-group">
                        <label class="" for="formPrice">Price</label>
                        <span class="">
                            <input type="text" name="formPrice" value="" size="40" class="form-control" id="formPrice" />
                        </span>
                    </div>
                </div>
                <div class="small-12 medium-6 columns">
                    <div class="form-group">
                        <label class="" for="formDownPayment">Down Payment</label>
                        <span class="">
                            <input type="text" name="formDownPayment" value="" size="40" class="form-control" id="formDownPayment" aria-required="true" aria-invalid="false"  />
                        </span>
                    </div>
                </div>
        
                <div class="small-12 medium-6 columns">
                    <div class="form-group">
                        <label class="" for="formLoanTerm">Loan Term</label>
                        <span class="">
                            <input type="text" name="formLoanTerm" value="" size="40" class="form-control" id="formLoanTerm" aria-required="true" aria-invalid="false"  />
                        </span>
                    </div>
                </div>
                <div class="small-12 medium-6 columns">
                    <div class="form-group">
                        <label class="" for="formAnnualPercentageRates">Annual Percentage Rate</label>
                        <span class="">
                            <input type="text" name="formAnnualPercentageRates" value="" size="40" class="form-control" id="formAnnualPercentageRates" aria-invalid="false"  />
                        </span>
                    </div>
                </div>
                <div class="clearfix"></div>
                
                <div class="small-12 columns">
                    <div id="fine-print-1" class="fine-print">
                        *Monthly payment estimates are for informational purposes and do not represent a financing offer from the seller of this vehicle.
                    </div>
                    <div id="fine-print-2" class="fine-print">
                        <?php echo get_field('disclaimer_text_payment_calculation', 'option'); ?>
                    </div>
                </div>
        
                <div class="small-12 columns">
                    <div class="monthly-payment" >Monthly Payment</div>
                </div>
        
                <div class="small-12 columns">
                    <div id="est-pmt" class="green calculated-price" ></div>
                </div>                
                <div class="small-12 columns">
                    <div class="hide">
                        <input type="hidden" name="stockNum" value="" size="40" class="form-control" id="stockNum" aria-required="true" aria-invalid="false"  />
                    </div>
                    <div id="est-pmt-error" class="red" ></div>
                </div>                 
                
                <div class="small-12 columns">
                    <div class="form-group">
                        <button  id="calculateMonthlyPmt" type="submit" class="btn btn-super btn-square btn-ghost-alt ">Calculate</button>
                    </div>
                </div>
        
            </div>
         </div>
    
    <?php
        $output = ob_get_clean();
        //flush();
        return $output;        
    }
    
    
    
    

    /**
     * build_search_checkboxes
     * @var in_cat: the category of the checkbox (ie types, features, price, weight, condition, etc)
     * @var in_counts: the counts of how much that item is in the current search
     * @return output checkboxes
     */
    public function build_search_checkboxes ($in_cat, $in_counts){
        
        ob_start();
        
            foreach($in_counts[$in_cat] as $k => $v){
                $title = str_replace(array("_","waf"),array(" ",""), $k);
                $checkboxHide = ""; 
                if(isset($in_counts[$in_cat][$k]) && $in_counts[$in_cat][$k] != 0 ) {
                    $thisCount = $in_counts[$in_cat][$k];
                }else{
                    $thisCount = "0";
                    if($in_cat != "types" && $in_cat != "thecondition"){
                        $checkboxHide = "hide-check";
                    }
                }
            ?>
                
                <div class="small-12  columns nopadding-right">
                    <div id="<?php echo $k;?>-container" class="right checkbox-container <?php echo $checkboxHide;?>">
                        <label for="<?php echo  $in_cat.'-'.$k.'-checkbox'; ?>" class="left" >
                            <?php echo  ucwords($title); ?>
                        </label>
                        <span id="<?php echo  $in_cat.'-'.$k.'-count'; ?>" class="green" >
                            (<?php echo $thisCount; ?>)
                        </span>
                        <input type="checkbox" id="<?php echo  $in_cat.'-'.$k.'-checkbox'; ?>" 
                            data-cat="<?php echo $in_cat; ?>" data-column="<?php echo $k; ?>" class="search-checkbox"
                            <?php if(isset($_SESSION['where'][$in_cat][$k]) && $_SESSION['where'][$in_cat][$k] == "true"){echo "checked";} ?> 
                        />    
                    </div>
                </div>                           
            
            <?php
            }
            
        /*
         * <!-- onclick="xajax_update_inventory('<?php echo $in_cat; ?>','<?php echo $k; ?>',this.checked);" />  -->
         */ 
        
        $output = ob_get_clean();
        //flush();
        
        return $output;        

    }



    /**
     * build_dd_options : fills in the select box options on the home page
     * @var in_cat: the category of the checkbox (ie types, features, price, weight, condition, etc)
     * @var in_counts: the counts of how much that item is in the current search
     * @return output dd options
     */
    public function build_dd_options($in_cat, $in_counts){
        
        $dd = "";
        
        foreach($in_counts[$in_cat] as $k => $v){
            $title = str_replace(array("_","waf"),array(" ",""), $k);
        
            $dd .= "<option value='$k'>$title</option>";

        }
                    
        
        return $dd;         
    }


    /**
     * Build_comparison => build the comparison box via page load or AJAX on the rvs inventory page
     */
    public function build_comparison (){
        
        
        $comparison = "";
        foreach($_SESSION['compare_data'] as $num => $dataArray){
            
            if( strtolower($dataArray['condition']) == "used"){
                $rootImage = "wp-content/gallery/used/".$dataArray['stock']."/main.jpg";
            }else{
                $rootImage = "wp-content/gallery/".$dataArray['year']."/current/stock/".$dataArray['stock']."/main.jpg";
            }
            
            //make sure image exists
            if(!file_exists($rootImage)){
                $rootImage = 'wp-content/themes/msl/assets/img/no-images.jpg';
            }
            
            if(preg_match("/Call/",$dataArray['price'])){
                $dataArray['price'] = "Call for Price";
            }else{
                
            }
            
            $comparison .= 
                "<div class='compare-row' >".
            
                    "<img src='".BASE_URL.$rootImage."' alt='".$dataArray['stock']."'  />".
   
                    "<div class='compare-text' >".
                        "<div class='compare-name' >".$dataArray['name']."</div>".
                        "<div class='compare-price' >".$dataArray['price']."</div>".
                    "</div>".
            
                    "<div class='clear'></div>".
            
                    "<div class='remove-comparison' onclick='".esc_js( "xajax_update_comparison( 0, \"compare_".$dataArray['stock']."\", \"\", \"\", \"\", \"\" );" )."'".
                        " data-stock='".$dataArray['stock']."' ><div class='remove-comparison-x'>X</div></div>".
            
                "</div>";
        } 

        return $comparison;
        
    }


    /**
     * build_pagination - Build the pagination for inventory page (page load and via AJAX)
     * @var $in_rowCount : The current number of results through the latest query
     * @return $paginationArr : has all the args needed to call paginate_links_custom
     */
    public function build_pagination($in_rowCount){
        
        $paginationArr = array();
        
        
        //Pagination
        $current = (intval(get_query_var('paged'))) ? intval(get_query_var('paged')) : 1;
          
        //global $wp_rewrite;
         
        $pagination_args = array(
         'base' => @add_query_arg('paged','%#%'),
         'format' => '',
         'total' => ceil($in_rowCount/$_SESSION['rows_per_page']),
         'current' => $current,
         'show_all' => false,
         'mid_size' => 1,
         'prev_text' => __('<i class="fa fa-chevron-left"></i>'),
         'next_text' => __('<i class="fa fa-chevron-right"></i>'), 
         'type' => 'plain'
        );
         
        $start = ($current - 1) * $_SESSION['rows_per_page'];
        $end = $start + $_SESSION['rows_per_page'];
        $end = ($in_rowCount < $end) ? $in_rowCount: $end;
        
        $totalRecords = $in_rowCount;
        $resultsLine = ($start + 1).'-'.$end.' of '.$totalRecords;
        
        $paginationArr['start'] = $start;
        $paginationArr['end'] = $end;
        $paginationArr['results-line'] = $resultsLine;
        
        //if( $wp_rewrite->using_permalinks() )
        // $pagination_args['base'] = user_trailingslashit( trailingslashit( remove_query_arg('s',get_pagenum_link(1) ) ) . 'page/%#%/', 'paged');
        
        //$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $link = get_pagenum_link(1);
        $parsedLink = parse_url($link);
        
        //echo "1: ".$parsedLink['host'].$parsedLink['path'].'page/%#%/'.'?'.$parsedLink['query']."<br />";
        //echo "2: ".get_pagenum_link(1)."<br />";
        //echo "3: ".$pagination_args['base']."<br />";
        //echo "4: ".trailingslashit( remove_query_arg('s',get_pagenum_link(1) ) )."<br />";
        
        $query = "";
        if(isset($parsedLink['query']) && $parsedLink['query'] != ""){
            //$query = '?'.$parsedLink['query'];
        }
        $pagination_args['base'] = $parsedLink['host'].$parsedLink['path'].'page/%#%/'.$query;
         
        if( !empty($wp_query->query_vars['s']) )
        $pagination_args['add_args'] = array('s'=>get_query_var('s'));        
        
        $paginationArr['args'] = $pagination_args;
        
        return $paginationArr;     
    }



    /**
     * sendEmail => Send the awesomely formatted detail email to the user (the contact form 7 email goes to Motor Sportsland)
     * @var $posted_data : Data passed through from  Contact Form 7
     */
    public function sendEmail($posted_data){
        
 
        $stock = $posted_data['hidden-stock'];
        
        $stockOverviewRows = $this->fetch_inventory(false,array(0=>$stock));
        $stockOverview = array_shift($stockOverviewRows['rows']);//strip off the "0" since there is only one row and not multiple "rows"
        $stockSpecs = $this->fetch_specs($stockOverview['year'],$stockOverview['make'],$stockOverview['line'],$stockOverview['model']);  
        $stockSpecsRows = $stockSpecs['rows'];
          
        
        if(!empty($stockOverviewRows) && !empty($stockSpecsRows)){
    
            //Floorplan image
            $floorplanData = $stockSpecsRows[0]['floorplan'];
            $floorplanImage = "wp-content/gallery/".$stockOverview['year']."/newrvs/floorplan/".strtolower($stockOverview['line'])."/".$floorplanData.".png";    
        }
    
        //get title
        $stockTitle = $this->build_inventory_title($stockOverview['thecondition'], $stockOverview['year'], $stockOverview['make'], $stockOverview['line'], $stockOverview['model']);
        
        //if used, get used images.  Otherwise, get new images
        if( strtolower($stockOverview['thecondition']) == "used"){
            $sliderImages = "wp-content/gallery/used/".$stock."/";
        }else{
            $sliderImages = "wp-content/gallery/".$stockOverview['year']."/current/stock/".$stock."/";
        }
        
    
        $phoneNumber = get_field('local_phone_number', 'option');
        
        
        $menu_logo = get_field('menu_logo', 'option'); 
        $logoWidth = $menu_logo['sizes']['large-width'] / 2;
        $logoHeight = $menu_logo['sizes']['large-height'] / 2;


        $pricing = $this->build_pricing($stockOverview['price'], $stockOverview['msrp']);
        
        // $price = number_format ( $stockOverview['price'], 2 );
        // $msrp = number_format ( $stockOverview['msrp'], 2 );
        // $msrpCalc = str_replace(",", "", $stockOverview['msrp']);
        // $priceCalc =  str_replace(",", "", $stockOverview['price']);
        // $difference = floatval($msrpCalc) - floatval($priceCalc); 
        // $difference = "$".number_format ( ($difference), 2 ).""; 
        $description = $stockOverview['description'];
     
        // $fetchEMP = $this->fetch_est_monthly_payment($stockOverview['price']);
        // $estMonthlyPayment = $fetchEMP['emp'];        
     
        $email = "
        <table style='margin-bottom: 0; width:100%; max-width: 875px; min-width: 740px; ' cellspacing='0' cellpadding='0' >
            <tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td colspan='2' style='width:100%; min-height: 100px; '>
                    ".$posted_data['name'].",<br /><br />".nl2br($posted_data['message'])."
                </td>
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr>
        </table>
        
        <table style='margin-bottom: 0; width:100%; max-width: 875px; min-width: 740px; ' cellspacing='0' cellpadding='0' >
            <tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td style='width:80%; height: 100px; text-align:middle; border-bottom: 1px solid #cccccc;'>
                    <img src='".$menu_logo['sizes']['large']."' width='$logoWidth' height='$logoHeight' />
                </td>
                <td style='width:20%; font-size: 15px; color: #636363; border-bottom: 1px solid #cccccc; text-align: right;'>$phoneNumber</td>
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr>
        </table>
        
        <table style='width:100%; min-width: 740px; max-width: 875px;'  cellspacing='0' cellpadding='0'>
            <tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td colspan='2' style='width:100%; height: 100px; text-align:middle; font-size: 23px; color: #7d7d7d;'>
                    $stockTitle
                </td>
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr>            
            <tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td colspan='2' style='width:100%; height: 40px; text-align:middle; color: #5dbb46; font-size: 21px; font-weight: bold;'>
                    {$pricing['price']}
                </td>
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr> 
            <tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td colspan='2' style='width:100%; height: 60px;  text-align:middle; font-size: 18px;  color: #636363; '>
                    Stock #: $stock
                </td>
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr>            
            <tr ><td colspan='4' style='height:30px;'>&nbsp;</td></tr>
            <tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td  style='width:25%; height: 15px; text-align: right; border-right: 1px solid #cccccc; color: #636363; padding-right: 10px; '>
                    MSRP
                </td>
                <td  style='width:75%; height: 15px; color: #636363; font-weight: bold; padding-left: 10px; '>
                    {$pricing['msrp']}
                </td>            
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr>  
            <tr ><td colspan='4' style='height:30px;'>&nbsp;</td></tr>
            <tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td  style='width:25%; height: 15px; text-align: right; border-right: 1px solid #cccccc; color: #636363; padding-right: 10px; '>
                    You Save
                </td>
                <td  style='width:75%; height: 15px; color: #ef4d24; font-weight: bold; padding-left: 10px; '>
                    {$pricing['savings']}
                </td>            
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr>        
            <tr ><td colspan='4' style='height:30px;'>&nbsp;</td></tr>
            <tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td  style='width:25%; height: 15px; text-align: right; border-right: 1px solid #cccccc; color: #636363; padding-right: 10px; '>
                    Est. Monthly Payment
                </td>
                <td  style='width:75%; height: 15px; color: #5dbb46; font-weight: bold; padding-left: 10px; '>
                    {$pricing['estMonthlyPayment']}
                </td>            
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr>";
    
            if (file_exists($sliderImages) ) {
                $email .= 
                    "<tr ><td colspan='4' style='height:50px;'>&nbsp;</td></tr>
                    <tr >
                        <td style='min-width: 30px;'>&nbsp;</td>
                        <td colspan='2' style='width100%;'>
                            <img src='".BASE_URL.$sliderImages."1.jpg' />
                        </td>            
                        <td style='min-width: 30px;'>&nbsp;</td>
                    </tr> 
                    <tr ><td colspan='4' style='height:50px;'>&nbsp;</td></tr>";
            }else{
                $email .= "<tr ><td colspan='4' style='height:50px;'>&nbsp;</td></tr>";
            }
    
            if (file_exists($floorplanImage) ) {
                $email .= 
                "<tr >
                    <td style='min-width: 30px;'>&nbsp;</td>
                    <td colspan='2' style='width100%; background-color: #f5f5f5; text-align: center; valign: middle; height: 260px; border: 1px solid #cccccc;'>
                        <img src='".BASE_URL.$floorplanImage."' style='height: 200px;' />
                    </td>            
                    <td style='min-width: 30px;'>&nbsp;</td>
                </tr> 
                <tr ><td colspan='4' style='height:50px;'>&nbsp;</td></tr>";
            }
            
            $email .=
            "<tr >
                <td style='min-width: 30px;'>&nbsp;</td>
                <td colspan='2' style='width100%; font-size: 15px; color: #636363; line-height: 24px;'>
                    $description <br /><br />
                    For more information, call us at <a class='phone-number bold' href='tel:$phoneNumber' style='text-decoration: none;color:#5dbb46;'>$phoneNumber</a>     
                </td>            
                <td style='min-width: 30px;'>&nbsp;</td>
            </tr> 
            <tr ><td colspan='4' style='height:50px;'>&nbsp;</td></tr> 
            
                
            <tr>
                <td style='min-width: 30px;'>&nbsp;</td>
                <td colspan='2'>
                    <table style='width:100%; min-width: 740px; max-width: 875px; border: 1px solid #cccccc;'  cellspacing='0' cellpadding='0' >"; 
                    
                 
                        $oldTitle = "";
                        $floorplanData = "";
                        $first = true;
                        foreach($stockSpecsRows as $k => $v){
                        
                            $currentTitle = $v['spec_type'];  
                 
                                $addPadding = "";
                            
                            if($currentTitle != $oldTitle){
                                $addPadding = "padding-top: 30px; padding-bottom: 10px;";
                                
                                if($first != true){
                                    $email .= 
                                    "<tr>
                                        <td style='height: 20px; background-color: #ebebeb;' >&nbsp;</td>
                                        <td style='height: 20px;' >&nbsp;</td>
                                    </tr>";                      
                                }
                       
                                $email .= 
                                "<tr>
                                    <td colspan='2' 
                                        style='background-color: #cccccc; height: 40px; font-size: 19px; color: #636363; font-weight: bold; text-align: center;' >".
                                        $v['spec_type'].
                                   "</td>
                                </tr>";
                      
                            }
                        
                            $email .= '
                            <tr>
                                
                                <td style="width: 50%; text-align: right; padding-right: 30px; '.
                                    $addPadding.' background-color: #ebebeb; font-size: 17px; color: #7d7d7d; height: 30px;">'.
                                    $v['label'].
                               '</td>  
                                <td style="width: 50%; padding-left: 30px; '.$addPadding.' font-size: 17px; color: #7d7d7d; height: 30px; padding-right: 20px;">';
                                    $value = $v['value'] == "1" ? "yes" : $v['value']; 
                                    $email .=  $value.' '.$v['units'];
                                    
                            $email .= '                         
                                </td> 
                               
                            
                            </tr>';
                        
                       
                            $oldTitle = $currentTitle;
                            $first = false;
                        }
     
        
            $email .= '
                            <tr>
                                <td style="height: 20px; background-color: #ebebeb;" >&nbsp;</td>
                                <td style="height: 20px;" >&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td style="min-width: 30px;">&nbsp;</td>
                </tr>
            </table>';

        /*
        $multiple_recipients = array(
            'jarom@marketingepic.com',
            'jaromhig@hotmail.com'
        ); 
        */
        
        //$test = "<pre>".print_r($posted_data,1)."</pre>";
        
        $to = $posted_data['send-to'];
        $subject = 'Stock #'.$stock.' Details from '.BASE_URL.'details/?stock='.$stock;
        $body = "$email";//<br /><br />$test";
        $headers[] = 'From: Motor Sportsland No-reply <no-reply@motorsportsland.com>';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        
        wp_mail( $to, $subject, $body, $headers );
                
        
    }



    /**
     * sendEmail => Send the awesomely formatted comparison email to the user (the contact form 7 email goes to Motorsportsland)
     * @var $posted_data : Data passed through from  Contact Form 7
     */
    public function sendComparisonEmail($posted_data){
        
 
        //$stock = $posted_data['hidden-stock'];

    $compEmail =
        "<table style='margin-bottom: 0; width:100%; max-width: 875px; min-width: 740px; ' cellspacing='0' cellpadding='0' >
            <tr >
                <td style='min-width: 15px;'>&nbsp;</td>
                <td colspan='2' style='width:100%; min-height: 100px; '>
                    ".$posted_data['name'].",<br /><br />".nl2br($posted_data['message'])."
                </td>
                <td style='min-width: 15px;'>&nbsp;</td>
            </tr>
            <tr >
                <td colspan='3' style='height:30px;'>&nbsp;</td>
            </tr>
        </table>
        
        <table style='margin-bottom: 0; width:100%; max-width: 875px; min-width: 740px; ' cellspacing='0' cellpadding='0' ><tr>";
         
            $count = count($_SESSION['compare']);
    
            if($count == 1){
                $interiorSizing = "100%";
            }elseif($count == 2){
                $interiorSizing = "50%";
            }elseif($count == 3){
                $interiorSizing = "33.3%";
            }else{
                $interiorSizing = "25%";
            }
            
            $inventory = $this->fetch_inventory (false, $_SESSION['compare'], "");
    
            $start = 0;
            $end = $count;
     
            for ($i=$start;$i < $end ;++$i ) {
         
                $row = $inventory['rows'][$i];
    
        
                if( strtolower($row['thecondition']) == "used"){
                    $rootImage = "wp-content/gallery/used/".$row['stock']."/main.jpg";
                }else{
                    $rootImage = "wp-content/gallery/".$row['year']."/current/stock/".$row['stock']."/main.jpg";
                }
                            
                if (file_exists($rootImage)) {
                    $img = $rootImage;
                    //'<img src="'.BASE_URL.$rootImage.'" alt="'.$row['stock'].'" class="prod-image" style="height:160px; width: 275px;" />';
                } else {
                    $img = 'wp-content/themes/msl/assets/img/no-images.jpg';
                }
                
                // $fetchEMP = $this->fetch_est_monthly_payment($row['price']);
                // $estMonthlyPayment = number_format($fetchEMP['emp'],2);  
                //                 
                // if($row['price'] == '' || $row['price'] == '0.00') {
                    // $row['price'] = 'Call for Price';
                // } else {
                    // $row['price'] = "$".number_format ( $row['price'], 2 );
                // }    
                
                $pricing = $this->build_pricing($row['price'], $row['msrp']);       
                                                    
          
                $inventoryTitle = $this->build_inventory_title($row['thecondition'], $row['year'], $row['make'],$row['line'],$row['model']); 
            
                    
        $compEmail .=
                '<td style="padding-right: 15px; padding-left: 15px; width:'.$interiorSizing.';">
                
                    <div  style="height: 440px;border: solid 1px #cccccc;background-color: #f5f5f5;"  >
                        <div style="padding: 15px; position: relative;" >';  
        
                        $compEmail .=
                            '<div  style="font-size: 15px;color: #626262;padding-bottom: 15px;padding-top: 20px;font-weight: 400;line-height: 23px;">'
                                .$inventoryTitle.
                            '</div>';
                       
                        $compEmail .= "<div style='height: 100px;width: 100%;background: url(".BASE_URL.$img.") no-repeat center center; background-size: cover;'>&nbsp;</div>"; 
                        
                        $compEmail .=
                            '<div style="padding-top: 15px;font-size: 21px;color: #5dbb46;" >'.$pricing['price'].'</div>
                            <div style="padding-top: 30px;font-size: 14px;font-weight: 400;color: #626262;" >Stock #: '.$row['stock'].'</div>
                            <div style="padding-top: 30px;" >';
                                
                                if (strtolower($row['thecondition']) != "used"){
                                    
                                    $compEmail .= 
                                    '<table style="width:100%;">
                                        <tr>
                                            <td style="width:50%;border-right: 1px solid #cccccc; text-align:right;padding-right: 15px;font-size: 11px;line-height: 13px;font-weight: 400;color: #626262;">MSRP</td>
                                            <td style="width:50%;padding-left: 15px;font-size: 11px;line-height: 13px;font-weight: 400;color: #626262;font-weight:bold;">';
                                                  
                                                $compEmail .=  $pricing['msrp']; 
                                               
                                            $compEmail .= 
                                            '</td>
                                        </tr>
                                        <tr>
                                            <td style="height:8px;" colspan="2">&nbsp;</td>
                                        </tr> 
                                        <tr>
                                            <td style="width:50%;border-right: 1px solid #cccccc; text-align:right;padding-right: 15px;font-size: 11px;line-height: 13px;font-weight: 400;color: #626262;">You Save</td>
                                            <td style="width:50%;padding-left: 15px;font-size: 11px;line-height: 13px;font-weight: 400;color: #ef4d24;font-weight:bold;">';
                                                $compEmail .=  $pricing['savings']; 
                                                
                                            $compEmail .=
                                            '</td>
                                        </tr>
                                        <tr>
                                            <td style="height:8px;" colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td style="width:50%;border-right: 1px solid #cccccc; text-align:right;padding-right: 15px;font-size: 11px;line-height: 13px;font-weight: 400;color: #626262;">Est. Monthly<br />Payment</td>
                                            <td style="width:50%;padding-left: 15px;font-size: 11px;line-height: 13px;font-weight: 400;color: #5dbb46;font-weight:bold;">';
                                                $compEmail .=  $pricing['estMonthlyPayment'];
                                               
                                            $compEmail .=
                                            '</td>
                                        </tr>                                                                                                           
                                    </table>';
                                } 
    
                            $compEmail .= 
                           '</div>  
                                                                                 
                        </div>    
           
                    </div>
    
              
                </td>';
    
             }
    
        $compEmail .= 
            "</tr>
         </table>
         <table style='margin-bottom: 0; width:100%; max-width: 875px; min-width: 740px; ' cellspacing='0' cellpadding='0' >
            <tr >
                <td style='height:30px;'>&nbsp;</td>
            </tr>
        </table>";  
                                   
        $num = 0;
        foreach($inventory['rows'] as $key => $values){
                
            $stockSpecs = $this->fetch_specs($values['year'],$values['make'],$values['line'],$values['model']);  
            $stockSpecsRows = $stockSpecs['rows'];
            $stockSpecsArr[$num] = $stockSpecsRows;
    
            $num++;
        }
        
        //get all labels for category headers
        $allLabels = array();
        foreach($stockSpecsArr as $k => $v){
            foreach($v as $innerkey => $innerArr){
                $allLabels[$innerArr['spec_type']][] = $innerArr['label'];
            }
            
        }
        $labels = $this->super_unique($allLabels);
    
    
      
        $compEmail .=
        "<table style='margin-bottom: 0; width:100%; max-width: 875px; min-width: 740px; ' cellspacing='0' cellpadding='0' >";
                
                
            $num=0;
            foreach($labels as $cat => $label){ 
            
                $compEmail .=
                '<tr class="">
                    <td colspan="'.$count.'" style="background-color: #5dbb46; border-right: 1px solid #5dbb46;color: #fff; text-align: center; height: 46px; width: 100%;">'.$cat.'</td>
                 </tr>
                <tr style="background-color: #f5f5f5;border-left: 1px solid #cccccc;border-right: 1px solid #cccccc;">';
                 
                $loopCount = 0;
                foreach($stockSpecsArr as $key => $spec){
                        
                    //for last border...
                    $lastBorder = $addLastBorder = "";
                    if($count - $loopCount == 1){
                        $lastBorder = "border-right: 0;";
                        $addLastBorder = "border-right: 1px solid #cccccc;";
                    }
                    
                    if($loopCount == 0){
                        $leftBorder = "";
                        $addLeftBorder = "border-left: 1px solid #cccccc;";
                    }else{
                        $leftBorder = "border-left: 1px solid #cccccc;";
                        $addLeftBorder = "";
                    }
                    
                    $compEmail .= 
                    "<td style='width: $interiorSizing;padding: 15px 15px 30px;$addLeftBorder $addLastBorder' valign='middle'>
                        <div style='border-right: 1px solid #cccccc; $leftBorder  $lastBorder'  >";
                    
                    foreach($label as $number => $specType){
                        
                        $compEmail .= $loopCount == 0 ? "<div style='height: 40px;font-size: 13px;color: #626262;' >".$specType."</div>" : "<div style='height: 40px;font-size: 13px;'>&nbsp;</div>";
                                                
                        $hasMatch = false;
                        foreach($spec as $batch => $detail){
      
                            if($detail['label'] == $specType ){
                                
                                if($cat == "Additional Features" && $detail['value'] == 1){
                                    $detail['value'] = "yes";
                                }
                                
                                $compEmail .= "<div style='height: 40px;font-size: 13px;color: #5dbb46; text-align:center;'>".$detail['value']." ".$detail['units']."</div>";
                                $hasMatch = true;
                            }
                        }
                        if($hasMatch == false){
                            $compEmail .= "<div style='height:40px;'>&nbsp;</div>";
                        }
     
                     }
    
                        $colSpan = $loopCount + 1;
                       $compEmail .= "
                                
                            </div>
                        </td>";
                    $loopCount++;   
     
                 }
    
                 $compEmail .=  
                 '</tr>';                            
        
                $num++;
            }
                                        
                            
        $compEmail .=              
        '</table>';
    

        $test = "<pre>".print_r($posted_data,1)."</pre>";
        
        $to = $posted_data['send-to'];
        $subject = 'Comparisons from '.BASE_URL.'';
        $body = "$compEmail";//<br /><br />$test";
        $headers[] = 'From: Motor Sportsland No-reply <no-reply@motorsportsland.com>';
        $headers[] = 'Content-Type: text/html; charset=UTF-8';
        
        wp_mail( $to, $subject, $body, $headers );
    
    
    }

 
    /**
     * update_inventory_details => update the msrp, price, and descripton of a specified stock inventory item
     * @var $in_stock : The stock number of the inventory item being updated
     * @var $in_msrp : The MSRP being updated
     * @var $in_map : The Map being updated (not used)
     * @var $in_price : The Price being updated
     * @var $in_descr : THe description being udated
     * @return Updated 
     */
    function update_inventory_details($in_stock, $in_msrp, $in_map, $in_price, $in_descr){
 
        //INSERT INTO `mslweb`.`newinv_info` (`stock`,`price`,`msrp`,`map`,`description`) VALUES ()
        // ON DUPLICATE KEY  UPDATE 
        $updateQuery = "
            INSERT INTO newinv_info (`stock`,`price`,`msrp`,`map`,`description`)
            VALUES (%d, %f, %f, %f, %s)
            ON DUPLICATE KEY UPDATE `price` = %f, `msrp` = %f, `map` = %f, `description` = %s;
        ";    
        $query = $this->db->prepare($updateQuery, $in_stock, $in_price, $in_msrp, $in_map, $in_descr, $in_price, $in_msrp, $in_map, $in_descr);
        $update = $this->db->get_results($query, ARRAY_A);

        return $update;
    }
    
    
 
    /**
     * Create a record in the model_aliases table (to help match up lines)
     * @var $in_manuf : The Manuf
     * @var $in_line : The line
     * @var $in_model : The model being updated
     * @var $in_year : the year of the model/line
     */
     function update_alias_details($in_manuf, $in_line, $in_model, $in_year, $in_oldModel){
         //"INSERT INTO `mslweb_new`.`model_aliases` (`year`,`manuf`,`line`,`model`,`manuf_alias`,`line_alias`,`model_alias`) 
         //VALUES (". $row['year'].",'".$row['make']."'".",'".$row['line_label']."'".",'".$row['model']."','".$manuf."'".",'".$make."'".",'".$model."')";
         $updateQuery = "
            INSERT INTO model_aliases (`year`,`manuf`,`line`,`model`,`manuf_alias`,`line_alias`,`model_alias`)
            VALUES (%d, %s, %s, %s, %s, %s, %s)
            ON DUPLICATE KEY UPDATE `model_alias` = %s;
        ";    
        $query = $this->db->prepare($updateQuery, $in_year, $in_manuf, $in_line, $in_oldModel, $in_manuf, $in_line, $in_model, $in_model);//remove 2nd in_model
        $update = $this->db->get_results($query, ARRAY_A);

        return $update;
     }
 
    
    /**
     * remove duplicates from multidimensional array
     * @var $array - the array that will have duplicates removed
     */
    function super_unique($array) {
               
        $result = array();    
        
        foreach ($array as $key => $value) {
            //check $array array or not
            if (is_array($value)) {
                //check the array there is any duplicate value
                if (count($value) == count(array_unique($value))) {
                    $result[$key] = $value;
                } else {
                    $result[$key] = array_unique($value);
                }
            } else {
                $result[$key] = $value;
            }
        }
        //return array without duplicate value
        return $result;
    }       
    
    
    
    
    /**
     * calPay => Calculate Monthly Payments 
     * Found on http://stackoverflow.com/questions/10747498/php-mortgage-calculator-canada
     * @var $MORTGAGE : Principal
     * @var $AMORTYEARS : Term of loan in years
     * @var $AMORTMONTHS : Just leave this as 0, but months (Monthly (12), Semi-Monthly (24), Bi-Weekly(26) and Weekly(52) 
     * @var $INRATE : Interest Rate (ie 5.5)
     * @var $COMPOUND : Just set this at 12, compounded monthly
     * @var $FREQ : Number of Payments in a year => 12 unless bimonthly (24), etc.
     * @var $DOWN : Down Payment (not a %)
     * @return Monthly Payment Amount
     */    
    function calcPay($MORTGAGE, $AMORTYEARS, $INRATE, $DOWN, $COMPOUND=12, $FREQ=12, $AMORTMONTHS=0){
        $MORTGAGE = $MORTGAGE - $DOWN;
        $compound = $COMPOUND/12;
        $monTime = ($AMORTYEARS * 12) + (1 * $AMORTMONTHS);
        $RATE = ($INRATE*1.0)/100;
        $yrRate = $RATE/$COMPOUND;
        $rdefine = pow((1.0 + $yrRate),$compound)-1.0;
        $PAYMENT = ($MORTGAGE*$rdefine * (pow((1.0 + $rdefine),$monTime))) / ((pow((1.0 + $rdefine),$monTime)) - 1.0);
        if($FREQ==12){
            return $PAYMENT;}
        if($FREQ==26){
            return $PAYMENT/2.0;}
        if($FREQ==52){
            return $PAYMENT/4.0;}
        if($FREQ==24){
            $compound2 = $COMPOUND/$FREQ;
            $monTime2 = ($AMORTYEARS * $FREQ) + ($AMORTMONTHS * 2);
            $rdefine2 = pow((1.0 + $yrRate),$compound2)-1.0;
            $PAYMENT2 = ($MORTGAGE*$rdefine2 * (pow((1.0 + $rdefine2),$monTime2)))/  ((pow((1.0 + $rdefine2),$monTime2)) - 1.0);
            return $PAYMENT2;
        }
        
        /*
            if($stockOverview['types']=="Motor Home" || $stockOverview['types']=="Mini Home" || $stockOverview['types']=="Diesel")
                            $doc=399;
            elseif($stockOverview['types']=="Folding Trailer"  ||$stockOverview['types']=="Fold Toy")
                            $doc=199;
            else
                            $doc=299;
        
                           
            if($price <= 20000){
                            $myint = 5.5;
                            $mydown = 0;
                            $myterm = 120;//10 year
                                           
            }elseif($price > 20000 && $price <=25000){
                            $myint = 5.5;
                            $mydown = 0;
                            $myterm = 144;
                                           
            }elseif($price > 25000 && $price <=50000){
                            $myint = 5.5;
                            $mydown = 0;
                            $myterm = 180;
                                           
            }elseif($price > 50000 && $price <=75000){
                            $myint = 5.25;
                            $mydown = 0;
                            $myterm = 240;
                                           
            }elseif($price > 75000){
                            $myint = 4.99;
                            $mydown = $price * .1;
                            $myterm = 240;
                                           
            }
            $irate = $myint/100;
            $mrate= $myint/1200;
            $tax = ($price +250) *.0685;
            $tmp_ann = pow(1/(1+($mrate)),$myterm-1);
            $ann1 = (1-$tmp_ann)/($mrate);
            $tmp_int = (1+((45*$irate)/360));
           
            // CALCULATE ANNUITIES          
            //        $annuity1=annu($myint/100,$myterm,1);       // Calculate annuity1   
             
            // CALCULATE ODD DAYS INTEREST 
            //  echo  $tmp_int=(1+((45*($myint/100))/360));
            // FIGURE MAIN_DIV 
            $MAIN_DIV=(1+$ann1)/$tmp_int;
         
            $unpaided = $price+$tax+$doc+250 - $mydown;
                       
            $mymonthly=(($unpaided)/($MAIN_DIV)*100);
            $mymonthly=(int)$mymonthly;   // Truncate   
            $mymonthly/=100;
            $mymonthly=number_format($mymonthly,0);
            
            echo "<br /><br />".$MAIN_DIV."<br /><br />";
                            //payment     
            //  $mysavings = 100; //number_format($msrp_num-$showprice_num,2);
         */  
    }    



    /**
     * Get dd (create a dropdown based on the passed data) 
     * @var $in_ddList = 
     * @var $in_key = 
     * @var $in_dataType = 
     */
    public function get_dd ($in_ddList, $in_key, $in_dataType="list"){
        
        
        if($in_dataType != "list"){
            
            //
            foreach($in_ddList as $k => $v){
                $list[] = $v[$in_key];
            }
        }else{
            //get list ready
            $list = explode(",",$in_ddList);
        }

        
        //build drop down
        $dd = "<select onchange=\"xajax_buffer(this.value, '$in_key', '', '0');\" >
                    <option value=''> </option>";   //xajax_updateSearch(this.value, '$in_key');
        
        //add options based on list
        foreach($list as $k => $v){
        
            $selected = "";
            if(isset($_SESSION['choices'][$in_key]) && $_SESSION['choices'][$in_key] == $v){
                $selected = "selected";
            }
            
            $dd .= "<option value='$v' $selected>$v</option>";
        }
        
        $dd .= "</select>";
        
        return $dd;     
        
    }
    
        
}
?>