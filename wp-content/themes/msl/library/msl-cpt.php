<?php
/**
 * Custom post type Monthly Payment
 */
function monthly_payment_cpt() {
  $labels = array(
    'name'               => _x( 'Monthly Payment', 'post type general name' ),
    'singular_name'      => _x( 'Monthly Payment', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Monthly Payment Options' ),
    'edit_item'          => __( 'Edit Monthly Payment Options' ),
    'new_item'           => __( 'New Monthly Payment Options' ),
    'all_items'          => __( 'View Monthly Payment Options' ),
    'view_item'          => __( 'View Monthly Payment Options' ),
    'search_items'       => __( 'Search Monthly Payment Options' ),
    'not_found'          => __( 'No Monthly Payment Options' ),
    'not_found_in_trash' => __( 'No Monthly Payment Options found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Monthly Payment'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Monthly Payment',
    'public'        => true,
    'menu_position' => 6,
    'supports'      => array( 'title'), //, 'editor' , 'thumbnail', 'excerpt', 'comments' 
    'has_archive'   => true,
  );
  register_post_type( 'monthly_payment', $args ); //this is what's used to call in contact details
}
add_action( 'init', 'monthly_payment_cpt' );



/**
 * Custom post type Manager's Specials
 */
function manger_special_cpt() {
  $labels = array(
    'name'               => _x( 'Manager\'s Specials', 'post type general name' ),
    'singular_name'      => _x( 'Manager\'s Special', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Manager\'s Special (add stock number)' ),
    'edit_item'          => __( 'Edit Manager\'s Specials' ),
    'new_item'           => __( 'New Manager\'s Special' ),
    'all_items'          => __( 'View Manager\'s Specials' ),
    'view_item'          => __( 'View Manager\'s Special' ),
    'search_items'       => __( 'Search Manager\'s Special' ),
    'not_found'          => __( 'No Manager\'s Special' ),
    'not_found_in_trash' => __( 'No Manager\'s Special found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Manager\'s Specials'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Manager\'s Specials',
    'public'        => true,
    'menu_position' => 6,
    'supports'      => array( 'title'), //, 'editor' , 'thumbnail', 'excerpt', 'comments' 
    'has_archive'   => true,
  );
  register_post_type( 'manager_special', $args ); //this is what's used to call in contact details
}
add_action( 'init', 'manger_special_cpt' );

 
 
/**
 * Custom post type Part Specials
 */
function part_specials_cpt() {
  $labels = array(
    'name'               => _x( 'Part Specials', 'post type general name' ),
    'singular_name'      => _x( 'Part Special', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Part Special' ),
    'edit_item'          => __( 'Edit Part Special' ),
    'new_item'           => __( 'New Part Special' ),
    'all_items'          => __( 'View Part Specials' ),
    'view_item'          => __( 'View Part Special' ),
    'search_items'       => __( 'Search Part Specials' ),
    'not_found'          => __( 'No Part Specials' ),
    'not_found_in_trash' => __( 'No Part Specials found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Part Specials'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Part Specials',
    'public'        => true,
    'menu_position' => 6,
    'supports'      => array( 'title'), //, 'editor' , 'thumbnail', 'excerpt', 'comments' 
    'has_archive'   => true,
  );
  register_post_type( 'part_specials', $args ); //this is what's used to call in contact details
}
add_action( 'init', 'part_specials_cpt' ); 
 



/**
 * Custom post type Custom Numbers
 */
function custom_numbers_cpt() {
  $labels = array(
    'name'               => _x( 'Custom Numbers', 'post type general name' ),
    'singular_name'      => _x( 'Custom Number', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Custom Number' ),
    'edit_item'          => __( 'Edit Custom Number' ),
    'new_item'           => __( 'New Custom Number' ),
    'all_items'          => __( 'View Custom Numbers' ),
    'view_item'          => __( 'View Custom Number' ),
    'search_items'       => __( 'Search Custom Numbers' ),
    'not_found'          => __( 'No Custom Number' ),
    'not_found_in_trash' => __( 'No Custom Number found in the Trash' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Custom Numbers'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Custom Number',
    'public'        => true,
    'menu_position' => 6,
    'supports'      => array( 'title'), //, 'editor' , 'thumbnail', 'excerpt', 'comments' 
    'has_archive'   => true,
  );
  register_post_type( 'custom_numbers', $args ); //this is what's used to call in contact details
}
add_action( 'init', 'custom_numbers_cpt' );
?>
