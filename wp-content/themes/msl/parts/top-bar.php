<?php
/**
 * Template part for top bar menu
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */
$menu_logo = get_field('menu_logo', 'option'); 
$logoWidth = $menu_logo['sizes']['large-width'] / 2;
$logoHeight = $menu_logo['sizes']['large-height'] / 2;

$phoneNumber = get_field('local_phone_number', 'option');
?>

<div class="fixed ">
<div class="header-top"><!-- top-bar-container contain-to-grid show-for-medium-up -->
    <nav class="" data-topbar role="navigation"><!-- top-bar -->
        <!--
        <ul class="title-area">
            <li class="name">
                <h1><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></h1>
            </li>
        </ul>
        -->

        <div class="page-width" role="main">
        
            <section id="topbar-contact-header" >
                <div class="page-width">
                     <div class="row">
                        <div class="small-6 medium-offset-7 medium-3 columns nopadding" >
                            <div id="phone-number" class="phone-number" >
                                <a class="" href="tel:<?php echo $phoneNumber; ?>"><?php echo $phoneNumber; ?></a>
                            </div>
                        </div> 
                        <div class="small-6 medium-2 columns nopadding" >
                            <div id="contact-us" class="text-center"  data-reveal-id="contact-us-popup">
                                 Contact Us 
                            </div>
                        </div>    
                    </div>
                </div>
            </section>     
            
            <section class="top-bar-section  " >

                <div id="topbar-logo-and-menu"  class="relative page-width"> <!-- TODO  page-width  -->
                
                    <div class="row">
                        <div class="small-12 medium-5 columns" >
                            <a id="logo-link" href="<?php echo home_url(); ?>" >
                               <img id="" src="<?php echo $menu_logo['sizes']['large']; ?>" alt="<?php echo $menu_logo['alt']; ?>" 
                                    title="<?php echo $menu_logo['title']; ?>" style="max-width: <?php echo $logoWidth; ?>px; max-height:<?php echo $logoHeight; ?>px;" />
                            </a>
                        </div> 
                        <div class="small-12 medium-7 columns" >
                            <?php //foundationpress_top_bar_l(); ?>
                            <?php //foundationpress_top_bar_r(); ?>
                        </div> 
                    </div>
    
                    <div id="nav-container" >
      
                        <?php wp_nav_menu( array( 'theme_location' => 'top-bar-r' ) ); //, 'walker' => new megaMenuWalkerJarom()  ?>
    
                    </div>
                </div> 
                                
            </section> 
    
            
        </div> 
    </nav>
</div>
</div>
<!-- display this header when printing -->
<div class="page-width comp-print">
   <div class="row">
        <div class="small-8 columns" >  
            <img id="" src="<?php echo $menu_logo['sizes']['large']; ?>" style="max-width: <?php echo $logoWidth; ?>px; max-height:<?php echo $logoHeight; ?>px;" />
        </div>
        <div class="small-4 columns" >  
            <div class="tr"><?php echo $phoneNumber; ?></div>
        </div>        
   </div>           
</div>