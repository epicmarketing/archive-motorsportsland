<?php

    /****************************************************************
    The image for featured image comes from the right side bar of every page (when logged into the CMS)
    <?php get_template_part( 'parts/featured-image'); ?>
    ******************************************************************/

    //get featured image
    $thumbnail_id = get_post_thumbnail_id($post->ID);
    $thumbnail_object = get_post($thumbnail_id);
    
    
    //this will grab a parent image, but is currently untested and commented out
    //if($thumbnail_object->post_type != "attachment"){
    //    $thumbnail_id = get_post_thumbnail_id($thumbnail_object->post_parent);
    //    $thumbnail_object = get_post($thumbnail_id); 
    //}
    
    //print_r($thumbnail_object);    
    
    $supported_image = array(
        'gif',
        'jpg',
        'jpeg',
        'png'
    );
    
    $src_file_name = $thumbnail_object->guid;
    $ext = strtolower(pathinfo($src_file_name, PATHINFO_EXTENSION)); // Using strtolower to overcome case sensitive
    $isImage = false;
    if (in_array($ext, $supported_image)) {
        $isImage = true;
    } 
 
    $header_banner = $thumbnail_object->guid; 

?>

<?php if($isImage ){ ?>
    <div id="featured-image" style="background: url('<?php echo $header_banner; ?>') center center no-repeat;">
        &nbsp;
    </div>
<?php } ?>