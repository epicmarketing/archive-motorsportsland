<?php 
    /****************************************************************
    The variables for the title and main content come from the main Page Header and content WYSIWIG
    By default, it's included on every page (I've just put it here to keep the markup DRY)
    <?php get_template_part( 'parts/title-and-main-content'); ?>
    ******************************************************************/

    if ( have_posts() ) : the_post(); 
    
    $title = get_the_title();
    
    $content = get_the_content();
    $content = apply_filters('the_content', $content);
    
   
    endif; // end of the loop. 
?> 



<header>
    <h1 class="entry-title"><?php echo $title; ?></h1>
</header>

<?php do_action( 'foundationpress_page_before_entry_content' ); ?>
<div class="entry-content">
    <?php echo $content; ?>
</div>