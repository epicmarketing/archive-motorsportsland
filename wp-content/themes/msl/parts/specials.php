<?php 
    /****************************************************************
    Build the Manager's specials at the bottom of a page
    <?php get_template_part( 'parts/specials'); ?>
    ******************************************************************/
    
    //call in Motor Sportsland API
    $api = new msl_api();
    $specials = $api->fetch_manager_specials();
    
    //echo "<div class='red'>All Specials:</div><pre>".print_r($specials,1)."</pre>";
    
    $twoSpecials = array_rand ( $specials , 2  );
    
    $twoSpecialsStocks = array($specials[$twoSpecials[0]],$specials[$twoSpecials[1]]);
    
    //echo "<div class='red'>Random Specials:</div><pre>".print_r($twoSpecialsStocks,1)."</pre>";
    
    $rows = $api->fetch_inventory(false, $twoSpecialsStocks);
    
    //echo "<div class='red'>Results from two Specials:</div><pre>".print_r($rows,1)."</pre>";
?>


   


<div class="manager-special-container">
    <div class="small-12 columns ">
        <div class="display-for-medium-up special-top-border" >&nbsp;</div>
    </div>
      
    <?php   
       foreach($rows['rows'] as $special){
    ?>       
        <div class="small-12 medium-6 end columns ">
            
           
            <div class="special-header left" >
                <?php echo $api->build_inventory_title($special['thecondition'], $special['year'], $special['make'], $special['line'], $special['model'], true); ?>
            </div>
            <div class="special-link right"><a href="<?php echo BASE_URL."specials/"; ?>" >Special</a></div>
            <div class="clearfix"></div>
  
            <?php
            
                $rootImage = "wp-content/gallery/".$special['year']."/current/stock/".$special['stock']."/1.jpg";
                if (file_exists($rootImage)) {
                    $img = $rootImage;
                    //'<img src="'.BASE_URL.$rootImage.'" alt="'.$special['stock'].'" class="special-prod-image" />';
                } else {
                    $img = "wp-content/themes/msl/assets/img/no-images.jpg";
                    //'<img src="'.BASE_URL.$rootImage.'" alt="'.$special['stock'].'" class="special-prod-image"  />';
                }
                
                //echo $img;
            ?>
            
            <a href="<?php echo BASE_URL.'details/?stock='.$special['stock']; ?>">
            <div class="special-img-container relative " style="background: url('<?php echo BASE_URL.$img;?>') center 60% / cover;">
                
                <div class="dark-mask" ></div>
                <div class="special-price white" >
                    $<?php echo number_format ( $special['price'], 0 ); ?>
                </div>
                <div class="special-deal green" >
                     <?php 
                        $msrp = str_replace(",", "", $special['msrp']);
                        $price =  str_replace(",", "", $special['price']);
                        $difference = floatval($msrp) - floatval($price); 
                        echo "$".number_format ( ($difference), 0 )." Off MSRP"; 
                     ?>
                </div>
            </div>
            </a>
            
        </div>
        
    <?php    
       }
    ?> 
      
          
</div>  
    