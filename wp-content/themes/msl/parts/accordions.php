<?php

    /****************************************************************
    This variable comes from the "Accordion Repeater" Custom Field.  
    It can be assigned to other templates, etc, in the CMS admin sectin 
    and can then be added to php files with: 
    <?php include_once(FILEPATH.'parts/accordions.php') ?>
    <?php get_template_part( 'parts/accordions'); ?>
    *****************************************************************/

    $repeater_arr = get_field('accordion_repeater');
    
    //only build if the variable is on page
    if(!empty($repeater_arr)){
?>


    <ul class="accordion" data-accordion="">
        
        <?php
            foreach($repeater_arr as $key => $repeaterRow_arr){
                $num=0;
        ?>
        
            <li class="accordion-navigation relative">
                <a href="#panel<?php echo $key; ?>"><?php echo $repeaterRow_arr['accordion_header']; ?></a>
                <div id="panel<?php echo $key; ?>" class="content">
                    <div class="row">
                        <div class="small-12 medium-offset-1 medium-10 columns">
                            <?php echo $repeaterRow_arr['accordion_text']; ?>
                        </div>
                    </div>    
                </div>
            </li>                            

        <?php
            }
        ?>                  
        
    </ul>
    
<?php }//end if ?>