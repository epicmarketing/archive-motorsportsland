<?php 
    /****************************************************************
    This variable comes from the "Generic Page Content" Custom Field.  
    It can be assigned to other templates, etc, and can then be added with 
    <?php get_template_part( 'parts/secondary-content'); ?> 
    *****************************************************************/
    
    // check if the flexible content field has rows of data
    if( have_rows('secondary_content') ):
 ?> 

        <div class="container-wrap-main" id="secondaryContent">
            <div class="container">
                <?php
                    
                    // loop through the rows of data
                    while ( have_rows('secondary_content') ) : the_row();
                                // check current row layout
                        if( get_row_layout() == 'text_blocks' ):
                                $columns = get_sub_field('columns');
                                // check if the nested repeater field has rows of data
                                if( have_rows('text_block') ): ?>
                                  
                                        <div class="small-12 columns">
                                             <hr class="divider" />
                                        </div>
                                  
                                    <div class=" sub-section">
                                    <?php while ( have_rows('text_block') ) : the_row(); ?>
                                        <div class="small-12 medium-<?php echo $columns ?> columns">
                                       <?php if($image = get_sub_field('header_image')): ?>
                                            <div class="image-header"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive" /></div>
                                       <?php endif; ?>
                                           <?php echo get_sub_field('content'); ?>
                                        </div>
                                       <?php endwhile; ?>
                                    </div>

                                <?php endif; // repeater field has rows of data ?>
                    <?php endif; // check current row layout ?>
                    <?php endwhile; // loop through the rows of data ?>
           
            </div>
        </div>

 <?php endif; // check if the flexible content field has rows of data ?>        