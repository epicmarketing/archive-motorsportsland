<?php 
    /****************************************************************
    This variable comes from the "Generic Page Content" Custom Field.  
    It can be assigned to other templates, etc, and can then be added with 
    <?php include_once(FILEPATH.'parts/overlay-image.php') ?>
    *****************************************************************/
     $overlayHeaderImage = get_field( 'overlay_header_image' );  
 ?>           
           
<?php if($overlayHeaderImage){ ?>
    <div id="image-overlay" class="relative" >
        <div id="image-overlay-container" >
            <img src="<?php echo $overlayHeaderImage['url']; ?>" alt="<?php echo $overlayHeaderImage['alt']; ?>" class="img-responsive" />
        </div>
    </div>
<?php } ?>