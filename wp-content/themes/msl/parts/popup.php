<?php 
    /****************************************************************
    Form from contact us button at the top of the site
    Uses variables from "Motor Sportsland General Contact Settings" in the CMS
    Include it anywhere by pasting the line below:
    <?php get_template_part( 'parts/popup'); ?>
    ******************************************************************/

    //Address and Phone Details from "Motor Sportsland General Contact Settings"
    $addressLine1 = get_field('address_line_1', 'option');
    $addressLine2 = get_field('address_line_2', 'option');
    $city = get_field('city', 'option');
    $state = get_field('state', 'option');
    $zipCode = get_field('zip_code', 'option');
    
    $phoneNumber = get_field('local_phone_number', 'option');
    $tollFreePhoneNumber = get_field('toll_free_phone_number', 'option');   

    $hoursOfOperation = get_field('hours_of_operation', 'option');
    
   

?> 

<div id="contact-us-popup" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
    
    <!-- close button -->
    <a class="close-reveal-modal" aria-label="Close">&#215;</a>
    
    <div class="row">
        <div class="small-12 columns">    
          <div id="contact-us-title">Motor Sportsland</div>
        </div> 
        <div class="small-12 medium-6 columns nopadding-right">  
            <div class="contact-details-text">    
                <?php echo $addressLine1."<br />";?>
                <?php if($addressLine2 != "") echo $addressLine2."<br />"; ?>
                <?php echo $city;?>, <?php echo $state;?> <?php echo $zipCode;?>
            </div>    
         </div>  
        <div class="small-12 medium-6 columns nopadding-left">
            <div class="contact-details-text pad-left"> 
                <div>
                    <div class="display-inline green">Local:</div>
                    <div class="display-inline phone-number no-link-color">
                        <a class="" href="tel:<?php echo $phoneNumber; ?>"><?php echo $phoneNumber; ?></a> 
                    </div>
                </div>
                <div><div class="display-inline green">Toll-Free:</div><div class="display-inline"> <?php echo $tollFreePhoneNumber;?></div></div> 
            </div>
         </div>

          
        <hr class="divider" />
          

        <div class="small-12 columns"> 
            <div class="contact-hours-text">    
                <?php echo $hoursOfOperation;?>
            </div>    
         </div>  
                        
    </div> 

  <?php echo do_shortcode('[contact-form-7 id="121" title="Contact Us"]'); ?>
  
</div>