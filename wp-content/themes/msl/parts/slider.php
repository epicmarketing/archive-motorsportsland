<?php 
    /****************************************************************
    Put a slider on the page.  The featured image is the background, while images and text
    are pulled in via a repeater field.
    The slider uses bxslider (http://bxslider.com/options)
    <?php get_template_part( 'parts/slider'); ?>
    ******************************************************************/
    
 
    //get featured image
    $thumbnail_id = get_post_thumbnail_id($post->ID);
    $thumbnail_object = get_post($thumbnail_id);
    
    
    //this will grab a parent image, but is currently untested and commented out
    //if($thumbnail_object->post_type != "attachment"){
    //    $thumbnail_id = get_post_thumbnail_id($thumbnail_object->post_parent);
    //    $thumbnail_object = get_post($thumbnail_id); 
    //}
    
    //print_r($thumbnail_object);    
 
    $header_banner = $thumbnail_object->guid; 
    
    //get slider repeater
    $repeater_arr = get_field('slider_repeater');

?>

<?php if($repeater_arr){ ?>

    
    <div id="slider" class="relative" >
        <div id="slider-featured-image" style="background: url('<?php echo $header_banner; ?>') center center no-repeat; ">
            &nbsp;
        </div>
        
        <div id="slider-adjustment" >
            <div id="slider-container" >
                <ul class="bxslider" >
                  
                  
                    <?php
                        foreach($repeater_arr as $key => $repeaterRow_arr){
                            //print_r($repeaterRow_arr);
                    ?>
                        <li>
                          <div class="relative">
                              <div class="slider-text-container" >
                                 <div class="slider-header"><?php echo $repeaterRow_arr['slider_header']; ?></div>
                                  <div class="slider-text"><?php echo $repeaterRow_arr['slider_text']; ?></div>
                                  <a class="graybutton" href="<?php echo $repeaterRow_arr['slider_button_url']; ?>">
                                      <?php echo $repeaterRow_arr['slider_button_text']; ?>
                                  </a> 
                              </div>
                               
                              <div class="slider-img" > 
                                <img src="<?php echo $repeaterRow_arr['slider_image']['url']; ?>" alt="<?php echo $repeaterRow_arr['slider_image']['alt']; ?>" class="" />
                              </div>     
                          </div>                
                        </li>
                        
                    <?php
                        }
                    ?>             
     
                </ul>
            </div>
        </div>
    </div>

<?php } ?>