<?php 
    /****************************************************************
    Build the Similar Listings at the bottom of a page
    The page must have a GET variable for stock in the URL (e.g. ?stock=28334)
    <?php get_template_part( 'parts/similar'); ?>
    ******************************************************************/
    
    
    //page must have stock
    $stock = "";
    if( isset($_GET['stock']) && is_numeric($_GET['stock']) ){
        $stock = $_GET['stock'];
    }
    
    if( !empty($stock)){
        //call in Motor Sportsland API
        $api = new msl_api();
        $similarListings = $api->fetch_similar_listings($stock);
        
        if( count($similarListings) > 1 ){
        
            //get two        
            $twoSimilar = array_rand ( $similarListings , 2  );//but not "too" similar ;)
            $twoSimilarStocks = array($similarListings[$twoSimilar[0]]['stock'],$similarListings[$twoSimilar[1]]['stock']);
            
            //now get inventory details for these two stock numbers
            $rows = $api->fetch_inventory(false, $twoSimilarStocks);
    
            if(isset($_GET['test']) && $_GET['test'] == 1 && is_user_logged_in()){
                echo "<div class='red'>Similar Stock (same type but not same make):</div><pre>".print_r($similarListings,1)."</pre>";
                echo "<div class='red'>Two Similar Stocks:</div><pre>".print_r($twoSimilarStocks,1)."</pre>";
                echo "<div class='red'>Results from two Specials:</div><pre>".print_r($rows,1)."</pre>";
            }
        
?>

            <div class="manager-special-container">
                <div class="small-12 columns">
                    <div class=""><h1>Similar RVs</h1></div>
                </div>
                  
                <?php   
                foreach($rows['rows'] as $similar){
                ?>       
                <div class="small-12 medium-6 end columns ">
                    
                   
                    <div class="special-header " >
                        <?php echo $api->build_inventory_title($similar['thecondition'], $similar['year'], $similar['make'], $similar['line'], $similar['model'], true); ?>
                    </div>
                    <div class="clearfix"></div>
        
                    <?php
                    
                        $rootImage = "wp-content/gallery/".$similar['year']."/current/stock/".$similar['stock']."/1.jpg";
                        if (file_exists($rootImage)) {
                            $img = $rootImage;
                            //'<img src="'.BASE_URL.$rootImage.'" alt="'.$similar['stock'].'" class="special-prod-image" />';
                        } else {
                            $img = "wp-content/themes/msl/assets/img/no-images.jpg";
                            //'<img src="'.BASE_URL.'wp-content/themes/msl/assets/img/no-images.jpg" alt="No image currently available" class="special-prod-image"  />';
                        }                
                        
                    ?>
        
                    
                    <a href="<?php echo BASE_URL.'details/?stock='.$similar['stock']; ?>">
                    <div class="special-img-container relative" style="background: url('<?php echo BASE_URL.$img;?>') center 60% / cover;">
                        
        
        
                        <div class="dark-mask" ></div>
                        <div class="special-price white" >
                            $<?php echo number_format ( $similar['price'], 0 ); ?>
                        </div>
                        <div class="special-deal green" >
                             <?php 
                                $msrp = str_replace(",", "", $similar['msrp']);
                                $price =  str_replace(",", "", $similar['price']);
                                $difference = floatval($msrp) - floatval($price); 
                                echo "$".number_format ( ($difference), 0 )." Off MSRP"; 
                             ?>
                        </div>
                    </div>
                    </a>
                    
                </div>
                
                <?php    
                }
                ?> 
             
            </div>  
 <?php    
        }
   }//end if for $stock
?>    