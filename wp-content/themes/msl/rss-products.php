<?php
/**
 * Template Name: Custom RSS Template - Feedname
 */
$api = new msl_api();
$result = $api->fetch_inventory("","",1);
$rows = $result['rows'];

header('Content-Type: '.feed_content_type('rss-http').'; charset='.get_option('blog_charset'), true);
echo '<?xml version="1.0" encoding="UTF-8"?'.'>';
?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
    <channel>
        <title>Motor Sportsland Product Catalog</title>
        <link>http://dev.epicslc.com/msl</link>
        <description>All current inventory for Motor Sportsland</description>

<?php 
    foreach($rows as $k => $row){
        
        // $skip = array('27898','28263','28264','28306','28381');
        // $skip = array();
        // if(in_array($row['stock'],$skip)){
            // continue;
        // }//issue was caused by "&" in description
        
            if( strtolower($row['thecondition']) == "used"){
                $rootImage = "wp-content/gallery/used/".$row['stock']."/main.jpg";
            }else{
                $rootImage = "wp-content/gallery/".$row['year']."/current/stock/".$row['stock']."/main.jpg";
            }
            
            //make sure image exists
            if(!file_exists($rootImage)){
                $rootImage = 'wp-content/themes/msl/assets/img/no-images.jpg';
            }            
                        
            if (file_exists($rootImage)) {
                $img = BASE_URL.$rootImage;
            } else {
                $img = BASE_URL.'wp-content/themes/msl/assets/img/no-images.jpg';
            }
            
            $price = (isset($row['price']) && !empty($row['price']) ? 'USD'.$row['price'] : 'Call for details');//this doesn't work, it must be a numeric value
            $desc = (isset($row['description']) && !empty($row['description']) ? htmlspecialchars($row['description']) : 'Call for details about this unit.');
?>    

        <item>
            <g:id><?php echo $row['stock'];?></g:id>
            <g:title>"<?php echo $api->build_inventory_title($row['thecondition'], $row['year'], $row['make'],$row['line'],$row['model']);?>"</g:title>
            <g:description><?php echo $desc;?></g:description>
            <g:link><?php echo BASE_URL; ?>details?stock=<?php echo $row['stock']; ?></g:link>
            <g:image_link><?php echo $img;?></g:image_link>
            <g:brand><?php echo $row['line'];?></g:brand>
            <g:condition><?php echo $row['thecondition']; ?></g:condition>
            <g:availability>in stock</g:availability>
            <g:price><?php echo $price; ?></g:price>
            <g:google_product_category>920</g:google_product_category>
            <g:custom_label_0>MSRP: <?php echo $row['msrp']; ?></g:custom_label_0>
        </item>
<?php 
    }
?>          
    </channel>
</rss>