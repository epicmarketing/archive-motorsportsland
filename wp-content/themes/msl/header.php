<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */

    $args = array(
        'post_type' => 'custom_numbers', 
        'posts_per_page' => 100
    );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();
        $customNumberRepeater = get_field( 'custom_number_repeater' );

        //echo "<h3 style='padding-top: 40px;'>".get_the_title()."</h3> ";

    endwhile;
    
    //Cycle through features text
    foreach($customNumberRepeater as $key => $keyFeature){
        
        if(isset($_GET['utm_source']) &&  !empty($_GET['utm_source']) && strtolower($_GET['utm_source']) == strtolower($keyFeature['url_variable']) ){
            $_SESSION['phoneNumber'] = strtolower($keyFeature['replacement_phone_number']);
            $_SESSION['source'] = strtolower($keyFeature['url_variable']);
            $_SESSION['session'] = $_SERVER["HTTP_HOST"];
            set_custom_cookie($_SESSION['source'], $_SESSION['phoneNumber']);
        }

    }
    
    //because cookies die after first page!!  WHY?!
    if(isset($_SESSION['phoneNumber']) && $_SESSION['phoneNumber'] != "" && isset($_SESSION['source']) && $_SESSION['source'] != "" && isset($_SESSION['session']) && $_SESSION['session'] == $_SERVER["HTTP_HOST"] ){
       set_custom_cookie($_SESSION['source'],$_SESSION['phoneNumber']);
    }
    wp_reset_query();
    //echo "<pre>".print_r($_SESSION,1)."</pre>";
    //echo session_id();
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-precomposed.png">
		
		<?php wp_head(); ?>
		
		<?php 
		$templateCheck =  basename( get_page_template() );
		if($templateCheck == "details.php"){
		        
		    //call in Motor Sportsland API
            $api = new msl_api();

            $stockOverview = array();
            $stock = "";
            if(isset($_GET['stock']) && is_numeric($_GET['stock'])){
                $stock = $_GET['stock'];
                
                $stockOverviewRows = $api->fetch_inventory(false,array(0=>$stock));
                $stockOverview = array_shift($stockOverviewRows['rows']);//strip off the "0" since there is only one row and not multiple "rows"

            }
		?>
		
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
            
            fbq('init', '1620045334913836');
            fbq('track', "PageView");
            fbq('track', 'ViewContent', {
              content_name: '<?php echo $stockOverview['thecondition'].' '.$stockOverview['year'].' '.$stockOverview['make'].' '.$stockOverview['line'].' '.$stockOverview['model']; ?>',
              content_category: '920',//Vehicles & Parts    Vehicles    Motor Vehicles  Recreational Vehicles
              content_ids: ['<?php echo $stock;?>'],
              content_type: 'product',
              value: <?php echo $stockOverview['price'];?>,
              currency: 'USD',
              product_catalog_id: '199996510356139'
            }); 
        </script>
        <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=1620045334913836&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
      

		
		<?php
        }else{
		?>

        <?php 
        }
        ?>
        		
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>
	
	<!-- Contact Popup/Reveal -->
    <?php get_template_part( 'parts/popup'); ?>
	
	<div class="off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap">
	
	<?php do_action( 'foundationpress_layout_start' ); ?>
	
	<!--
	<nav class="tab-bar">
		<section class="left-small">
			<a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
		</section>
		<section class="middle tab-bar-section">
			
			<h1 class="title">
				<?php bloginfo( 'name' ); ?>
			</h1>

		</section>
	</nav> 

	<?php get_template_part( 'parts/off-canvas-menu' ); ?>
	-->

	<?php get_template_part( 'parts/top-bar' ); ?>

<section class="container" role="document">
	<?php do_action( 'foundationpress_after_header' ); ?>
	

<?php
//session_start();
?>	
	
<?php	
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
$wpdb->show_errors();
?>
